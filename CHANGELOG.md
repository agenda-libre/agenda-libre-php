# [3.6.0](https://git.framasoft.org/agenda-libre/agenda-libre-php/compare/3.5.0...3.6.0) (2019-05-16)


### Bug Fixes

* **sql:** sql schema for install ([44ace62](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/44ace62))
* default date in campaign administration ([5a70f6c](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/5a70f6c))
* event marker is now displayed on the map ([21c9997](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/21c9997))
* GeoJSON feed is now valid ([e7414b5](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/e7414b5))
* pb of quotes in campaign edition ([13c4a7b](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/13c4a7b)), closes [#56](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/56)
* remove php7 warning about construct method in classes ([4603eee](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/4603eee))
* save localgroup roles of an event ([7366de6](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/7366de6))
* various ([39fe54f](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/39fe54f)), closes [#57](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/57)


### Features

* change display of events in relation with a localgroup ([8bac49a](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/8bac49a))
* localgroup list ([c9822ce](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/c9822ce))
* migrate from openlayers to leaflet ([b0c17fa](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/b0c17fa))
* New export formats ([3a65410](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/3a65410)), closes [#8](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/8)



<a name="3.5.0"></a>
# [3.5.0](https://git.framasoft.org/agenda-libre/agenda-libre-php/compare/3.4.0...3.5.0) (2018-01-25)


### Bug Fixes

* **sql:** quote in region table ([77e5f36](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/77e5f36))
* allow only preview when editing new localgroup ([088d619](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/088d619))
* correct links in RSS subscribed in Firefox ([3c32d75](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/3c32d75)), closes [#42](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/42)
* hide context menu when not admin ([db7d422](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/db7d422))
* remove password from error message ([b329f1e](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/b329f1e))
* top of the page displayed twice when identification required ([e7140ca](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/e7140ca))


### Features

* Allow duplicate event. ([a970ff2](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/a970ff2)), closes [#6](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/6)
* improvement filter and export events ([f406a52](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/f406a52)), closes [#25](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/25)
* make easier purpose new event ([57724fd](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/57724fd)), closes [#37](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/37)



<a name="3.4.0"></a>
# [3.4.0](https://git.framasoft.org/agenda-libre/agenda-libre-php/compare/3.3.0...v3.4.0) (2017-06-16)


### Bug Fixes

* bad answer in ajax query to find localgroups ([441d401](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/441d401))
* empty map when served as https ([01ab446](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/01ab446)), closes [#42](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/42)
* fix map parameter reference when calling rss list ([9edf581](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/9edf581)), closes [#45](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/45)
* sql disambiguation ([f42da02](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/f42da02))
* wrong diff in tags when editing an event ([4590344](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/4590344)), closes [#43](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/43)


### Chores

* move default map parameters in config.inc.php ([d52a18a](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/d52a18a))
* update name of french regions ([1fb980b](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/1fb980b))


### Features

* add css class style in calendar depending on tags of the event ([01b75af](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/01b75af)), closes [#44](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/44)
* add tooltip over tags ([f181f70](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/f181f70)), closes [#36](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/36)
* allow to link one or more localgroups to each event ([750aa40](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/750aa40)), closes [#5](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/5)
* geolocation when editing a localgroup ([7f722ad](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/7f722ad))
* geolocation when editing an event ([28051cc](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/28051cc)), closes [#39](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/39) [#40](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/40)
* manage localgroups ([3836bf1](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/3836bf1)), closes [#7](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/7)


### BREAKING CHANGES

* Add the following configuration in your `www/inc/config.inc.php` file in line 99:

```php
/*
 * Map settings
 **/
$mapGlobalDisplay = array( /** in map.php */
    'lat'     => 46.8,
    'lon'     => 1,
    'zoom'    => 6,
);
$mapLocalgroupDisplay = array( /** in showlocalgroup */
    'zoom'    => 13,
);
$mapEventDisplay = array( /** in showevent */
    'zoom'    => 11,
);
```
* Add the following text into `adl/text/text.inc.php` on line 51 (and/or into your theme directory):

```php
    $adl_lg_role = array('organizer'=>"organisateur", 'coorganizer'=>"co-organisateur", 'partner'=>"partenaire", 'other'=>"autre");
```

Add the following text into `adl/templates/header.inc.html` on line 12 and into `adl/templates/iframe_header.inc.html` on line 12, and/or into your theme directory:

```php
    <link rel="stylesheet" type="text/css" href="adl/css/jquery-ui.css" />
```

Add the following text into `adl/css/calendar.css` on line 382:

```css
    /*
     * Format event submission in relation with localgroups.
     * (used only in submit.php)
     */
    fieldset {
        background-color:#CCC;
        /*max-width:500px;*/
        padding:1em;
        margin: 2em;
        border:2px solid blue;
        -moz-border-radius:8px;
        -webkit-border-radius:8px;
        border-radius:8px;
        text-align:left;
    }

    legend {
        text-align:left;
        font-style: italic;
        color: navy;
        font-weight: bold;
    }

    label {
        text-align:left;
        font-style: italic;
    }
    label.only {
        display: block;
        padding-top: 1em;
    }

    p.legend {
      margin-top:0em;
      margin-bottom:0px;
      margin-left:2em;
      text-align:left;
      font-style: italic;
      padding:0;
      color: Teal;
    }

    .required {
      font-weight: bold;
      text-decoration:
    }

    #lg-container {
        display: inline-block;
        position: relative;
        vertical-align: middle;
        width: 360px;
    }
    #lg-container input {
        width:100%
    }
    #lg-container ul {
        left:0 !important;
        right:0 !important;
        max-height:320px;
        overflow-y:auto;
        overflow-x:hidden;
    }
```

Execute the following sql statement:

```sql
CREATE TABLE `adl_localgroups_events` (
  `localgroup_id` int(11) NOT NULL DEFAULT '0',
  `event_id` int(11) NOT NULL DEFAULT '0',
  `role` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `adl_localgroups_events`
  ADD PRIMARY KEY (`event_id`,`localgroup_id`),
  ADD KEY `role` (`role`);
```
* Update regions sql table:

```bash
mysql -h localhost -p agendadb -u agendauser < sql/regions.sql
```
* This release require to add some configuration settings and to process some sql commands.

Add the following lines in file `inc/config.inc.php`:

```php
/**
 * set localgroupManagement to true to enable localgroups management feature
 **/
$localgroupManagement = true;

/**
 * set localgroupSubmit to true to enable localgroups submission feature
 **/
$localgroupSubmit = true;
```

Execute the following sql commands:

```sql
ALTER TABLE `adl_localgroups`
  ADD `country` VARCHAR(255) NULL ,
  ADD `submitter` VARCHAR(255) NOT NULL ,
  ADD `moderated` INT(11) NOT NULL DEFAULT '0' ,
  ADD `secret` VARCHAR(255) NULL ,
  ADD `decision_time` DATETIME NOT NULL ,
  ADD `submission_time` DATETIME NOT NULL ,
  ADD INDEX (`moderated`) ,
  ADD INDEX(`name`);

CREATE TABLE IF NOT EXISTS adl_localgroups_department
(
  `localgroup_id` int(11) NOT NULL,
  `department` int(11) NOT NULL,
  UNIQUE KEY `localgroup_id` (`localgroup_id`,`department`)
) DEFAULT CHARSET=utf8 COMMENT='scope of localgroups over departments';

REPLACE INTO adl_localgroups_department (localgroup_id, department)
  SELECT id, floor(postalcode/1000) FROM adl_localgroups WHERE floor(postalcode/1000)<97;
REPLACE INTO adl_localgroups_department (localgroup_id, department)
  SELECT id, floor(postalcode/100) FROM adl_localgroups WHERE floor(postalcode/1000)=97;

ALTER TABLE `adl_localgroups`
  DROP `region`,
  DROP `department`;

UPDATE `adl_regions` SET `id` = '50' WHERE `adl_regions`.`id` = 27;
INSERT INTO `adl_regions` (`id`, `name`) VALUES ('27', 'Territoires d\'outre-mer');
UPDATE `adl_region_department` SET `region_id` = '50' WHERE `adl_region_department`.`department_id` = 0;
INSERT INTO `adl_region_department` (`region_id`, `department_id`) VALUES ('26', '976');
INSERT INTO `adl_region_department` (`region_id`, `department_id`) VALUES ('27', '98');
```

Note: edit `adl_ prefix` if needed.



<a name="3.3.0"></a>
# [3.3.0](https://git.framasoft.org/agenda-libre/agenda-libre-php/compare/3.2.0...3.3.0) (2016-06-01)


### Bug Fixes

* add trim function on fields from form submit ([b53e388](https://git.framasoft.org/agenda-libre/agenda-libre/agenda-libre-php/commit/b53e388))
* address warning and error submitting event ([b9ab28c](https://git.framasoft.org/agenda-libre/agenda-libre/agenda-libre-php/commit/b9ab28c))
* edit address in submit form ([7f7c5fb](https://git.framasoft.org/agenda-libre/agenda-libre/agenda-libre-php/commit/7f7c5fb)), closes [#35](https://git.framasoft.org/agenda-libre/agenda-libre/agenda-libre-php/issues/35)
* stats were shifted by one month ([710bc7d](https://git.framasoft.org/agenda-libre/agenda-libre/agenda-libre-php/commit/710bc7d)), closes [#32](https://git.framasoft.org/agenda-libre/agenda-libre/agenda-libre-php/issues/32)


### Features

* allow administrator to choose tags minimum size ([6433eda](https://git.framasoft.org/agenda-libre/agenda-libre/agenda-libre-php/commit/6433eda)), closes [#30](https://git.framasoft.org/agenda-libre/agenda-libre/agenda-libre-php/issues/30)


### BREAKING CHANGES

* If you're upgrading from a previous version, you have to update the configuration file `config.inc.php` in order to add the variables `tagMinSize` and `tagMaxSize` (see `config.inc.php.template` file for an example).



<a name="3.2.0"></a>
# [3.2.0](https://git.framasoft.org/agenda-libre/agenda-libre-php/compare/3.1.0...3.2.0) (2016-03-29)


### Bug Fixes

* bug which authorize to show an event even if not moderated ([f369f64](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/f369f64))
* campaign is_tag attribute saved ([ece1fa0](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/ece1fa0)), closes [#28](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/28)
* choose place from address from nominatim ([41a3e41](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/41a3e41)), closes [#12](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/12) [#1](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/1)
* correct label in tag cloud page ([29a3dc3](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/29a3dc3)), closes [#27](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/27)
* event links in mail don't display anything ([8173e99](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/8173e99)), closes [#31](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/31)
* fix wikipedia link for city of an event ([eff8dc1](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/eff8dc1))
* geoloc fix nominatim datas after choice ([b9c119b](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/b9c119b))
* geoloc fix when nominatim answer if way type ([00a166d](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/00a166d))
* global scope for var ([55ff446](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/55ff446))
* group mails about an event in discussion ([68d9ce8](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/68d9ce8)), closes [#20](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/20)
* **doc:** fix typo in license headers ([470fc5f](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/470fc5f))
* include once classes ([edfa40b](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/edfa40b))
* labels and wikipedia url ([384e2ab](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/384e2ab))
* make difference between error and warning messages submiting event ([935218e](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/935218e)), closes [#29](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/29)
* notice daylimit unknown from tag link ([ab700d9](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/ab700d9))
* notice message when first execution ([d6b763b](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/d6b763b)), closes [#21](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/21)
* region function call not moved ([2aa64f7](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/2aa64f7))
* remove MyISAM type or engine in SQL scripts ([90f86ce](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/90f86ce)), closes [#22](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/22)
* sql in transfert keyword ([e7854eb](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/e7854eb)), closes [#26](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/26)

### Features

* **doc:** remove old sql statements to upgrade from a very old release ([3e028d6](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/3e028d6))
* **doc:** replace mysql by mariadb in install guide ([63cb497](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/63cb497)), closes [#19](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/19)
* adapt region an departement links to the new status in France in year 2016 ([ec18898](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/ec18898)), closes [#1](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/1) [#24](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/24)
* add 'add to my calendar' action to event page ([a8d29df](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/a8d29df)), closes [#23](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/23)
* allow moderator to edit an event in the past ([b997471](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/b997471))
* change address input mode and geolocalization process ([5727729](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/5727729))
* make simpler id/ref in sent mails ([831a8e9](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/831a8e9))
* remove adl_cities table reference in all scripts and sql config ([dc318eb](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/dc318eb))
* update regions to the new status of France in year 2016 in python script ([6fdef01](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/6fdef01))


### BREAKING CHANGES

* apply the following change on database:

```sql
DELETE FROM TABLE `adl_regions`, `adl_region_department`;
```

then reload data from the `regions.sql` sql file located in the `sql` directory.

Note: edit adl_ prefix if needed.
* as the adl_cities table is now useless and has been removed from the installation guide, you can remove this table from any existing agenda by running the following sql statement.

```sql
DROP TABLE adl_cities;
```

Note: edit adl_ prefix if needed.
* apply the following change on database:

```sql
ALTER TABLE `adl_events`
  DROP `moderator_mail_id`,
  DROP `submitter_mail_id`;
```

Note: edit adl_ prefix if needed.



<a name="3.1.0"></a>
# [3.1.0](https://git.framasoft.org/agenda-libre/agenda-libre-php/compare/3.0.0...3.1.0) (2015-11-02)


### Bug Fixes

* adl-submit.py uses the new VERSION file ([b652de7](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/b652de7))
* parameters of a function call to calendar_mail ([c0d2c62](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/c0d2c62))
* remove notice message from submit form when preview ([6fc3493](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/6fc3493)), closes [#9](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/9)
* **css:** update path to logo ([98dcfb1](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/98dcfb1))
* remove useless parameter check ([88358bb](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/88358bb))
* rename, move and publish script adl-submit.py / submit-event.py ([7ec7478](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/7ec7478))
* restore secure permissions ([47a50d0](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/47a50d0))
* submit-event.py works with any FQDN ([53b6137](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/53b6137))
* testevent.php works again ([7cb88b6](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/7cb88b6)), closes [#2](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/2) [#16](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/16)

### Features

* **doc:** add a README file ([b4f9514](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/b4f9514)), closes [#11](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/11)
* **doc:** add CONTRIBUTE.md file ([85ae18b](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/85ae18b))
* **doc:** convert guides to markdown ([aaa9e4d](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/aaa9e4d))
* **doc:** load sql file sql/campaign.sql during install ([e80f61a](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/e80f61a))
* **doc:** update guides to jessie ([fd82761](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/fd82761)), closes [#15](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/15)
* add automatic changelog generation capability ([bd2270e](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/bd2270e)), closes [#13](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/13)
* base url and version of the agenda are set in script headers ([ae2750e](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/ae2750e))
* replace old headers by GPLv3 headers ([94d3818](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/94d3818)), closes [#10](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues/10)
* style bigger to "Proposer un evenement" link and fields informations ([49d6f37](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/49d6f37))
* switch from GPLv2 to GPLv3 ([6537e40](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/6537e40))


### BREAKING CHANGES

* doc: upgrade instructions (from old releases) in UPGRADE.md file are available until this release only.



<a name="3.0.0"></a>
# [3.0.0](https://git.framasoft.org/agenda-libre/agenda-libre-php/compare/9299bf2...3.0.0) (2015-08-19)


### Bug Fixes

* script extract-cities-coordinates.sh use LibreOffice ([e9b9173](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/e9b9173))
* webalizer is not a feature of this agenda ([9299bf2](https://git.framasoft.org/agenda-libre/agenda-libre-php/commit/9299bf2))



<a name="19/02/2009"></a>
# 19/02/2009 (19/02/2009)

* Amélioration des messages de notification pour qu'ils s'organisent en fils de discussion.

<a name="17/02/2009"></a>
# 17/02/2009 (17/02/2009)

* Ajout d'une action "Ajouter à mon calendrier" sur chaque évènement. L'idée est d'avoir un lien sur chaque évènement qui permet d'enregistrer juste cet évènement dans son calendrier.

<a name="26/01/2009"></a>
# 26/01/2009 (26/01/2009)

* Amélioration de l'interface de modération pour proposer un système de petites notes permettant une meilleure communication entre les modérateurs.

<a name="26/09/2008"></a>
# 26/09/2008 (26/09/2008)

* Passage à l'encodage UTF-8 pour les pages Web, les flux RSS, les courriers électroniques envoyés et la base de données.

<a name="23/09/2008"></a>
# 23/09/2008 (23/09/2008)

* Le soumetteur d'un évènement peut maintenant éditer ou annuler son évènement pendant la phase de modération.
* Nouvelle action de modération permettant de demander au soumetteur d'un évènement d'ajouter des informations complémentaires à un évènement.

<a name="05/09/2008"></a>
# 05/09/2008 (05/09/2008)

* Intégration de [TinyMCE](http://www.tinymce.com/), un éditeur html WYSIWYG, afin de faciliter l'accès à l'*agenda du libre* pour ceux qui ne connaissent pas le html (contribution de Erwan Lehérissé).

<a name="12/07/2008"></a>
# 12/07/2008 (12/07/2008)

* Les évènements peuvent désormais être édités ou annulés par leur soumetteur après modération
* Refonte complète de la carte, désormais basée sur un fond de carte [OpenStreetMap](http://www.openstreetmap.org), en utilisant la bibliothèque Javascript [OpenLayers](http://www.openlayers.org). Cette carte affiche les prochains évènements, ainsi que la localisation des groupes d'utilisateurs.
* Ajout du tag `<georss:point>` dans le flux RSS pour préciser la localisation géographique d'un évènement. Cela peut servir à afficher les évènements sur une carte, comme sur la carte de l'Agenda du Libre, ou sur [Google Maps](http://maps.google.com/maps?q=http://www.agendadulibre.org/rss.php?region=all). Ce tag fait partie de la spécification [GeoRSS](http://www.georss.org).

<a name="29/06/2008"></a>
# 29/06/2008 (29/06/2008)

* Un courrier électronique est désormais envoyé au soumetteur dès que l'évènement est enregistré et est en attente de modération.

<a name="08/05/2008"></a>
# 08/05/2008 (08/05/2008)

* Nouvelle interface de modération.
* Édition des évènements validés par les modérateurs.

<a name="15/03/2008"></a>
# 15/03/2008 (15/03/2008)

* Mise en production d'une nouvelle CSS (développée par Erwan Lehérissé).

<a name="14/01/2007"></a>
# 14/01/2007 (14/01/2007)

* Mise en place du système de tags sur les évènements.
* Statistiques par ville.

<a name="29/10/2006"></a>
# 29/10/2006 (29/10/2006)

* Mise en place du sélecteur de région pour la navigation dans l'agenda.
* Affichage des flèches de navigation vers le passé ou le futur seulement si il y a des évènements à voir dans le passé ou le futur.

<a name="09/07/2006"></a>
# 09/07/2006 (09/07/2006)

* Ajout d'une option `--test-output` au script de soumission. Elle permet de récupérer une page html qui ressemble à ce que donnera l'évènement dans l'*agenda du libre* une fois modéré. Ceux qui utilisent le script de soumission peuvent donc avoir une prévisualisation.

<a name="02/05/2006"></a>
# 02/05/2006 (02/05/2006)

* Simplification dans l'affichage des dates. Quand le jour de début et de fin sont identiques, le jour n'est indiqué qu'une seule fois.

<a name="04/03/2006"></a>
# 04/03/2006 (04/03/2006)

* Ajout d'une *favicon* (proposée par Air1).

<a name="25/02/2006"></a>
# 25/02/2006 (25/02/2006)

* Ajout d'une liste des questions fréquemment posées.

<a name="05/02/2006"></a>
# 05/02/2006 (05/02/2006)

* Améliorations mineures du script `extract-gulls.py`.
* Mise en place d'une *favicon* basée sur le logo proposée par Air1.
* Mise à jour de la documentation d'installation.

<a name="19/02/2009"></a>
# 19/02/2009 (19/02/2009)

* Ajout d'une boîte de recommandations sur la page de soumission d'un évènement.
* Correction de problèmes avec Internet Explorer au niveau de la carte et de la feuille css.
* Affichage des groupes locaux de la région dans la carte, et lors de la consultation des informations sur un évènement.
* Ajout d'une page de statistiques `stats.php`.
* Amélioration du script de soumission et rédaction d'une documentation `submit-script-doc.php`.

<a name="21/12/2005"></a>
# 21/12/2005 (21/12/2005)

* Ajout d'une css alternative (proposée par Air1).

<a name="17/11/2005"></a>
# 17/11/2005 (17/11/2005)

* Légère amélioration de la carte, un lien est maintenant disponible.
* Petites améliorations sur le calendrier iCal.

<a name="05/11/2005"></a>
# 05/11/2005 (05/11/2005)

* Ajout d'une carte des évènements par région `map.php`

<a name="04/10/2005"></a>
# 04/10/2005 (04/10/2005)

* Correction d'un problème de recouvrement mineur dans le css.

<a name="20/09/2005"></a>
# 20/09/2005 (20/09/2005)

* Correction d'un bug dans la génération du calendrier iCal (oubli de stripper les backslashes ajoutés lors de l'insertion dans la base sql).
* Mise à jour de la liste des clients iCal qui fonctionnent.

<a name="19/09/2005"></a>
# 19/09/2005 (19/09/2005)

* Correction de plusieurs bugs dans la génération des calendriers iCal (champ UID manquant, champ PRODID manquant, point-virgules à la place de deux points, etc).
* Utilisation d'URLs en `webcal://` pour les calendriers.

<a name="18/09/2005"></a>
# 18/09/2005 (18/09/2005)

* Les calendriers donnent maintenant les évènements des 12 mois précédents dans le passé et jusqu'à l'infini dans le futur.
* Indication d'Evolution 2.0.4 et de KOrganizer dans les clients qui fonctionnent avec les calendriers iCal.
* Ajout d'une boîte rappelant aux visiteurs l'existence des flux rss et des calendriers iCal.
* Ajout de la liste des flux rss dans l'en-tête de la page pour que les navigateurs comme Firefox les affichent automatiquement.

<a name="17/09/2005"></a>
# 17/09/2005 (17/09/2005)

* Développement de la génération de calendriers *iCal* `icallist.php`. Comme pour les flux rss, il y a un calendrier disponible pour chaque région, ainsi qu'un calendrier national.
* Intégration des patches de validation des entrées (par David Mentré).
* Intégration du patch ajoutant le calendrier annuel (par David Mentré).
* Mise en place d'une authentification HTTP pour l'accès aux statistiques, afin d'éviter le *spam de referers*. Il est toujours possible d'accéder aux statistiques : le mot de passe est affiché dans la boîte de dialogue d'authentification.

<a name="18/08/2005"></a>
# 18/08/2005 (18/08/2005)

* Amélioration mineure de la documentation d'installation (contribution de Thierry Boudet).
* Mise en valeur du jour courant de l'agenda (contribution de Maxime Petazzoni).
* Ajout de conseils supplémentaires pour les contributeurs d'évènements.
* Affichage de la portée de l'évènement dans l'interface de modération, pour faciliter celle-ci.

<a name="30/07/2005"></a>
# 30/07/2005 (30/07/2005)

* Amélioration des fonctionnalités d'envoi de mail : les  modérateurs reçoivent un mail à chaque soumission d'évènement, à chaque édition, validation et suppression d'évènement.

<a name="15/06/2005"></a>
# 15/06/2005 (15/06/2005)

* Correction d'un bug dans la génération du rss.
* Amélioration de la css (proposée par Jean-Marie Favreau).

<a name="13/06/2005"></a>
# 13/06/2005 (13/06/2005)

* Rédaction des recommendations pour la modération.
* Mise en place d'un système de prévisualisation lors de la soumission d'un évènement.

<a name="12/06/2005"></a>
# 12/06/2005 (12/06/2005)

* Ajout d'un fichier `bd-private.inc.php.template` donnant un exemple de fichier `bd-private.inc.php`.
* Les jours passés et les jours à venir sont maintenant de couleur différente (patch soumis par Mélanie Bats).
* Dans le mail envoyé lorsqu'un évènement est modéré, un lien vers l'évènement est donné (patch soumis par Mélanie Bats).
* Utilisation d'une fonction ̀`quote_smart` pour formater correctement les arguments d'une requête sql (patch initialement soumis par Mendolia Davide).

<a name="07/06/2005"></a>
# 07/06/2005 (07/06/2005)

* Ajout du schéma des tables sql.

<a name="05/06/2005"></a>
# 05/06/2005 (05/06/2005)

* Lancement du site.
