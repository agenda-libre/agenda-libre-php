# about

Le projet *agenda-libre-php* est un logiciel libre qui permet de gérer collectivement un agenda.

C'est une solution adaptée pour les collectifs ou les organisations qui souhaitent proposer un agenda mutualisé et ouvert aux contributions externes, via un mécanisme de modération.

Ce projet a initialement été écrit pour l'[agenda du libre](http://www.agendadulibre.org/), bien que ce dernier n'utilise plus cette base de code.

N'hésitez pas à contribuer au développement de ce logiciel libre sur la [forge de développement](https://git.framasoft.org/agenda-libre/agenda-libre-php) des développeurs. Les discussions autour des améliorations à apporter au logiciel ont lieu sur la liste de diffusion *devel@agendadulibre.org* (cette liste nécessite une [inscription préalable](https://listes.agendadulibre.org/wws/subscribe/devel) très simple).

# history

Il y a longtemps, le site agenda.lolix.org, géré par Rodolphe Quiedeville, permettait de disposer d'un agenda des évènements locaux organisés partout en France autour du logiciel libre. Depuis sa décision d'arrêter de maintenir ce site, cet agenda manquait, et le nombre de dépêches postées sur LinuxFr à propos d'évènements locaux est devenu trop important. Il a donc semblé pertinent de relancer un site avec la même idée : un simple agenda des manifestations autour du logiciel libre. De cette idée est né l'[agenda du libre](http://www.agendadulibre.org/) en 2005.

D'abord géré par des personnes de bonne volonté, l'[agenda du libre](http://www.agendadulibre.org/) a depuis été repris par l'association [April](http://www.april.org/) qui a redéveloppé l'agenda dans le langage ruby. L'[agenda du libre du Québec](http://www.agendadulibre.qc.ca/event/2015/7/) a également redéveloppé une version de l'agenda dans le langage python.

Toutes ces versions sont disponibles dans ce groupe [agenda-libre](https://git.framasoft.org/groups/agenda-libre) :

- [agenda-libre-php](https://git.framasoft.org/agenda-libre/agenda-libre-php)
- [agenda-libre-ruby](https://git.framasoft.org/agenda-libre/agenda-libre-ruby)
- [agenda-libre-python](https://git.framasoft.org/agenda-libre/agenda-libre-python)

## contributors

- Mélanie Bats, main author
- Thomas Petazzoni, main author
- David Mentré, some patches
- Erwan Lehérissé, css and favicon
- Loïc Dayot, new features
- Ploc, some patches

