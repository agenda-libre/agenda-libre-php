var args = process.argv.slice(2),
    version = args[0],
    fs = require('fs'),
    baseVersionFilepath = "./VERSION",
    distVersionFilepath = "./www/VERSION";

fs.writeFile(baseVersionFilepath, version, function(err) {
    if(err) {
        return console.log(err);
    }

    console.log("version " + version + " written to " + baseVersionFilepath);
});

fs.writeFile(distVersionFilepath, version, function(err) {
    if(err) {
        return console.log(err);
    }

    console.log("version " + version + " written to " + distVersionFilepath);
});
