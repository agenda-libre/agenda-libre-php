<?php

/*
 * Copyright 2008-2015
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * - Loic Dayot <ldayot CHEZ ouvaton POINT org>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

$privatePage = true;

include("inc/main.inc.php");
include("inc/class.event.inc.php");

if (isset($_POST['__event_requestinfo_cancel']))
{
  header("Location: moderation.php");
  exit;
}
else if (isset($_POST['__event_requestinfo_send']))
{
  $id = get_safe_integer('id', 0);

  $event = new event($db, $id);
  if ($event->error || $event->moderated)
    {
      put_header();
      echo "<p>Opération interdite</p>";
      error($event->message);
      put_footer();
      exit;
    }

  $text =
    "Bonjour,\n\n" .
    "Nous avons bien reçu votre proposition d'évènement '" .
    stripslashes($event->title) . "', et celui-ci a toute sa " .
    "place dans {$adl_title}. Néanmoins, avant d'être " .
    "validé, nous avons besoin de quelques informations " .
    "complémentaires sur cet évènement :\n\n" .
    stripslashes($_POST["__text"]) . "\n\n" .
    "Nous vous invitons à ajouter ces informations en éditant " .
    "directement l'évènement à l'adresse suivante : \n\n" .
    calendar_absolute_url("editevent.php?id=" . $event->id
        . "&secret=" . $event->secret) . "\n\n" .
    "Avec tous nos remerciements pour votre contribution,\n\n" .
    "-- \n" .
    "L'équipe des modérateurs de {$adl_title}.";

  $text = wordwrap($text);

  calendar_mail($event->submitter,
    "Demande d'informations sur l'évènement '" .
    stripslashes($event->title) . "'", $text,
    $event->id);

  calendar_mail($moderatorlist,
    "Demande d'informations sur l'évènement '" .
    stripslashes($event->title) . "' envoyée", $text,
    $event->id);


  $note = "<p>Demande d'informations complémentaires&nbsp;:</p>" .
       "<pre>" . wordwrap(stripslashes($_POST["__text"])) . "</pre>";

  $sql = "insert into {$GLOBALS['db_tablename_prefix']}notes (contents, date, event_id, author_id) values (" .
     $db->quote_smart($note) . "," .
     $db->quote_smart($now)  . "," .
     $id                     . "," .
     $user->get_id_logon()   . ")";

  $ret = $db->query($sql);
  if ($ret == FALSE)
    {
      error("La requête a échoué");
      return -1;
    }

  header("Location: moderation.php");
}

$id = get_safe_integer('id', 0);

put_header("Demande d'informations sur un évènement");

$event = new event($db, $id);
if ($event->error)
  {
    error($event->message);
    put_footer();
    exit;
  }

if ($event->moderated)
  {
    echo "<p>Évènement déjà modéré</p>";
    put_footer();
    exit;
  }

echo '<p class="moderationheader">';
echo "<a href=\"moderation.php\">Modération</a>".
  "&nbsp;&gt;&gt;&gt&nbsp;Demande d'informations sur l'évènement";
echo "</p>";

?>

<div class="moderationbox">
<h2>Rédaction du message</h2>

<form action="requestinfosevent.php?id=<?php echo $id ?>" method="post">
<p>Bonjour,</p>

<p>Nous avons bien reçu votre proposition d'évènement '<? echo
$event->title; ?>', et celui-ci a toute sa place dans <?php echo $adl_title; ?>.
Néanmoins, avant d'être validé, nous avons besoin de quelques
informations complémentaires sur cet évènement :</p>

<textarea id="__text" cols="80" rows="10" name="__text"></textarea>

<p>Nous vous invitons à ajouter ces informations en éditant directement
l'évènement à l'adresse suivante :</p>

<p><?php echo calendar_absolute_url("editevent.php?id=" . $event->id
. "&secret=" . $event->secret) ?></p>

<p>Avec tous nos remerciements pour votre contribution,</p>

<p>L'équipe des modérateurs de <?php echo $adl_title; ?>.</p>

<input name="__event_requestinfo_send" type="submit" value="Envoyer"/>
<input name="__event_requestinfo_cancel" type="submit" value="Annuler"/>
</form>
</div>

<?php
echo '<div class="moderationbox">';
echo $event->formatHTML();
echo '</div>';

put_footer();
?>
