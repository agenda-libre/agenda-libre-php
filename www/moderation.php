<?php

/*
 * Copyright 2008-2015
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * - Loïc Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

$privatePage = true;
include('inc/main.inc.php');
include_once('inc/class.region.inc.php');

put_header("Modération");

echo "<h2>Évènements à modérer</h2>\n";

echo "<p style=\"font-weight: bold; text-align: center;\">Modérateurs, merci de lire et de tenir compte des <a href=\"moderation-howto.php\">recommandations de modération</a>.</p>";

$ret = $db->query ("select id, title, start_time, end_time, datediff(now(), submission_time) as waiting_time, city, region ".
  "from {$GLOBALS['db_tablename_prefix']}events where moderated=0 order by start_time");

if ($ret == FALSE)
{
  echo "<p class=\"error\">Requête SQL invalide</p>";
  put_footer();
  exit;
}

if ($db->numRows($ret)>0) {
  echo "<table style=\"text-align: center; width: 100%\" class=\"stats\">\n";

  echo "<tr>\n";
  echo "<th>Titre</th>\n";
  echo "<th>Date</th>\n";
  echo "<th>Ville</th>\n";
  echo "<th>Région</th>\n";
  echo "<th>En attente depuis</th>\n";
  echo "<th>Actions</th>\n";
  echo "</tr>";

  $i = 0;
  while ($row = $db->fetchObject($ret))
  {
    if ($i % 2)
        $class = "odd";
    else
        $class = "even";

    $start_day = onlyday_mysql2humanreadable($row->start_time);
    if ($start_day == onlyday_mysql2humanreadable($row->end_time)) {
        $date = "Le " .  $start_day . ", de "
        . onlyhour_mysql2humanreadable($row->start_time) . " à "
        . onlyhour_mysql2humanreadable($row->end_time);
    } else {
        $date = "Du " . date_mysql2humanreadable($row->start_time)
        . " au " . date_mysql2humanreadable($row->end_time);
    }

    echo "<tr class=\"" . $class . "\">\n";
    echo " <td  style=\"padding-top: 0.5em; padding-bottom: 0.5em\">" . stripslashes($row->title) . "</td>\n";
    echo " <td>" . $date . "</td>\n";
    echo " <td>" . stripslashes($row->city) . "</td>\n";
    $oRegion = new region($db, $row->region);
    echo " <td>" . stripslashes($oRegion->name) . "</td>\n";
    if ($row->waiting_time == 0)
      echo "<td>Aujourd'hui</td>\n";
    else if ($row->waiting_time == 1)
      echo "<td>Hier</td>\n";
    else
      echo "<td>" . $row->waiting_time . " jours</td>\n";
    echo " <td>";
    echo "<a href=\"editevent.php?id=" . $row->id . "\"><i class='fas fa-edit'></i> &Eacute;diter</a>&nbsp;-&nbsp;";
    echo "<a href=\"validateevent.php?id=" . $row->id . "\"><i class='fas fa-thumbs-up'></i> Valider</a>&nbsp;-&nbsp;";
    echo "<a href=\"rejectevent.php?id=" . $row->id . "\"><i class='fas fa-thumbs-down'></i>Refuser</a><br/>";
    echo "<a href=\"requestinfosevent.php?id=" . $row->id . "\"><i class='fas fa-question'></i> Demander des infos</a><br/>";
    echo "<a href=\"addnoteevent.php?id=" . $row->id . "\"><i class='fas fa-comment'></i> Ajouter une note</a>";
    echo " </td>\n";
    echo "</tr>\n";

    $sql = "select * from {$GLOBALS['db_tablename_prefix']}notes ".
      "where event_id=" . $row->id . " order by date desc";
    $notes = $db->query($sql);
    while($note = $db->fetchObject($notes))
    {
      echo "<tr class=\"" . $class . "\">\n";
      echo "<td style=\"background: white;\"></td>";
      echo "<td style=\"text-align: left; padding: 5px;\" colspan=\"5\">";
      echo $note->contents;
      echo "<br/>";
      echo "<p style=\"text-align: right; font-size: 80%; padding: 0px; margin: 0px;\">";
      $authorUser = new user($db, $session);
      echo "<i>Posté par " . $authorUser->get_name($note->author_id) . ", le " . date_mysql2humanreadable($note->date) . "</i>";
      echo "</p>";
      echo "</td>";
      echo "</tr>";
    }

    $i = $i + 1;
  }

  echo "</table>\n";
}

put_footer();

?>
