<?php
/*
 * Copyright 2007-2018
 * - Mélanie Bats <melanie POINT bats CHEZ utbm POINT fr>
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * - Loic Dayot <ldayot CHEZ ouvaton POINT org>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

include("inc/main.inc.php");
include("inc/class.event.inc.php");
include('inc/class.export.inc.php');

function list_events($events)
{
  global $db;
  $event = new event($db);

  echo " <ul>\n";
  while($event->id = $db->getOne($events))
    {
      $event->get();
      echo "<li>";
      echo "<a href=\"showevent.php?id=" . $event->id . "\">";
      echo stripslashes($event->title);
      echo "</a>";
      $startday = onlyday_timestamp2humanreadable(date_mysql2timestamp($event->start_time));
      $endday   = onlyday_timestamp2humanreadable(date_mysql2timestamp($event->end_time));
      echo "<br/>";
      if ($startday == $endday)
        echo "le " . $startday . " ";
      else
        echo "du " . $startday . " au " . $endday . " ";
      echo " à " . $event->city;
      echo "</li>";
    }
  echo " </ul>\n";
}

$hasevent = FALSE;

$eventList = new exportEvent($db);
$eventList->get2fields();
$events = $eventList->getEventsList();

if ($eventList->region != "all")
{
    $region = new region($db, $eventList->region);
    $region = $region->name;
}
else
  $region = $adl_all_region;

put_header("Liste d'évènements");

echo "<h2>Les évènements $region <i>" . implode(' ', $eventList->tags) . "</i></h2>\n";

if (! $events)
{
  error (_("Erreur lors de la requête SQL."));
  put_footer();
  exit;
}

// display exports url
echo "<div class='list-group'>Ces événements en flux ";
echo "<a class='list-group-items' href=\"". $eventList->rssUrl(). "\"><i class='fas fa-rss'></i>&nbsp;RSS</a>&nbsp; ";
echo "<a class='list-group-items' href=\"". $eventList->icalUrl(). "\"><i class='fas fa-calendar'></i>&nbsp;iCal</a>&nbsp; ";
echo "<a class='list-group-items' href=\"". $eventList->jsUrl(). "\"><i class='fab fa-js'></i>&nbsp;JavaScript</a>&nbsp; ";
echo "<a class='list-group-items' href=\"http://www.google.com/calendar/render?cid=".
  $eventList->calGoogleUrl(). "\"><i class='fab fa-google'></i>&nbsp;calendrier Google</a>";
echo "</p>";

if ($db->numRows($events))
{
  $hasevent = TRUE;
  echo "<p>";
  echo "<b>Prochainement</b>";
  if ($db->numRows($events) == 1)
    echo ", un évènement&nbsp;:";
  else
    echo ", " . $db->numRows($events) . " évènements&nbsp;:";
  echo "</p>";

  // display event list
  list_events($events);
}


// Dans le passé
unset($eventList->start);
$eventList->daylimit = 0;
$eventList->past = true;
$events = $eventList->getEventsList();

if (! $events)
{
  error (_("Erreur lors de la requête SQL."));
  put_footer();
  exit;
}

if ($db->numRows($events))
{
  $hasevent = TRUE;
  echo "<p>";
  echo "<b>Dans le passé</b>";
  if ($db->numRows($events) == 1)
    echo ", un évènement&nbsp;:";
  else
    echo ", " . $db->numRows($events) . " évènements&nbsp;:";
  echo "</p>";
  list_events($events);
}

if (! $hasevent)
  echo "<p>Aucun évènement avec ce filtre.</p>";

put_footer();
?>

