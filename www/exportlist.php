<?php

/*
 * Copyright 2015-2018
 * - Loic Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

include("inc/main.inc.php");
include("inc/class.export.inc.php");

put_header("Tous les exports");

echo "<h2>Liste des flux exportables</h2>";
echo $adl_export_intro;

$tagsE = array();

// Purpose url

echo "<fieldset><legend>". _("Sélection"). "</legend>";

$export = new exportEvent($db);
$export->get2fields();
// Get tags from form
$export->getTagList('tags');


echo "<ul class='fa-ul'>\n";
echo "  <li><i class='fas fa-li fa-rss'></i>&nbsp;Flux RSS 1.0 : <a href=\"". $export->rssUrl(). "\">". $export->rssUrl(). "</a>\n";
echo "  <li><i class='fas fa-li fa-rss'></i>&nbsp;Flux GeoRSS (sans les détails) : <a href=\"". $export->georssUrl(). "\">". $export->georssUrl(). "</a>\n";
echo "  <li><i class='fas fa-li fa-code'></i>&nbsp;Flux XML : <a href=\"". $export->XmlUrl(). "\">". $export->XmlUrl(). "</a>\n";
echo "  <li><i class='fas fa-li fa-code'></i>&nbsp;Flux KML : <a href=\"". $export->KmlUrl(). "\">". $export->KmlUrl(). "</a>\n";
echo "  <li><i class='fab fa-li fa-jsfiddle'></i>&nbsp;Flux GeoJSON : <a href=\"". $export->geoJsonUrl(). "\">". $export->geoJsonUrl(). "</a>\n";
echo "  <li><i class='fas fa-li fa-calendar'></i>&nbsp;Calendrier iCal : <a href=\"". $export->iCalUrl(). "\">". $export->iCalUrl(). "</a>\n";
echo "  <li><i class='fab fa-li fa-google'></i>&nbsp;Google calendar : <a href=\"". $export->calGoogleUrl(). "\">". $export->calGoogleUrl(). "</a>\n";
echo "  <li><i class='fab fa-li fa-js'></i>&nbsp;widget (JavaScript) : <code><a href=\"". $export->jsUrl(). "\">". $export->jsCode(). "</a></code>\n";
echo "  <li><i class='fas fa-li fa-share'></i>&nbsp;web (liste dans le navigateur) : <a href=\"". $export->webUrl(). "\">". $export->webUrl(). "</a>\n";
echo "</ul>\n";

echo "</fieldset>\n";

echo "<form action=\"\" method=\"get\" enctype=\"multipart/form-data\">\n";
echo "<fieldset><legend>". _("Critères de sélection"). "</legend>";

// Select place
$region = new region($db);
echo "<strong>". _("Région :") ."</strong> ". $region->selectHTML($export->region). " ";

// Select duration
echo $export->selectDurationHTML($export->daylimit). " ";

// Select tags
$tagsHtml = new tagsHtml($db);
echo $tagsHtml->selectTagsHTML($export->tags);

// Select localgroup
$localgroups = new localgroups($db);
$localgroups->localgroup_id = $export->localgroup_id;
echo "<p>". $localgroups->selectHTML('localgroup'). "</p>\n";

echo "  <input type=\"submit\" name=\"submit\" value=\"". _("Mettre à jour la sélection"). "\" /> <input type=\"reset\" name=\"reset\" value=\"". _("Réinitialiser"). "\" />\n";
echo "</fieldset>";

echo "</form>\n";

echo "<hr>\n";
echo $adl_rss_more;
echo "<hr>\n";
?>

<p>Les exports calendrier iCal ont été testés avec succès avec :</p>

<ul>
<li><a href="http://www.mozilla.org/projects/calendar/sunbird.html">Mozilla Sunbird</a>, l'application Calendrier indépendante de Mozilla. <br/>Pour ajouter l'Agenda du Libre, allez dans <i>File</i> puis <i>Subscribe to Remote Calendars</i>. Indiquer qu'il s'agit d'un calendrier distant en sélectionnant <i>Remote</i>. À l'étape suivante, sélectionnez <i>WebDAV</i>, et indiquer l'URL du calendrier que vous trouverez ci-dessous. À l'étape qui suit, donner un nom à ce calendrier, puis validez.</li>
<li>L'<a href="http://www.mozilla.org/projects/calendar/download.html">extension Calendar</a> pour Mozilla Firefox. <br/>Pour ajouter l'Agenda du Libre, allez dans <i>Fichier</i> puis <i>S'abonner à un calendrier distant</i>. Dans la fenêtre qui s'ouvre, entrez un nom pour le calendrier, ainsi que l'URL de celui-ci, disponible dans la liste ci-dessus.</li>
<li><a href="http://korganizer.kde.org/">KOrganizer</a>, le calendrier de KDE</li>
<li><a href="http://www.gnome.org/projects/evolution/">Evolution</a> (versions 2.0.4 et 2.2.x).</li>
<li>vCalendar, <a href="http://claws.sylpheed.org/plugins.php">un plugin</a> pour <a href="http://claws.sylpheed.org">Sylpheed Claws</a>. Chargez le module par le menu Configuration/Modules, puis avec un clic droit sur le dossier vCalender, choisissez "S'inscrire à un Webcal".</li>
<li><a href="http://www.gnu.org/software/emacs/emacs.html">GNU Emacs</a>, à l'aide de l'extension <a href="http://de.geocities.com/ulf_jasper/lisp/icalendar.el.txt">icalendar.el</a></li>
</ul>

<p>Les applications suivantes peuvent sans doute fonctionner:</p>
<ul>
<li>Une <a href="http://www.mozilla.org/projects/calendar/download.html">extension Calendar</a> pour Thunderbird.</li>
</ul>

<p>N'hésitez pas à <a href="<?php echo scramble_email($adl_mailto_devel); ?>">nous faire part</a> de vos succès et de vos échecs avec d'autres logiciels.</p>

<?php
echo $adl_ical_more;

echo "<hr>\n";

echo $adl_js_more;

echo "<script type=\"text/javascript\" language=\"JavaScript\" src=\"". $export->jsUrl(). "\"></script>\n";

put_footer();
?>
