<?php

/*
 * Copyright 2004-2018
 * - Mélanie Bats <melanie POINT bats CHEZ utbm POINT fr>
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * - Loic Dayot <ldayot CHEZ ouvaton POINT org>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

include("inc/main.inc.php");
include_once("inc/class.event.inc.php");
include_once("inc/class.localgroup.inc.php");
include_once("inc/class.export.inc.php");

$event_id = get_safe_integer('id', 0);
$event = new event($db, $event_id);
if ($event->error || $event_id==0)
{
  put_header("Informations sur un évènement");
  error ($event->message);
  put_footer();
  exit;
}
if (! $event->moderated)
{
  put_header("Informations sur un évènement");
  error (_("L'événement est en attente de modération."));
  put_footer();
  exit;
}

// Select localgroups

$localgroups = new localgroups($db);
$localgroups->getFromDepartment($event->department);
if (count($localgroups->localgroups)<10)
{
    $localgroups->getFromRegion($r = $event->region);
}

if ($event->latitude>0)
  {
    $jcode = <<<JSCODE1

  <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="js/leaflet.js"></script>
  <script type="text/javascript" src="js/leaflet.markercluster-src.js"></script>
  <script type="text/javascript" src="js/displayMap.js"></script>


  <script type="text/javascript">
      var lat= {$event->latitude};
      var lon= {$event->longitude};
      var zoom= {$GLOBALS['mapEventDisplay']['zoom']};

      function init() {

        displayMap(lat, lon, zoom);

        /* events ------------------------------------- */
        addLayer('%s',
          "<span style='color:darkred;'><i class='fas fa-calendar'></i>&Eacute;v&eacute;nements</a></span>",
          eventIcon, false);

        /* localgroups ------------------------------------- */
JSCODE1;

    $region = new region($db);
    $jcode .= "\n        addLayer('". calendar_absolute_url('exportlocalgroups.php?').
      (isset($r) ? "region={$r}" : "depart={$event->department}"). "',
      \"<span style='color:blue;'><i class='fas fa-users'></i>{$adl_localgroups_acronym}</a></span>\",
      localgroupIcon, false);\n";

    $jcode .= "\n      }\n  </script>\n";

    $export = new exportEvent($db);
    $export->event_id = $event_id;
    $url = $export->geoJsonUrl();
    $jcode = sprintf($jcode, $url);

    put_header("Informations sur un évènement", $jcode, "init();");
    // end map
  }
else
  put_header("Informations sur un évènement");

// context div
echo "<div id=\"localgroup-list\">";

// Display campaigns in context div
include_once('inc/class.campaign.inc.php');
$campaigns = new campaigns($db);
// If event in a campaign, display it
echo $campaigns->returnTagMessage($event->tags);

// Display actions in context div
echo "<h1>Actions</h1>\n";
echo "<a href=\"ical.php?event_id=". $event_id. "\" title=\"Export au format iCal\"><i class='fas fa-calendar'></i> Ajouter à mon calendrier</a><br/>";
echo "<a href=\"submit.php?id=" . $event_id . "&new\"><i class='fas fa-copy'></i> Dupliquer évènement</a><br/>";
if ($session->exists('agenda_libre_id'))
  { // moderateur
    echo "<a href=\"editevent.php?id=" . $event_id . "\"><i class='fas fa-edit'></i> &Eacute;diter évènement</a><br/>";
    echo "<a href=\"cancelevent.php?id=" . $event_id . "\"><i class='fas fa-trash'></i> Annuler évènement</a>";
  }

// Display Localgroups in context div
if (count($localgroups->localgroups) > 0)
  {
    echo "<h1>". $adl_localgroups_around. "</h1>";
    echo $localgroups->aroundListHtml();
  }

echo "</div>\n";

// Display event
echo $event->formatHTML();

// start map
if ($event->latitude>0)
  echo "<ul class='mist' id='map' style=\"margin: auto; width:70%; height:500px; border: 1px solid black;\"></ul>\n";
// end map

put_footer();

?>
