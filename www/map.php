<?php
/*
 * Copyright 2004-2018
 * - Mélanie Bats <melanie POINT bats CHEZ utbm POINT fr>
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * - Loic Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

include("inc/main.inc.php");
include("inc/class.export.inc.php");

$jcode = <<<JSCODE1

  <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="js/leaflet.js"></script>
  <script type="text/javascript" src="js/leaflet.markercluster-src.js"></script>
  <script type="text/javascript" src="js/displayMap.js"></script>

  <script type="text/javascript">
      var lat= {$GLOBALS['mapGlobalDisplay']['lat']};
      var lon= {$GLOBALS['mapGlobalDisplay']['lon']};
      var zoom= {$GLOBALS['mapGlobalDisplay']['zoom']};

      function init() {

        displayMap(lat, lon, zoom);

        /* events ------------------------------------- */
        addLayer('%s',
          "<span style='color:darkred;'><i class='fas fa-calendar'></i>&Eacute;v&eacute;nements</a></span>",
          eventIcon, false);

        /* localgroups ------------------------------------- */
JSCODE1;

$region = new region($db);
foreach ($region->getList() as $reg) {
    $jcode .= "\n        addLayer('". calendar_absolute_url('exportlocalgroups.php'). "?region={$reg['id']}',
          \"<span style='color:blue;'><i class='fas fa-users'></i>{$reg['name']}</a></span>\",
          localgroupIcon, true);\n";
}

$jcode .= "\n      }\n  </script>\n";

$export = new exportEvent($db);

$export->daylimit = get_safe_integer('daylimit', 30);
$export->getTagList('tags');
$url = $export->geoJsonUrl();
$jcode = sprintf($jcode, $url);

put_header("Carte", $jcode, "init();");

echo "<h2>Carte des événements</h2>\n";

echo "<form action=\"\" method=\"get\" enctype=\"multipart/form-data\">\n";
echo "<fieldset><legend>". _("Critères de sélection"). "</legend>";
// Select duration
echo $export->selectDurationHTML($export->daylimit). " ";
// Select tags
$tagsHtml = new tagsHtml($db);
echo $tagsHtml->selectTagsHTML($export->tags);
echo "  <input type=\"submit\" name=\"submit\" value=\"". _("Mettre à jour la sélection").
  "\" /> <input type=\"reset\" name=\"reset\" value=\"". _("Réinitialiser"). "\" />\n";
echo "</fieldset>";
echo "</form>\n";

echo "<ul class='mist' id='map' style=\"margin: auto; width:70%; height:500px; border: 1px solid black;\"></ul>\n";

put_footer();
?>
