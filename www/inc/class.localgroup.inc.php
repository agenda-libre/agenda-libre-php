<?php

/*
 * Copyright 2015-2018
 * - Loic Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

include_once('inc/class.region.inc.php');
include_once('inc/class.geocode.inc.php');
include_once('inc/class.event.inc.php');

class localgroup {
    var $id,            // from table
        $name,
        $url,
        $comment,
        $address, $postalcode, $city,
        $country,
        $latitude, $longitude,
        $contact, $phone, $mail,
        $submitter, $moderated, $secret,
        $decision_time, $submission_time;

    var $geocode,         // class geocode
        $place,           // text input by user
        $osmId,           // place id from geocode
        $department, $region, $region_id,
        $reach;           // array, department list
    var $events;          // array of events
    var $waiting_time;

    var $is_preview;
    var $error, $message;
    var $db;

    // -------------------------------------------------------------------

    function __construct($db, $id=0)
    {
        $this->db = $db;
        $this->error = 0;
        $this->message = "";
        $this->geocode = new geocode($db);
        $this->reach = array();
        if ($id>0)
        {
            $this->id = $id;
            $this->get();
        }
        return ! $this->error;
    }

    // -------------------------------------------------------------------

    function get()
    {
    global $adl_localgroup;
        $query = "SELECT *, datediff(now(), submission_time) as waiting_time ".
            "FROM {$GLOBALS['db_tablename_prefix']}localgroups ".
            "WHERE id=" . $this->db->quote_smart($this->id);
        $result = $this->db->query ($query);
        if (! ($record = $this->db->fetchObject($result)))
        {
            $this->message = "Aucun {$adl_localgroup} avec l'ID ". $this->id;
            $this->error = true;
            return false;
        }
        $this->name         = $record->name;
        $this->url          = $record->url;
        $this->comment      = $record->comment;

        $this->address          = $record->address;
        $this->place            = $this->address;
        $this->postalcode       = sprintf("%'.05d", $record->postalcode);
        $this->city             = $record->city;
        $this->department       = floor($this->postalcode/1000);
        if ($this->department==97)
            $this->department       = floor($this->postalcode/100);
        if ($this->department>0)
        {
            $region = new region($this->db);
            $this->region_id    = $region->getRegionFromDepartment($this->department);
            $this->region       = $region->name;
            unset($region);
        } else {
            $this->region_id    = 29;
            $this->region       = "Inconnue";
        }
        $this->country          = $record->country;
        $this->latitude         = $record->latitude;
        $this->longitude        = $record->longitude;

        if (strripos($this->city, $this->place)===false)
        {
            $this->place = $this->address. " ".
                sprintf("%'.05d", $this->postalcode). " ". $this->city.
                " ". $this->country;
        }
        // contact
        $this->contact          = $record->contact;
        $this->phone            = $record->phone;
        $this->mail             = $record->mail;
        // moderation
        $this->submitter        = $record->submitter;
        $this->moderated        = ($record->moderated == 1);
        $this->secret           = $record->secret;
        $this->decision_time    = $record->decision_time;
        $this->submission_time  = $record->submission_time;
        $this->waiting_time     = $record->waiting_time;

        unset($record);
        $this->db->freeResult($result);

        // get reach
        $this->reach = array();
        $query = "SELECT * FROM {$GLOBALS['db_tablename_prefix']}localgroups_department".
            " WHERE localgroup_id=".
            $this->db->quote_smart($this->id). " ORDER BY department";
        $result = $this->db->query ($query);
        while ($record = $this->db->fetchObject($result))
        {
            $this->reach[] = $record->department;
        }
        if (count($this->reach)==0 && $this->department>0)
        {
            $this->reach[] = $this->department;
        }

        $this->message = "";
        $this->error = false;
        return true;
    }

  // -------------------------------------------------------------------

    function save()
    {
    global $adl_localgroup, $now;
        $sql = ($this->id>0 ? "UPDATE " : "INSERT INTO ");
        $sql .= "{$GLOBALS['db_tablename_prefix']}localgroups SET " .
          "name=" .         $this->db->quote_smart ($this->name)        . ", ".
          "url=" .          $this->db->quote_smart ($this->url)         . ", ".
          "city=" .         $this->db->quote_smart ($this->city)        . ", ".
          "address=".       $this->db->quote_smart ($this->address)     . ", ".
          "postalcode=".    $this->db->quote_smart ($this->postalcode)  . ", ".
          "country=" .      $this->db->quote_smart ($this->country)     . ", ".
          "latitude=" .     $this->db->quote_smart ($this->latitude)    . ", ".
          "longitude=" .    $this->db->quote_smart ($this->longitude)   . ", ".
          "contact=" .      $this->db->quote_smart ($this->contact)     . ", ".
          "phone=" .        $this->db->quote_smart ($this->phone)       . ", ".
          "mail=" .         $this->db->quote_smart ($this->mail)        . ", ".
          "comment=" .      $this->db->quote_smart ($this->comment)     . ", ".
          "submitter=" .   $this->db->quote_smart ($this->submitter);
        if ($this->id==0)
        {
            $sql .= ", moderated='0', ".
                "secret='" . ($this->secret = md5(uniqid(rand(), true))) . "', ".
                "submission_time=". $this->db->quote_smart($now);
        } else {
            $sql .= ", moderated=". ($this->moderated ? 1 : 0);
            if ($this->moderated)
            {
                $sql .= ", decision_time=". $this->db->quote_smart($now);
            } else {
                $sql .= ", submission_time=". $this->db->quote_smart($now);
            }
            $sql .= " WHERE id=" .    $this->db->quote_smart ($this->id);
        }

        $ret = $this->db->query($sql);
        if ($ret == FALSE)
        {
            $this->error=true;
            $this->message = returnError("Impossible d'enregistrer le {$adl_localgroup}");
            error ("La requête <i>" . $sql . "</i> a échoué");
        } else {
            if ($this->id>0)
            { // Delete old reach
                $sql = "DELETE FROM {$GLOBALS['db_tablename_prefix']}localgroups_department".
                    " WHERE localgroup_id=" . $this->db->quote_smart($this->id);
                $ret = $this->db->query($sql);
                if (! $ret)
                {
                  $this->error=true;
                  $this->message = returnError("Impossible de supprimer la port&eacute;e du {$adl_localgroup}");
                  error ("La requête <i>" . $sql . "</i> a échoué");
                }
            }
        }
        if ($this->id==0)
        {
            $this->id = $this->db->insertid();
        }
        if (! $this->error && count($this->reach)>0)
        { // Add reach
            $sql = "INSERT INTO {$GLOBALS['db_tablename_prefix']}localgroups_department ".
              "(localgroup_id, department) VALUES ";
            $sep = "";
            foreach ($this->reach as $department)
            {
                $sql .= $sep. "(". $this->db->quote_smart ($this->id).
                  ", ". $this->db->quote_smart ($department). ")";
                $sep = ", ";
            }
            $ret = $this->db->query($sql);
            if ($ret == FALSE)
            {
                $this->error=true;
                $this->message = returnError("Impossible d'enregistrer la port&eacute;e de {$adl_localgroup}");
                error ("La requête <i>" . $sql . "</i> a échoué");
            }
        }

        return ! $this->error;
    } // end function_save();

    // -------------------------------------------------------------------

    function delete()
    {
    global $adl_localgroup;
        $sql = "DELETE FROM {$GLOBALS['db_tablename_prefix']}localgroups".
            " WHERE id=" . $this->db->quote_smart($this->id);
        $ret = $this->db->query($sql);
        if (! $ret)
        {
          $this->error=true;
          $this->message = returnError("Impossible de supprimer le {$adl_localgroup}");
          error ("La requête <i>" . $sql . "</i> a échoué");
          return false;
        } else {
            // delete reach
            $sql = "DELETE FROM {$GLOBALS['db_tablename_prefix']}localgroups_department".
                " WHERE localgroup_id=" . $this->db->quote_smart($this->id);
            $ret = $this->db->query($sql);
            if (! $ret)
            {
              $this->error=true;
              $this->message = returnError("Impossible de supprimer la port&eacute;e du {$adl_localgroup}");
              error ("La requête <i>" . $sql . "</i> a échoué");
              return false;
            }
        }
        return true;
    }

  // -------------------------------------------------------------------

    /*
     * Form (edit function)
     * $duplicate=true seems get datas but as new record
     */
    function formHtml($duplicate = false, $previewEnable=false, $submitEnable=true)
    {
    global $adl_localgroup, $session;
        $html = <<<JQUERY
    <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="https://openlayers.org/api/OpenLayers.js"></script>
JQUERY;
        if (file_exists("js/tinymce"))
        {
            $html .= <<<MCE
  <!-- tinyMCE -->
  <script language="javascript" type="text/javascript" src="js/tinymce/tinymce.min.js"></script>
  <script language="javascript" type="text/javascript">
      tinymce.init({
        selector: 'textarea',
        language : 'fr_FR',
        theme : 'modern',
        plugins : 'textcolor advlist autolink link image lists table charmap paste table preview',
        toolbar:  'undo redo | styleselect | bold italic forecolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | paste pastetext | code preview',
        menubar: false,
        width: '80%',
        height: 300,
        extended_valid_elements : [ 'a[name|href|target|title|onclick]',
          'img[class=myclass|!src|border:0|alt|title|width|height|style]' ],
      });
  </script>
  <!-- /tinyMCE -->
MCE;
        } // end if tiny_mce

        $html .= "<form action=\"moderatelocalgroups.php\" method=\"post\"".
            " enctype=\"multipart/form-data\" name=\"localgroupForm\" class='form'>\n".
            "<input type=\"hidden\" name=\"op\" value=\"update\" />\n";
        $html .= "<table class='stats'>";
        if ($this->id>0 && ! $duplicate) {
            $html.= "<input type='hidden' name='lg[id]' value='{$this->id}' />";
            $html.= "<input type='hidden' name='id' value='{$this->id}' />";
        }

        $html .= "<fieldset><legend>Description de l'&eacute;v&eacute;nement</legend>\n";
        $html .= "<label for='__lg_name' class='required only'>Saisir le nom du {$adl_localgroup}*</label>".
            "<input type='text' size='70' required name='lg[name]' value=\"{$this->name}\" id='__lg_name' />".
            "<br/>\n";
        $html .= "<label for='__lg_comment' class='only'>Description</label>".
            "<textarea name='lg[comment]' cols='70' rows='15' id='__lg_comment' ".
            "title=\"Commentaire/description.\">". $this->comment. "</textarea>".
            "<br/>\n";
        $html .= "<label for='__lg_url' class='only required'>URL*</label>".
            "<p class='legend'>Lien <b>direct</b> vers une page donnant plus d'informations sur".
            "le {$adl_localgroup} au format <code>https://url</code>".
            "ou <code>http://url</code></p>".
            "<input type='url' size='70' required name='lg[url]' id='__lg_url' value='". $this->url. "'/>";
        $html .= "</fieldset>\n\n";

        $html .= "<fieldset><legend>Localisation</legend>\n";
        $html .= $this->geocode->mapInput('lg', $this);
        $html .= "<label for='__event_reach' class='required only'>Portée*</label>".
            "<p class='legend'>Saisir les num&eacute;ros des d&eacute;partements".
            " dans lesquels le {$adl_localgroup} a une activit&eacute;, s&eacute;par&eacute;s par un espace.</p>".
            "<input type='text' name='lg[reach]' size='50' id='__lg_reach' required value=\"";
        foreach ($this->reach as $department)
            $html .= sprintf("%'.02d", $department). " ";
        $html .= "\"></fieldset>\n";


        $html .= "<fieldset><legend>Contacts</legend>\n";
        // contact
        $html .= "<label for='__lg_contact' class='only'>Nom du contact</label>".
            "<input type='text' size='40' name='lg[contact]' value=\"{$this->contact}\" id='__lg_contact' />".
            "<br/>\n";
        $html .= "<label for='__lg_phone' class='only'>T&eacute;l&eacute;phone</label>".
            "<input type='text' size='40' name='lg[phone]' value=\"{$this->phone}\" id='__lg_phone' />".
            "<br/>\n";
        $html .= "<label for='__lg_mail' class='only'>Courriel</label>".
            "<input type='email' size='40' name='lg[mail]' value=\"{$this->mail}\" id='__lg_mail' />".
            "<br/>\n";
        // submitter
        $html .= "<label for='__lg_submitter' class='only".
            ($session->exists("agenda_libre_id") ? "" : " required").
            "'>Soumetteur". ($session->exists("agenda_libre_id") ? "" : "*").
            "</label>".
            "<input type='email' size='40' ".
            ($session->exists("agenda_libre_id") ? "" : " required").
            " name='lg[submitter]' value=\"{$this->submitter}\" id='__lg_submitter' />\n";
        $html .= "</fieldset>\n";

        $html .= "<p class='legend'>";
        if ($previewEnable)
            $html .= "<input type=\"submit\" name=\"lg[preview]\" ".
            "value=\"". _("Prévisualisation"). "\" /> ";
        $html .= "<input type=\"submit\" ".
            "name=\"submit\" value=\"". _("Enregistrement"). "\" ".
            (! $submitEnable ? "disabled='disabled'" : ""). " />".
            "</p>\n";

        $html .= "</form>\n";

        return $html;

    } // end function_fromForm

  // -------------------------------------------------------------------

  /*
   * Form (edit function)
   * $duplicate=true seems get datas but as new record
   */
    function confirmDelFormHtml()
    {
    global $adl_localgroup;
        $html = $this->formatHtml();

        $html .= "<form action=\"moderatelocalgroups.php\" method=\"post\"".
            " enctype=\"multipart/form-data\" name=\"localgroupDelForm\">\n".
            "<input type=\"hidden\" name=\"op\" value=\"delete\" />\n";
        if ($this->id>0) {
            $html.= "<input type='hidden' name='id' value='{$this->id}' />";
        }
        $html .= "<label for='is_del' title=\"Cocher pour confirmer la suppression.\" >".
            "Supprimer le {$adl_localgroup} <input id='is_del' type='checkbox' ".
            "name='is_del' value='1' /></label>\n";

        $html .= "<input type=\"submit\" name=\"submit\" value=\"". _("Supprimer"). "\" />\n";
        $html .= "</form>\n";

        return $html;

    }

  // -------------------------------------------------------------------

  /*
   * Came from Form (del function)
   *
   */
  function fromConfirmDelForm()
  {
        if (isset($_POST['id'])
            && preg_match("~^[0-9]+$~", $_POST['id'])) { // match only positive integers
            $this->id = $_POST['id'];
        } else {
            $this->message .= error("Pas de numéro de validation");
        }

        if (! isset($_POST['__event_validation_confirm'])) {
            $this->message .= error("Pas de validation");
        }

        $this->error = ($this->message>"");

    return ! $this->error;
  }

  // -------------------------------------------------------------------

  /*
   * Came from Form (edit function)
   *
   */
    function fromForm()
    {
        if (isset($_POST['lg']['id'])
            && preg_match("~^[0-9]+$~", $_POST['id'])) { // match only positive integers
            $this->id = $_POST['lg']['id'];
        }

        /* text */
        foreach (array('name', 'url', 'place',
            'address', /*'postalcode',*/ 'city', 'country',
            'osmId', 'contact', 'phone', 'mail',
            'reach', 'submitter') as $fieldName)
        {
          if (isset($_POST['lg'][$fieldName]))
            $this->$fieldName = stripslashes(strip_tags($_POST['lg'][$fieldName]));
        }
        if (isset($_POST['lg']['lon'])) $this->longitude = $_POST['lg']['lon'];
        if (isset($_POST['lg']['lat'])) $this->latitude = $_POST['lg']['lat'];
        if (isset($_POST['lg']['postalcode']))
          $this->postalcode = sprintf("%'.05d", $_POST['lg']['postalcode']);

        /* html code */
        $aFieldName = array('comment');
        foreach ($aFieldName as $fieldName)
        {
          if (isset($_POST['lg'][$fieldName]))
            $this->$fieldName = stripslashes(strip_tags($_POST['lg'][$fieldName],
                "<p><strong><em><br/><a><ul><li><ol><b><i><img><h1><h2><h3><code>"));
        }

        if (isset($_POST['lg']['reach']) && trim($_POST['lg']['reach'])!='')
        { // format tags
            $lg_reach = $_POST['lg']['reach'];
            $this->reach = stripslashes(strip_tags(trim(str_replace("  ", " ", $lg_reach))));
            $this->reach = array_unique(explode(' ', $this->reach), SORT_NUMERIC);
            sort($this->reach);
        }
        else
        {
          $this->reach = array();
          $this->reach[] = substr($this->postalcode, 0, 2);
        }

        /* preview */
        $this->is_preview = isset($_POST['lg']['preview']);

        return true;

    } // end function_fromForm

  // -------------------------------------------------------------------

  // TODO
  // Check information from event edit form
    function check()
    {
        // bloquants
        if (strlen($this->name)<4)
        {
            $this->message .= error("Le nom est trop court");
        }

        if (! $this->place)
          {
            $this->message .= returnError ("Adresse vide");
          }

        if (! $this->url)
          {
            $this->message .= returnError ("URL vide");
          }
        elseif (strncmp ($this->url, "http://", 7) && strncmp($this->url, "https://", 8))
          {
            $this->message .= returnError ("URL ne commençant pas par http:// ou https://");
          }

        foreach ($this->reach as $dept)
        {
            if (! is_numeric($dept))
                $this->message .= error("La port&eacute;e ne contient pas des num&eacute;ros de d&eacute;partements");
        }

        $this->error = ($this->message>"");

        // avertissement bloquant si pas connecte
        if (!empty($this->submitter) && !
            preg_match("~^([-A-Za-z0-9_+.]*)@([-A-Za-z0-9_]*)\.([-A-Za-z0-9_.]*)$~",
            $this->submitter))
        {
            $this->message .= returnError ("Courriel du soumetteur invalide");
            if (! $session->exists("agenda_libre_id"))
                $this->error = true;
        }

        // avertissements non bloquants
        if (! isset($this->longitude))
          {
            $this->message .= returnError ("Adresse non localis&eacute;e");
          }

        if (!empty($this->mail) && !
            preg_match("~^([-A-Za-z0-9_+.]*)@([-A-Za-z0-9_]*)\.([-A-Za-z0-9_.]*)$~",
            $this->mail))
        {
            $this->message .= returnError ("Courriel du contact invalide");
        }

        if (strlen($this->comment) < 10)
          $this->message .= returnWarning("La description est bien courte");

        if (strlen($this->contact)<4)
            $this->message .= returnWarning("Le nom du contact est bien court");

        if (strlen($this->phone)<10)
            $this->message .= returnWarning("Le t&eacute;l&eacute;phone est bien court");

        return !$this->error;
    } //

  // -------------------------------------------------------------------
    function formatReachHtml()
    {
        return str_replace($this->department, "<strong>$this->department</strong>", implode(', ', $this->reach));
    }


  // -------------------------------------------------------------------

    /**
    * Format an localgroup in an HTML format
    *
    * @return A HTML description of the localgroup
    */
    function formatHtml()
    {
    global $adl_localgroup, $session;
        $name        = stripslashes($this->name);
        $region      = stripslashes($this->region);
        $city        = stripslashes($this->city);
        $address     = stripslashes($this->address);
        $postalcode  = stripslashes($this->postalcode);
        $reach       = $this->formatReachHtml();
        $comment     = stripslashes($this->comment);
        $url         = stripslashes($this->url);
        $contact     = stripslashes($this->contact);
        $mail        = scramble_email(stripslashes($this->mail));
        $phone       = stripslashes($this->phone);
        $submitter   = scramble_email(stripslashes($this->submitter));

        $result  = "<h2><i>" . $city . "</i> : " . $name . "</h2>\n\n";
        if (! $this->moderated) {
            $result .= "<p class='warning'>Attention, ce {$adl_localgroup}".
            " est en attente de mod&eacute;ration</p>\n";
        }
        $result .= "<h3>Localisation</h3>\n";
        $result .= "<p>{$address} - {$postalcode} "
                . "<i><a href=\"http://fr.wikipedia.org/wiki/" . strtolower($city) . "\">"
                . $city . "</a></i> - <a href=\"http://fr.wikipedia.org/wiki/"
                . $region . "\">" . $region . "</a></p>\n";
        $result .= "<p>Port&eacute;e&nbsp;: {$reach}.</p>\n";

        if ($comment>"") {
            $result .= "<h3>Description</h3>\n";
            $result .= "{$comment}\n";
        }

        $result .= "<h3>Informations</h3>\n";
        $result .= "<p>Site Web&nbsp;: <a href=\"" . $url . "\">" . $url . "</a></p>\n";
        $result .= "<p>Contact&nbsp;: {$contact} - <a href=\"mailto:"
                . $mail . "\">" . $mail . "</a> - {$phone}</p>\n";

        if ($session->exists("agenda_libre_id")) {
            $result .= "<p>Soumetteur&nbsp;: {$submitter}</p>\n";
        }

        return $result;
    }

    // -------------------------------------------------------------------

    /**
    * Format an localgroup in an ASCII format, suitable for e-mails
    *
    * @return An ASCII description of the localgroup
    */
    function formatAscii()
    {
    global $adl_localgroup;
        $name        = stripslashes($this->name);
        $region      = stripslashes($this->region);
        $city        = stripslashes($this->city);
        $address     = stripslashes($this->address);
        $postalcode  = stripslashes($this->postalcode);
        $comment     = stripslashes($this->comment);
        $url         = stripslashes($this->url);
        $contact     = stripslashes($this->contact);
        $mail        = scramble_email(stripslashes($this->mail));
        $phone       = stripslashes($this->phone);

        $result  = "Nom          : $name\n";
        $result .= "Localisation : {$address} - {$postalcode} {$city} - $region\n";
        $result .= "Description  : \n " . wordwrap(preg_replace ("/\n/", "\n ",
            strip_tags(utf8_encode($comment))));
        $result .= "<p>Site Web  : <a href=\"" . $url . "\">" . $url . "</a></p>\n";
        $result .= "<p>Contact   : {$contact} $mail - {$phone}\n";

        return $result;
    }

    // -------------------------------------------------------------------

    /**
    * Format an localgroup in an CSV format, suitable for OSM
    *
    * @return A CSV description of the event with localisation
    * lon\tlat\ttitle\tdescription\ticon\n
    */
    function formatCsvOsm()
    {
        // has localgroup coordonates ?
        if ($this->longitude==0)
        { // no
            return;
        }

        $ret =
            $this->longitude . "\t" .
            $this->latitude  . "\t" .
            $this->name       . "\t";
        // comment : address + url
        $ret .= "<p style=\"font-size: 60%; text-align: center;\"><i>";
        if ($this->address>"")
            $ret .= "{$this->address} - {$this->postalcode}";
        $ret .= " à " . $this->city . "</i><br/>";
        if ($this->comment>"")
        {
          $ret .= "<a href=\"". calendar_absolute_url("showlocalgroup.php?id=". $this->id). "\">".
            calendar_absolute_url("showlocalgroup.php?id=". $this->id). "</a>";
        } else {
          if ($this->url>"")
            {
              $ret .= "<a href=\"". $this->url. "\">". $this->url. "</a>";
            }
        }
        $ret .= "</p>" . "\t" .
            calendar_absolute_url(_themeFile("image/localgroup.png")) . "\n";

        return $ret;
    }

  // -------------------------------------------------------------------

    function toGeojson()
    {
        $properties = new \StdClass();
        $properties->id = $this->id;
        $properties->name = $this->name;
        $properties->href = calendar_absolute_url("showlocalgroup.php?id=". $this->id);
        $properties->place_name = $this->place;
        $properties->address = $this->address;
        $properties->city = $this->city;
        $properties->country = $this->country;
        $properties->postalcode = $this->postalcode;
        $properties->comment = $this->comment;

        $JSON_properties = json_encode($properties);

        return <<<PARTOFGEOJSON
    { "type": "Feature",
      "properties": {$JSON_properties},
      "geometry": {
        "type": "Point",
        "coordinates": [ {$this->longitude}, {$this->latitude} ]
      }
    }
PARTOFGEOJSON;
    }

  // -------------------------------------------------------------------

    function actionsHtml()
    {
    global $user;
        // context div
        $html = "<div id=\"localgroup-list\">";
        $html .= "<h1>Actions</h1>\n";
        if (!$this->moderated && $user->get_id_logon()) {
            $html .= "En attente depuis ". ($this->waiting_time == 0 ?
                "aujourd'hui" : ($this->waiting_time == 1 ? "hier" :
                    $this->waiting_time. " jours"));
            $html .= "<br/><a href='moderatelocalgroups.php?op=validateform&id={$this->id}'><i class='fas fa-file'></i> Valider</a>".
                "<br/><a href='moderatelocalgroups.php?op=rejectform&id={$this->id}'>".
                    "<i class='fas fa-minux-circle'></i> Refuser</a>".
                "<br/><a href='moderatelocalgroups.php?op=requestinfoform&id={$this->id}'>".
                    "<i class='fas fa-question'></i> Demander des infos</a>".
                "<br/><a href='moderatelocalgroups.php?op=addnoteform&id={$this->id}'>".
                    "<i class='fas fa-comment-alt'></i> Ajouter une note</a><br/><br />";
        }
        $html .= "<a href='moderatelocalgroups.php?op=edit&id={$this->id}'>".
            "<i class='fas fa-edit'></i> Éditer</a>";
        $html .= "<br/><a href='moderatelocalgroups.php?op=dupl&id={$this->id}'>".
            "<i class='fas fa-copy'></i> Dupliquer</a>".
            "<br/><a href='moderatelocalgroups.php?op=del&id={$this->id}'>".
            "<i class='fas fa-trash'></i> Supprimer</a>\n";
        $html .= "</div>\n";
        return $html;
    }

  // -------------------------------------------------------------------

    function confirmValidateFormHtml()
    {
    global $adl_localgroup;
        $html = <<<VALFORM
<div class="moderationbox">
    <form action="moderatelocalgroups.php?id={$this->id}" method="post">
        <input name="op" type="hidden" value="validate"/>
        <input name="id" type="hidden" value="{$this->id}"/>
        <p class="moderationheader">Confirmez-vous la validation de ce {$adl_localgroup}&nbsp;?</p>
        <input name="__lg_validation_confirm" type="submit" value="Oui"/>&nbsp;
        <input name="__lg_validation_cancel" type="submit" value="Non"/>
    </form>
</div>

<div class="moderationbox">
VALFORM;
        $html .= $this->formatHTML();
        $html .= '</div>';
        return $html;
    }

  // -------------------------------------------------------------------

    /*
    * Came from Form (validate function)
    *
    */
    function fromConfirmValidateForm()
    {
        if (isset($_POST['id'])
            && preg_match("~^[0-9]+$~", $_POST['id'])) { // match only positive integers
            $this->id = $_POST['id'];
        } else {
            $this->message .= error("Pas de numéro de suppression");
        }

        if (! isset($_POST['__lg_validation_confirm']) && $_POST['__lg_validation_confirm']=='Oui') {
            $this->message .= error("Pas de confirmation");
        }

        $this->error = ($this->message>"");

        return ! $this->error;
    }

    // -------------------------------------------------------------------

    function validate()
    {
    global $now, $adl_localgroup, $adl_title, $moderatorlist, $user;
        $this->moderated = true;
        $this->decision_time= $this->db->quote_smart($now);
        $this->save();
        $link = calendar_absolute_url("showlocalgroup.php?id=".$this->id);
        /* Send email */
        if (substr($this->submitter,0,4)!="http")
        {
            $mailBody =
                "Bonjour,\n\n" .
                wordwrap("Le {$adl_localgroup} que vous avez soumis a été modéré par " . $user->get_name() .
                ".\nIl est maintenant visible dans l'Agenda à l'adresse :\n") . $link . "\n\n" .
                wordwrap("Vous pouvez le modifier ultéreurement pour y ajouter des précisions en vous rendant à l'adresse :\n") .
                "  " . calendar_absolute_url("moderatelocalgroups.php?op=edit&id=" . $this->id . "&secret=" . $this->secret) . "\n\n" .
                wordwrap("Vous pouvez également l'annuler en vous rendant à l'adresse :\n") .
                "  " . calendar_absolute_url("moderatelocalgroups.php?op=cancelform&id=" . $this->id . "&secret=" . $this->secret) . "\n\n" .
                "=====================================================\n" .
                $this->formatAscii() . "\n" .
                "=====================================================\n\n" .
                "Merci de votre contribution à $adl_title et à bientôt !\n\n".
                "-- \nL'équipe de modération";

            calendar_mail ($this->submitter,
                "{$adl_localgroup} '" . $this->name . "' modéré",
                $mailBody,  $this->id);
            calendar_mail ($moderatorlist,
                "{$adl_localgroup} '" . $this->name . "' modéré",
                $mailBody,  $this->id);
        }

        if ($this->submitter != $this->contact && substr($this->contact,0,4)!="http")
        {
            $mailBody =
                "Bonjour,\n\n" .
                "Un {$adl_localgroup} dont vous êtes le contact a été publié dans {$adl_title}" .
                ".\nIl est maintenant visible à l'adresse\n" . $link . "\n\n" .
                "=====================================================\n" .
                $this->formatAscii() . "\n" .
                "=====================================================\n\n" .
                "*Nous vous invitons à déposer les événements à publier sur l'agenda.*\n\n".
                "À bientôt sur {$adl_title}.\n\n".
                "-- \nL'équipe de modération.";

            calendar_mail ($this->contact,
                "{$adl_localgroup} '" . $this->name . "' publié",
                $mailBody,
                $this->id);
        }

        return true;
    }

  // -------------------------------------------------------------------

    function confirmRejectFormHtml()
    {
    global $adl_localgroup;
        $html = <<<VALFORM
<p class="moderationheader">Confirmez-vous le rejet de ce {$adl_localgroup}&nbsp;?</p>
<div class="moderationbox">
    <form action="moderatelocalgroups.php?id={$this->id}" method="post">
        <input name="op" type="hidden" value="reject"/>
        <input name="id" type="hidden" value="{$this->id}"/>
        <p style="text-align: center;">Quel motif souhaitez-vous associer au rejet de ce {$adl_localgroup}&nbsp;?</p>
        <p>
        <label for="aa">Hors sujet <input id="aa" type="radio" name="reason" value="out-of-subject" onClick="hidetextarea();" /></label><br/>
        <label for="ab">Pas assez d'informations <input id="ab" type="radio" name="reason" value="not-enough-infos" onClick="hidetextarea();" /></label><br/>
        <label for="ac">{$adl_localgroup} déjà enregistré <input id="ac" type="radio" name="reason" value="duplicate" onClick="hidetextarea();" /></label><br/>
        <label for="ad">Raison spécifique (précisez) <input id="ad" type="radio" name="reason" value="custom" onClick="document.getElementById(\'customtext\').style.display = \'block\" /></label><br/>
        <textarea id="customtext" style="display: block;" cols="80" rows="10" name="customtext"></textarea>
        <script language=\'javascript\'>function hidetextarea() { document.getElementById(\'customtext\').style.display=\'none\ } hidetextarea();</script>
        <label for="ae">Spam ou test - ne pas avertir <input id="ae" type="radio" name="reason" value="hidden" checked="checked" onClick="hidetextarea();" /></label><br/>
        </p>
        <input name="__lg_rejection_confirm" type="submit" value="Rejeter"/>&nbsp;
        <input name="__lg_rejection_cancel" type="submit" value="Annuler"/>
        </form>
        </div>
    </form>
</div>

<div class="moderationbox">
VALFORM;
        $html .= $this->formatHTML();
        $html .= '</div>';
        return $html;
    }

  // -------------------------------------------------------------------

    /*
    * Came from Form (reject function)
    *
    */
    function fromConfirmRejectForm()
    {
        if (isset($_POST['id'])
            && preg_match("~^[0-9]+$~", $_POST['id'])) { // match only positive integers
            $this->id = $_POST['id'];
        } else {
            $this->message .= error("Pas de numéro de suppression");
        }

        if (! isset($_POST['__lg_validation_confirm']) && $_POST['__lg_validation_confirm']=='Oui') {
            $this->message .= error("Pas de confirmation");
        }

        $this->error = ($this->message>"");

        return ! $this->error;
    }

    // -------------------------------------------------------------------

    function reject()
    {
    global $now, $adl_localgroup, $adl_title, $moderatorlist;
        $this->validated = true;
        $this->decision_time= $this->db->quote_smart($now);
        $this->save();
        $link = calendar_absolute_url("showlocalgroup.php?id=".$this->id);

        /* Reason */
        switch ($_POST['reason'])
        {
            case "out-of-subject" :
              $reason = wordwrap($GLOBALS["adl_notrelated_message"]); break;
            case "not-enough-infos" :
              $reason = wordwrap($GLOBALS["adl_notenough_message"]); break;
            case "duplicate" :
              $reason = wordwrap($GLOBALS["adl_double_message"]); break;
            case "custom" :
              $reason = wordwrap("Toutefois, votre $adl_localgroup n'a".
                " pour le moment pas retenu l'attention des modérateurs,".
                " pour la raison suivante : " . stripslashes($_POST['customtext']));
                break;
            case "hidden" :
              $reason = "";
        }
        if ($reason != "")
        {
            /* Send email */
            $mailBody =
                "Bonjour,\n\n" .
                "Vous avez soumis un $adl_localgroup suivant dans {$adl_title}, et nous\n" .
                "vous remercions de cette contribution.\n\n" .
                $reason .
                "%s".
                "\n\nPour rappel, voici le contenu de votre $adl_localgroup :\n" .
                "=====================================================\n" .
                $this->formatAscii() . "\n" .
                "=====================================================\n\n" .
                "Avec tous nos remerciements pour votre contribution,\n\n" .
                "-- \nL'équipe de modération";

            $mailBodySubmitter = sprintf($mailBody,
                "\n\nPour toute réclamation, n'hésitez pas à contacter l'équipe de modérateurs\n" .
                "à l'adresse ". scramble_email($GLOBALS["moderatorlist"]). ".");

            if (substr($this->submitter,0,4) != "http")
            {
                calendar_mail ($this->submitter,
                "$adl_localgroup '" . stripslashes($this->title) . "' refusé",
                $mailBodySubmitter,
                $this->id);
            }

            calendar_mail ($moderatorlist,
                "$adl_localgroup '" . stripslashes($this->title) . "' refusé",
                sprintf($mailBody, ""),
                $this->id);
        }

        return true;
    }

  // -------------------------------------------------------------------
}


/****************************************************************/

class localgroups {

    var $localgroups;       // array of localgroup
    var $region_id,         // filters
        $department,
        $partOfName,
        $localgroup_id,
        $moderated;
    var $db;

    function __construct($db)
    {
        $this->db = $db;
        $this->raz();
        return false;
    }

  // -------------------------------------------------------------------
    function get()
    {
        $select = "SELECT DISTINCT id FROM {$GLOBALS['db_tablename_prefix']}localgroups AS lg".
            " LEFT JOIN {$GLOBALS['db_tablename_prefix']}localgroups_department ld ON ld.localgroup_id=lg.id".
            " LEFT JOIN {$GLOBALS['db_tablename_prefix']}region_department ON department=department_id";
        $where = " WHERE (moderated=". ($this->moderated ? 1 : 0). ")";

        if ($this->localgroup_id>0) {
            $where .= " AND (id=". $this->db->quote_smart($this->localgroup_id). ")";
        }
        elseif ($this->department>0)
        {
            $where .= " AND (department=". $this->db->quote_smart($this->department). ")";
        }
        elseif ($this->region_id>0)
        {
            if ($this->region_id==28)
            {
                $where .= " AND (region_id IS NULL)";
            } else {
                $where .= " AND (region_id=". $this->db->quote_smart($this->region_id). ")";
            }
        }
        elseif ($this->partOfName>"")
        {
            $where .= " AND (lg.name LIKE ". $this->db->quote_smart("%". $this->partOfName. "%"). ")";
        }

        $query = $select. $where.
            " ORDER BY ". ($this->moderated ? "region_id, department, name" : "region_id, submission_time").
            ($this->partOfName>"" ? " LIMIT 0,30" : " LIMIT 0,800");
        $result = $this->db->query ($query);

        while (($record = $this->db->fetchObject($result)))
        {
            $this->localgroups[] = new localgroup($this->db, $record->id);
        }
        $this->db->freeResult($result);
        return count($this->localgroups)>0;
    }

  // -------------------------------------------------------------------
    function listHtml()
    {
        $html = "<ul>\n";
        $region_id = $department = 0;
        foreach ($this->localgroups as $localgroup)
        {
            if ($region_id != $localgroup->region_id) {
                if ($region_id != 0) $html .= "</li></ul>\n";
                $html .= "<li><strong>{$localgroup->region}</strong><ul>";
                $region_id = $localgroup->region_id;
            }
            $html .= "  <li><a href='moderatelocalgroups.php?op=info&id={$localgroup->id}'>".
                "<i class='fas fa-users'></i>&nbsp;{$localgroup->name}</a> à {$localgroup->city} ".
                "(". $localgroup->formatReachHtml(). ")".
                "</li>\n";
        }
        $html .= "</ul>\n";
        return $html;
    }

  // -------------------------------------------------------------------
    function formatHtml($id)
    {
        $localgroup = new localgroup($this->db, $id);
        if (! $localgroup->error) {
            $html = $localgroup->actionsHtml();
            $html .= $localgroup->formatHtml();
        } else {
            $html = $localgroup->message;
        }
        return $html;
    }

  // -------------------------------------------------------------------
    function confirmValidateFormHtml($id)
    {
        $localgroup = new localgroup($this->db, $id);
        $html = $localgroup->confirmValidateFormHtml();
        return $html;
    }

  // -------------------------------------------------------------------
    function confirmRejectFormHtml($id)
    {
        $localgroup = new localgroup($this->db, $id);
        $html = $localgroup->confirmRejectFormHtml();
        return $html;
    }

  // -------------------------------------------------------------------
  // empty all filter variables
    private function raz()
    {
        unset($this->localgroups);
        $this->localgroups = array();
        $this->region_id =  0;
        $this->department = 0;
        $this->localgroup_id = 0;
        $this->partOfName = "";
        $this->moderated = true;
        return false;
    }

  // -------------------------------------------------------------------
  // Get localgroup list in a region
    function getFromRegion($region_id)
    {
        $this->raz();
        $this->region_id =  $region_id;
        return $this->get();
    }

  // -------------------------------------------------------------------
  // Get localgroup list in a department
    function getFromDepartment($department)
    {
        $this->raz();
        $this->department = $department;
        return $this->get();
    }

  // -------------------------------------------------------------------
  // Get localgroup list from part of name
    function getFromName($partOfName)
    {
        $this->raz();
        $this->partOfName = $partOfName;
        return $this->get();
    }

  // -------------------------------------------------------------------
  // Get localgroup list of one localgroup
    function getOne($localgroup_id)
    {
        $this->raz();
        $this->localgroup_id = $localgroup_id;
        return $this->get();
    }

  // -------------------------------------------------------------------
    function aroundListHtml()
    {
        $html = " <ul>";
        foreach ($this->localgroups as $localgroup)
          {
            $html .= "<li>";
            if ($localgroup->comment>"")
              {
                $html .= "<a href=\"showlocalgroup.php?id=". $localgroup->id;
                $html .= "\" title=\"". sprintf(_("Voir la fiche de %s"), $localgroup->name). "\">";
              } else {
                if ($localgroup->url>"")
                  {
                    $html .= "<a href=\"". $localgroup->url;
                    $html .= "\" title=\"". sprintf(_("Vers le site de %s"), $localgroup->name). "\">";
                  }
              }
            $html .= $localgroup->name;
            if ($localgroup->comment>"" | $localgroup->url>"")
              {
                $html .= "</a>";
              }
            $html .= " (". $localgroup->formatReachHtml(). ")</li>";
          }
        $html .= " </ul>";
        return $html;
    }

  // -------------------------------------------------------------------
    function formatCsvOsm()
    {
        $html = "lon\tlat\ttitle\tdescription\ticon\n";

        foreach ($this->localgroups as $localgroup)
        {
            $html .= $localgroup->formatCsvOsm();
        }
        return $html;
    }

  // -------------------------------------------------------------------
    function formatGeojson()
    {
        $html = "";
        foreach ($this->localgroups as $localgroup)
        {
            $html .= ($html=='' ? '' : ', '). $localgroup->toGeojson();
        }
        return $html;
    }

    // -----------------------------------------------------------------
    // Used to link event to localgroups in class.event.inc.php
    // event->edit()
    function formatJson()
    {
        $vals = array();
        foreach ($this->localgroups as $localgroup)
        {
            $vals[] = array(
                'id' => $localgroup->id,
                'label' => $localgroup->name. ' ('. $localgroup->department. ')',
                'value' => $localgroup->name. ' ('. $localgroup->department. ')',
            );
        }
        return json_encode($vals);
    }

    // -----------------------------------------------------------------
    // Present part of form to choose one localgroup
    function selectHTML()
    {
        $html = "";
        $html .= "<script type='text/javascript' src='js/jquery-3.2.1.min.js'></script>\n";
        $html .= "<script type='text/javascript' src='js/jquery-ui.min.js'></script>\n";
        $html .= "<script type='text/javascript' src='js/findLocalgroup.js'></script>\n";
        $html .= "<strong>". ucfirst($GLOBALS['adl_localgroup']). " :</strong> ";
        if ($this->localgroup_id>0)
        {
            $this->get();
            reset($this->localgroups);
            $localgroup = current($this->localgroups);
        }
        $html .="<span id='lg-container'>
            <input type='text' placeholder=\"Saisir une partie du nom du {$GLOBALS['adl_localgroup']}\"
                 id='lg_name' ";
        if (isset($localgroup))
        {
            $html .= "value='{$localgroup->name} ({$localgroup->department})' ";
        }
        $html .= "/>
            </span>
            <span id='loading' class='loading' style='display:none;'>
                <i class='fas fa-circle-notch fa-spin'></i></span>
            <input type='hidden' name='localgroup_id' value='{$this->localgroup_id}'/>
            </select>\n";
        return $html;
    }

}
