<?php

/*
 * Copyright 2004-2018
 * - Melanie Bats <melanie POINT bats CHEZ utbm POINT fr>
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * - Loic Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

include_once('inc/class.geocode.inc.php');
include_once('inc/class.region.inc.php');
include_once('inc/class.tagsHtml.inc.php');
include_once('inc/class.localgroup.inc.php');

class event {
  var $id;
  var $title,
      $description,
      $start_time, $start,
      $end_time, $end,
      $geocode,         // class geocode
      $place,           // text input by user
      $osmId,           // place id from geocode
      $address,
      $postalcode,
      $city,
      $department,
      $region,
      $country,
      $latitude,
      $longitude,
      $locality,        // portee : -1=locale 0=regionale 1=nationale 2=internationale
      $url,
      $contact,
      $submitter,
      $moderated,
      $secret,
      $decision_time,
      $submission_time;
  var $role;            // role of a localgroup for this event
  var $tags;            // array of tags
  var $localgroups;     // array of localgroups
  var $error, $message;
  var $db;

  // -------------------------------------------------------------------

  function __construct($db, $id=0)
  {
    $this->db = $db;
    $this->error = 0;
    $this->message = "";
    $this->geocode = new geocode($db);
    $this->localgroups = array();
    if ($id>0)
    {
      $this->id = $id;
      $this->get();
    }
    return ! $this->error;
  }

  // -------------------------------------------------------------------

  function get()
  {
    $query = "SELECT * FROM {$GLOBALS['db_tablename_prefix']}events WHERE id=" . $this->db->quote_smart($this->id);
    $result = $this->db->query ($query);
    if (! ($record = $this->db->fetchObject($result)))
    {
      $this->message = "Aucun évènement avec l'ID ". $this->id;
      $this->error = true;
      return false;
    }
    $this->title            = $record->title;
    $this->description      = $record->description;
    $this->start_time       = $record->start_time;
    $this->end_time         = $record->end_time;
    $this->start            = strtotime($this->start_time);
    $this->end              = strtotime($this->end_time);
    $this->address          = $record->address;
    $this->place            = $this->address;
    $this->postalcode       = sprintf("%'.05d", $record->postalcode);
    $this->city             = $record->city;
    $this->department       = $record->department;
    if ($this->department>0)
    {
        $region = new region($this->db);
        $this->region       = $region->getRegionFromDepartment($record->department);
        unset($region);
    } else {
        $this->region       = $record->region;
    }
    //$this->country           = $record->country;
    $this->latitude         = $record->latitude;
    $this->longitude        = $record->longitude;
    $this->locality         = $record->locality;
    $this->url              = $record->url;
    $this->contact          = $record->contact;
    $this->submitter        = $record->submitter;
    $this->moderated        = $record->moderated;
    $this->secret           = $record->secret;
    $this->decision_time    = $record->decision_time;
    $this->submission_time  = $record->submission_time;
    // tags
    $oTags = new tagsHtml($this->db);
    $this->tags             = $oTags->eventTagsList($this->id);
    // localgroups - not loaded
    $this->localgroups = array();
    $query = "SELECT le.* FROM {$GLOBALS['db_tablename_prefix']}localgroups_events le".
        " LEFT JOIN {$GLOBALS['db_tablename_prefix']}localgroups lg ON localgroup_id=lg.id".
        " WHERE (moderated=1) AND (event_id=". $this->db->quote_smart($this->id). ") ORDER BY role, lg.name";
    $result = $this->db->query ($query);
    while ($record = $this->db->fetchObject($result))
    {
        $localgroup = new localgroup($this->db);
        $localgroup->id = $record->localgroup_id;
        $localgroup->role = $record->role;
        $this->localgroups[] = $localgroup;
        unset($event);
    }
    $this->db->freeResult($result);

    $this->message = "";
    $this->error = false;
    return true;
  }

  // -------------------------------------------------------------------

  function save()
  {
    global $now;
    $sql = ($this->id>0 ? "UPDATE " : "INSERT INTO ");
    $sql .= "{$GLOBALS['db_tablename_prefix']}events SET " .
      "title=" .       $this->db->quote_smart ($this->title)                        . ", " .
      "start_time=" .  $this->db->quote_smart (date_timestamp2mysql ($this->start)) . ", ".
      "end_time=" .    $this->db->quote_smart (date_timestamp2mysql ($this->end))   . ", ".
      "description=" . $this->db->quote_smart ($this->description)                  . ", ".
      "address=" .     $this->db->quote_smart ($this->address)                      . ", ".
      "department=" .  $this->db->quote_smart (floor($this->postalcode/1000))       . ", ".
      "postalcode=" .  $this->db->quote_smart ($this->postalcode)                   . ", ".
      "city=" .        $this->db->quote_smart ($this->city)                         . ", ".
      "region=" .      $this->db->quote_smart ($this->region)                       . ", ".
      //"country=" .     $this->db->quote_smart ($this->country)                      . ", ".
      "latitude=" .    $this->db->quote_smart ($this->latitude)                     . "," .
      "longitude=" .   $this->db->quote_smart ($this->longitude)                    . "," .
      "locality=" .    $this->db->quote_smart ($this->locality)                     . ", ".
      "url=" .         $this->db->quote_smart ($this->url)                          . ", ".
      "contact=" .     $this->db->quote_smart ($this->contact)                      . ", ".
      "submitter=" .   $this->db->quote_smart ($this->submitter);
    if ($this->id==0)
      $sql .= ", moderated='0', ".
        "secret='" . ($this->secret = md5(uniqid(rand(), true))) . "', ".
        "submission_time=". $this->db->quote_smart($now);
    else
      $sql .= " WHERE id=" .    $this->db->quote_smart ($this->id);

    $ret = $this->db->query($sql);
    if ($ret == FALSE)
    {
      $this->error=true;
      $this->message = "La requête <i>" . $sql . "</i> a échoué";
      return false;
    }

    // get id if new
    if ($this->id==0) {
        $this->id = $this->db->insertid();
        $_is_modification = false;
    } else {
        $_is_modification = true;
    }

    // about tags ------------------------------------------------------
    if ($_is_modification) {
      // delete old
      $query = "DELETE FROM {$GLOBALS['db_tablename_prefix']}tags_events WHERE event_id='{$this->id}'";
      $result = $this->db->query($query);
      if (! $result)
      {
        $this->error=true;
        $this->message=_("Impossible de supprimer les anciens mots-clés");
        return false;
      }
    }
    // add new
    // manual tags
    $aTags = explode(" ", $this->tags);
    $aTagsKnown = array();
    // get tags which already exist
    $query = "SELECT id, name FROM {$GLOBALS['db_tablename_prefix']}tags WHERE name IN ('". implode("', '", $aTags). "')";
    $result = $this->db->query($query);
    if (! $result)
    {
      $this->error=true;
      $this->message=_("Impossible de lire les mots-clés");
      return false;
    }
    while ($record = $this->db->fetchObject($result))
    {
      $aTagsKnown[]=$record->name;
    }
    // Insert new tags
    if (count($aTagsNew = array_diff($aTags, $aTagsKnown))>0)
    {
      $query = "INSERT INTO {$GLOBALS['db_tablename_prefix']}tags (name) ".
        "VALUES ('". implode("'), ('", $aTagsNew). "')";
      $result = $this->db->query($query);
      if (! $result)
      {
        $this->error=true;
        $this->message=_("Impossible d'ajouter les mots-clés");
        return false;
      }
    }
    // make link between event and tags
    $query = "INSERT INTO {$GLOBALS['db_tablename_prefix']}tags_events (event_id, tag_id) ".
      "SELECT {$this->id}, id FROM {$GLOBALS['db_tablename_prefix']}tags ".
      "WHERE name IN ('". implode("','", $aTags). "')";
    $result = $this->db->query($query);
    if (! $result)
    {
      $this->error=true;
      $this->message=_("Impossible d'attribuer les mots-clés");
    }

    // about localgroups -----------------------------------------------
    if ($_is_modification) {
      // delete old
      $query = "DELETE FROM {$GLOBALS['db_tablename_prefix']}localgroups_events WHERE event_id='{$this->id}'";
      $result = $this->db->query($query);
      if (! $result)
      {
        $this->error = true;
        $this->message = _("Impossible de supprimer les liens avec les anciens ".
            $GLOBALS['adl_localgroups']);
      }
    }
    // add new
    // Insert new localgroups
    foreach ($this->localgroups as $localgroup)
    {
        $query = "INSERT INTO {$GLOBALS['db_tablename_prefix']}localgroups_events".
            " (localgroup_id, event_id, role) ".
            "VALUES (".
                $this->db->quote_smart($localgroup->id). ", ".
                $this->db->quote_smart($this->id). ", ".
                $this->db->quote_smart($localgroup->role).
            ")";

        $result = $this->db->query($query);
        if (! $result)
        {
            $this->error=true;
            $this->message=_("Impossible d'ajouter le lien avec le ".
                $GLOBALS['adl_localgroups']);
        }
    } // end foreach

    return ! $this->error;
  } // end function_save();

  // -------------------------------------------------------------------

  function delete()
  {
    $sql = "DELETE FROM {$GLOBALS['db_tablename_prefix']}events WHERE id=" . $this->db->quote_smart($this->id);
    $ret = $this->db->query($sql);
    if (! $ret)
    {
      $this->error=true;
      $this->message = returnError("Impossible de supprimer l'événement");
      error ("La requête <i>" . $sql . "</i> a échoué");
      return false;
    }
    // delete link between event and tags
    $query = "DELETE FROM {$GLOBALS['db_tablename_prefix']}tags_events ".
      "WHERE event_id=". $this->db->quote_smart($this->id);
    $ret = $this->db->query($query);
    if (! $ret)
    {
      error("Impossible de supprimer les mots-clés");
      // not required to return true
    }
    // else
    // clear tags table of tags not used

    // delete link between event and localgroups
    $query = "DELETE FROM {$GLOBALS['db_tablename_prefix']}localgroups_events ".
      "WHERE event_id=". $this->db->quote_smart($this->id);
    $ret = $this->db->query($query);
    if (! $ret)
    {
      error("Impossible de supprimer les liens avec les ".
        $GLOBALS['adl_localgroups']);
      // not required to return true
    }

    return true;
  }

  // -------------------------------------------------------------------

  function cancel()
  {
    global $now;

    $sql = "UPDATE {$GLOBALS['db_tablename_prefix']}events SET " .
      " moderated='0', decision_time=". $this->db->quote_smart($now).
      " WHERE id=" .    $this->db->quote_smart ($this->id);
    $ret = $this->db->query($sql);
    if ($ret == FALSE)
    {
      $this->error=true;
      $this->message = "La requête <i>" . $sql . "</i> a échoué";
      return false;
    }
    return ! $this->error;
  } // end cancel();

  // -------------------------------------------------------------------

  function edit($previewEnable=false, $submitEnable=true, $postUrl="submit.php")
  {
    global $adl_form_description_guide, $adl_form_tags_guide, $adl_localgroup, $adl_localgroups;

    echo "<form method=\"post\" name='event' action=\"{$postUrl}\">\n";

    echo "<script type=\"text/javascript\" src=\"js/jquery-3.2.1.min.js\"></script>\n".
      "<script type=\"text/javascript\" src=\"js/jquery-ui.min.js\"></script>";

    if (file_exists("js/tinymce"))
    {
      ?>
  <!-- tinyMCE -->
  <script language="javascript" type="text/javascript" src="js/tinymce/tinymce.min.js"></script>
  <script language="javascript" type="text/javascript">
      tinymce.init({
        selector: 'textarea',
        language : 'fr_FR',
        theme : 'modern',
        plugins : 'textcolor advlist autolink link image lists table charmap paste table preview',
        toolbar:  'undo redo | styleselect | bold italic forecolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | paste pastetext | code preview',
        menubar: false,
        width: '80%',
        height: 300,
        extended_valid_elements : [ 'a[name|href|target|title|onclick]',
          'img[class=myclass|!src|border:0|alt|title|width|height|style]' ],
      });
  </script>
  <!-- /tinyMCE -->
  <?php
    } // end if tiny_mce
?>
    <div class='form'>

        <fieldset><legend>Description de l'&eacute;v&eacute;nement</legend>

            <label for="__event_title" class='required only'>Titre*</label>
            <p class='legend'>Décrivez en moins de 5 mots votre évènement, sans y indiquer le lieu, la ville ni la date.</p>
            <input type="text" size="70" required name="__event_title" value="<?php echo $this->title;?>"/>
            <br/>

            <label for='__event_start_day' class='required only'>D&eacute;but*</label>
            <?php $this->generate_date_forms("start", $this->start); ?>
            <br/>
            <label for='__event_end_day' class='required only'>Fin*</label>
            <?php $this->generate_date_forms("end", $this->end); ?>
            <br/>
            <br/>
            <label for='__event_description' class='required only'>Description*</label>
            <p class='legend'>Décrivez de la manière la plus complète possible votre évènement.</p>
            <?php
            if (file_exists("js/tinymce"))
              echo "<noscript>". $adl_form_description_guide. "</noscript>\n";
            else
              echo $adl_form_description_guide. "\n";
            ?>
            <textarea rows="25" cols="70" name="__event_description"><?php echo $this->description;?></textarea>
            <br/>
            <label for='__event_url' class='required only'>URL*</label>
            <p class='legend'>Lien <b>direct</b> vers une page donnant plus d'informations sur
            l'évènement (lieu et horaire précis, programme détaillé...) au format <code>https://url</code>
            ou <code>http://url</code></p>
            <input type="url" size="70" required name="__event_url" value="<?php echo $this->url;?>"/>

        </fieldset>

        <fieldset><legend>Localisation</legend>
        <?php
        echo $this->geocode->mapInput('__event', $this); ?>
            <br/><label for='__event_locality' class='required only'>Portée*</label>
            <select name="__event_locality"><?php
              foreach ($GLOBALS["adl_locality"] as $key=>$value)
                echo "<option value=\"$key\"".
                    ($this->locality == $key ? " selected=\"selected\"" : "").
                    ">$value</option>\n";
            ?></select>

        </fieldset>

        <script type="text/javascript" src="js/findLocalgroup.js"></script>

        <fieldset><legend>Acteurs de l'organisation</legend>
            <label for="__event_lg" class='only'>Saisir les
            <?php echo $adl_localgroups; ?> et leur r&ocirc;le</label>
            <?php
            $nlg = 0;
            echo "<ul id='lg_list'>";
            foreach ($this->localgroups as $localgroup) {
                $oLocalgroup = new localgroup($this->db, $localgroup->id);
                echo "<li id='lg_n{$nlg}'>{$oLocalgroup->name} ({$oLocalgroup->department})\n".
                    " en tant que ". $GLOBALS["adl_lg_role"][$localgroup->role];
                echo "<input type='hidden' name='__event_lg_id[$nlg]' value='{$localgroup->id}' />";
                echo "<input type='hidden' name='__event_lg_role[$nlg]' value='{$localgroup->role}' />";
                echo " <a href='#' onClick='javascript:lg_delete({$nlg}); return false;'>retirer</a></li>\n";
                unset($oLocalgroup);
                $nlg++;
            } // endforeach ?>
            </ul>
            <span id='lg-container'>
            <input type='text' placeholder="Saisir une partie du nom du <?php echo $adl_localgroup; ?>"
                name='lg_name' id='lg_name' />
            </span>
            <span id='loading' class='loading' style='display:none;'>
                <i class='fas fa-circle-notch fa-spin'></i></span>
            <input type='hidden' name='localgroup_id' /> en tant que
            <select name='localgroup_role' title='Choisir le r&ocirc;le'>
                <option value=''></option>
                <?php // make known by javascript the php adl_lg_role variable content
                $js_roles = "<script type='text/javascript'>\nfunction role_name(role_key)\n{\n";
                foreach ($GLOBALS["adl_lg_role"] as $key=>$value) {
                    echo "<option value='$key'>$value</option>\n";
                    $js_roles .= "  if (role_key == '". $key. "') { return '". $value. "'; };\n";
                }
                $js_roles .= "}\n</script>\n";
            ?>
            </select> <a href='#' name='lg_add'
                onClick='javascript:lg_add(<?php echo $nlg; ?>); return false;'>ajouter</a>
            <?php echo $js_roles; ?>

            <br/>
            <label for='__event_contact' class='required only'>Contact*</label>
            <p class='legend'>Adresse courriel de contact (elle sera
            transformée de manière à éviter le spam) <b>ou</b>
            Lien direct vers un formulaire de contact au format
            <code>https://url</code> ou <code>http://url</code></p>
            <input type="text" size="70" required name="__event_contact"
            value="<?php echo $this->contact;?>"/>

            <br/>
            <label for='__event_submitter' class='only'>Soumetteur</label>
            <p class='legend'>Adresse courriel du soumetteur de
            l'évènement (qui ne sera utilisée que par les
            modérateurs pour informer de la validation ou du rejet).
            A défaut, l'adresse de contact sera utilisée.</p>
            <input type="text" size="70" name="__event_submitter"
            value="<?php echo $this->submitter;?>"/>

        </fieldset>


        <fieldset><legend>Mots-clés</legend>
  <?php
    $aCategoryTags = new tagsHtml($this->db);
    $aCategoryTags->selectWithCategoryTags(explode(" ", $this->tags));
    foreach ($aCategoryTags->HTML as $category_id=>$aSelect)
      echo "<label for='__event_tags_{$aSelect['categoryName']}' class='only'>".
        ucfirst($aSelect['categoryName']). "</label>".
        "<p class='legend'>{$aSelect['categoryDescription']}</p>".
        "{$aSelect['HTMLSelect']}<br/>\n";
    $aTags = explode(' ', $aCategoryTags->tagsRest);
  ?>
        <label for='__event_tags' class='only'>Mots-cl&eacute;s
            non cat&eacute;goris&eacute;s</label>
        <p class='legend'><?php
        echo $adl_form_tags_guide;
        echo "</p>\n";
        $oTags = new tagsHtml($this->db);
        $oTags->selectCategoryTags($aTags, 0, $fieldName="tags[0]", $multiple=true);

    // campagne en cours, on présente les tags associés
    include_once('inc/class.campaign.inc.php');
    $campaigns = new campaigns($this->db);
    echo ($campaignInProgress = $campaigns->returnTagExplain());

    echo $oTags->HTML;
    $tags = $oTags->tagsRest;
    ?><br /><input type="text" size="70" name="__event_tags"
        value="<?php echo $tags; ?>" />
    </fieldset>

   <p class='legend'><?php
    if ($previewEnable)
      echo "<input name=\"__event_preview\" type=\"submit\" value=\"Aperçu\" />";
    echo "<input name=\"__event_save\" type=\"submit\" value=\"Valider\" ".
      (! $submitEnable ? "disabled='disabled'" : ""). " />";
  ?></p>

  </form>
  <?php
  } // end function_edit

  // -------------------------------------------------------------------

  /**
   * Format an event in an ASCII format, suitable for e-mails
   *
   * @return An ASCII description of the event
   */
  function formatAscii()
  {
    $title       = stripslashes($this->title);
    $start       = date_timestamp2humanreadable($this->start);
    $end         = date_timestamp2humanreadable($this->end);
    $address     = stripslashes($this->address);
    $postalcode  = stripslashes($this->postalcode);
    $city        = stripslashes($this->city);
    $oRegion = new region($this->db, $this->region);
    $region      = stripslashes($oRegion->name);
    $description = html_entity_decode(stripslashes($this->description));
    $url         = stripslashes($this->url);
    $contact     =  (strncmp ($this->contact, "http://", 7)
        && strncmp($this->contact, "https://", 8)) ?
        scramble_email(stripslashes($this->contact)) :
        stripslashes($this->contact);
    $submitter   = scramble_email(stripslashes($this->submitter));
    $tags        = stripslashes($this->tags);

    $str =
      "Titre       : " . $title . "\n" .
      "Début       : " . $start . "\n".
      "Fin         : " . $end . "\n" .
      "Adresse     : " . $address . "\n" .
      "Code postal : " . $postalcode . "\n" .
      "Ville       : " . $city . "\n" .
      "Région      : " . $region . "\n" .
      "URL         : " . $url . "\n".
      "Contact     : " . $contact . "\n" .
      "Soumetteur  : " . $submitter . "\n" .
      "Mots-clés   : " . $tags . "\n" .
      "Description : \n " . wordwrap(preg_replace ("/\n/", "\n ",
        strip_tags(utf8_encode($description))));

    return $str;
  }

  // -------------------------------------------------------------------

  /**
   * Format an event so that it can be displayed in a nice fashion
   *
   * @param db Database handle
   *
   * @param title Title of the event
   *
   * @param start Starting time (timestamp format)
   *
   * @param end Ending time (timestamp format)
   *
   *
   * @return The HTML code that corresponds to the formatted event
   */
  function formatHtml($moderation = FALSE)
  {
    global $session;

    $title       = stripslashes($this->title);
    //$start       = date_timestamp2humanreadable($this->start);
    //$end         = date_timestamp2humanreadable($this->end);
    $address     = stripslashes($this->address);
    $postalcode  = stripslashes($this->postalcode);
    $city        = stripslashes($this->city);
    $regionO = new region($this->db, $this->region);
    $region      = stripslashes($regionO->name);
    //$description = stripslashes($this->description);
    $url         = stripslashes($this->url);
    $submitter   = scramble_email(stripslashes($this->submitter));
    if (strncmp ($this->contact, "http://", 7) && strncmp($this->contact, "https://", 8))
    {
        $contact = "mailto:". scramble_email(stripslashes($this->contact));
    } else {
        $contact = stripslashes($this->contact);
    }
    $start_day = onlyday_timestamp2humanreadable($this->start);
    if ($start_day == onlyday_timestamp2humanreadable($this->end)) {
      $date = "<p>Le " .  $start_day . ", de "
        . onlyhour_timestamp2humanreadable($this->start) . " à "
        . onlyhour_timestamp2humanreadable($this->end) . ".</p>\n";
    } else {
      $date = "<p>Du " . date_timestamp2humanreadable($this->start)
        . " au " . date_timestamp2humanreadable($this->end) . ".</p>\n";
    }

    $result  = "<h2><i>" . $city . "</i> : " . $title . "</h2>\n\n";
    $result .= "<h3>Date et lieu</h3>\n";
    $result .= $date;

    $result .= "<p>à $address - $postalcode <i><a href=\"http://fr.wikipedia.org/wiki/".
                urlencode(str_replace(' ', '_', $city)) . "\">"
              . $city . "</a></i>, <a href=\"http://fr.wikipedia.org/wiki/"
              . $region . "\">" . $region . "</a></p>\n\n";
    $result .= "<h3>Description</h3>\n";
    $result .= $this->description . "\n\n";
    $result .= "<h3>Informations</h3>\n";
    $result .= "<p>Site Web: <a href=\"" . $url . "\">" . $url . "</a></p>\n";

    $result .= "<h3>Organisateurs</h3>";
    if (count($this->localgroups)>0) {
        $result .= "<p>". ucfirst($GLOBALS['adl_localgroups']). " et leur r&ocirc;le&nbsp:<ul>\n";
        foreach ($this->localgroups as $localgroup) {
            $olocalgroup = new localgroup($this->db, $localgroup->id);
            $result .= "<li><a href='". calendar_absolute_url("showlocalgroup.php?id=".
                $localgroup->id). "'>{$olocalgroup->name} ({$olocalgroup->department})</a>\n".
                " en tant que ". $GLOBALS["adl_lg_role"][$localgroup->role].
                "</li>\n";
            unset($olocalgroup);
        } // endforeach
        $result .= "</ul></p>\n";
    }
    $result .= "<p>Contact: <a href=\"" . $contact . "\">" . $contact . "</a></p>\n";

    if ($session->exists("agenda_libre_id")) {
        $result .= "<p>Soumetteur&nbsp;: {$submitter}</p>\n";
    }

    if ($moderation)
      $result .= "<p>Évènement à portée <b>".
        $GLOBALS["adl_locality"][$this->locality]. "</b></p>";

    if ($this->tags != "")
    {
        $result .= "<h3>Mots-cl&eacute;s</h3>\n";
        $oTags = new tagsHtml($this->db);
        $result .= $oTags->formatTagsHtml($this->tags);
    }

    return $result;
  }

  // -------------------------------------------------------------------

  /*
   * Came from From (edit function)
   *
   */
  function fromForm()
  {
    /* Convert form date to timestamp */
    if (isset($_POST['__event_start_date']))
    {
      $this->start = date_humanreadable2timestamp(
        $_POST['__event_start_date']. " ".
        $_POST['__event_start_time']. ":00"
      );
    }

    if (isset($_POST['__event_end_date']))
    {
      $this->end = date_humanreadable2timestamp(
        $_POST['__event_end_date']. " ".
        $_POST['__event_end_time']. ":00"
      );
    }

    $afieldName = array('title', 'description',
      'place', 'osmId', 'address', 'postalcode', 'city', 'region', 'locality',
      'url', 'contact', 'submitter');

    foreach ($afieldName as $fieldName)
    {
      if (isset($_POST['__event_'. $fieldName]))
        $this->$fieldName = trim(stripslashes(strip_tags($_POST['__event_'. $fieldName],
          $fieldName=='description' ? "<p><strong><em><br/><a><ul><li><ol><b><i>" : "")));
      if (isset($_POST['__event'][$fieldName]))
        $this->$fieldName = stripslashes(strip_tags($_POST['__event'][$fieldName]));
    }
    if (isset($_POST['__event']['lon'])) $this->longitude = $_POST['__event']['lon'];
    if (isset($_POST['__event']['lat'])) $this->latitude = $_POST['__event']['lat'];

    if (isset($_POST['__event_tags']))
    { // format tags
      $event_tags = $_POST['__event_tags'];
      if (isset($_POST['tags']) && is_array($_POST['tags']))
      {
        foreach($_POST['tags'] as $aTags)
          if (is_array($aTags))
            $event_tags .= " ". implode(' ', $aTags);
          else
            $event_tags .= " ". $aTags;
      }
      $this->tags = stripslashes(strip_tags(trim(str_replace("  ", " ", $event_tags))));
    }
    else
      $this->tags = "";

    unset($this->localgroups);
    $this->localgroups = array();
    if (isset($_POST['__event_lg_id']))
    { // get localgroups
      $event_lg_id = $_POST['__event_lg_id'];
      $event_lg_role = $_POST['__event_lg_role'];
      foreach ($_POST['__event_lg_id'] as $form_id=>$lg_id) {
        $lg = new localgroup($this->db, $lg_id);
        $lg->role = $event_lg_role[$form_id];
        $this->localgroups[] = $lg;
      }
    }

    return true;
  } // end function_fromForm

  // -------------------------------------------------------------------

  // Check information from event edit form
  function check()
  {
  global $session, $tagMaxSize, $tagMinSize;
    $error_cnt = 0;

    if (! $this->title)
      {
        $this->message .= returnError("Titre vide");
        $error_cnt++;
      }

    if (stristr($this->title, "viagra"))
      {
        $this->message .= returnError ("Les spammeurs, dehors");
        $error_cnt++;
      }

    if (! $this->description)
      {
        $this->message .= returnError ("Description vide");
        $error_cnt++;
      }

    if (! $this->place)
      {
        $this->message .= returnError ("Adresse vide");
        $error_cnt++;
      }
    elseif (! $this->longitude)
      {
        $this->message .= returnWarning ("Adresse non localis&eacute;e");
      }

    if (! $this->url)
      {
        $this->message .= returnError ("URL vide");
        $error_cnt++;
      }
    elseif (strncmp ($this->url, "http://", 7) && strncmp($this->url, "https://", 8))
      {
        $this->message .= returnError ("URL ne commençant pas par http:// ou https://");
        $error_cnt++;
      }

    if ($this->start <= time())
      {
        $mes = "Le début de l'évènement est dans le passé.";
        if (! isset($session) or ! $session->exists('agenda_libre_id'))
        {
            $this->message .= returnError($mes);
            $error_cnt++;
        } else {
            $this->message .= returnWarning($mes);
        }
      }

    if ($this->end <= time())
      {
        $mes = "La fin de l'évènement est dans le passé.";
        if (! isset($session) or ! $session->exists('agenda_libre_id'))
        {
            $this->message .= returnError($mes);
            $error_cnt++;
        } else {
            $this->message .= returnWarning($mes);
        }
      }

    if ($this->end <= $this->start)
      {
        $this->message .= returnError ("La fin de l'évènement est avant le début");
        $error_cnt++;
      }

    if (! preg_match("~^([-A-Za-z0-9_+.]*)@([-A-Za-z0-9_]*)\.([-A-Za-z0-9_.]*)$~", $this->contact))
      {
        if (strncmp ($this->contact, "http://", 7) && strncmp($this->contact, "https://", 8))
            {
                $this->message .= returnError ("Contact, ni courriel valide, ni URL valide commençant pas par http:// ou https://");
                $error_cnt++;
            }
      }

    if (!empty($this->submitter) && !
      preg_match("~^([-A-Za-z0-9_+.]*)@([-A-Za-z0-9_]*)\.([-A-Za-z0-9_.]*)$~",
      $this->submitter))
      {
        $this->message .= returnError ("Courriel du soumetteur invalide");
        $error_cnt++;
      }

    $tags = str_replace(" +", " ", $this->tags);
    foreach(explode(" ", $tags) as $tag)
      {
        if ($tag == "")
          continue;

        if (! preg_match("~^[a-z0-9\-]*$~", $tag))
          {
            $this->message .= returnError("Tag '" . $tag . "' invalide. ".
              "Les tags ne doivent contenir que des lettres minuscules ".
              "sans accent, des chiffres ou des tirets.");
            $error_cnt++;
          }
        if (strlen($tag) < $tagMinSize || strlen($tag) > $tagMaxSize)
          {
            $this->message .= returnError(sprintf("Tag '" . $tag . "' trop court ou trop long. ".
                "La longueur des tags doit être de %s à %s caractères.", $tagMinSize, $tagMaxSize));
            $error_cnt++;
          }
      }

    $this->error = $error_cnt;
    return ($error_cnt == 0);
  } //

  // -------------------------------------------------------------------

  function notifySubmitter ()
  {
    $mail_title = "Votre évènement : '" . $this->title . "' est en attente de modération";

    $mail_body = "Bonjour,\n\n" .
      wordwrap("Votre évènement intitulé '" . $this->title .
               "', qui aura lieu le '" . date_timestamp2humanreadable($this->start) .
               "' a bien été enregistré dans ". $GLOBALS['adl_title']. ". ".
               "L'équipe de modération le prendra en charge très prochainement. " .
               "Pendant la modération et après celle-ci si votre évènement est validé, " .
               "vous pouvez éditer votre évènement à l'adresse :\n" .
               "  " . calendar_absolute_url("editevent.php?id=" .
                $this->id . "&secret=" . $this->secret) . "\n\n" .
               "et vous pouvez l'annuler en utilisant l'adresse :\n" .
               "  " . calendar_absolute_url("cancelevent.php?id=" .
                $this->id . "&secret=" . $this->secret) . "\n\n") .
      "Merci de votre participation !\n" .
      "-- ". $GLOBALS["adl_title"];

    calendar_mail($this->submitter, $mail_title, $mail_body, $this->id);

    return true;
  }

  // -------------------------------------------------------------------

  function toIcal()
  {
    global $timezone, $adl_uid_end;
    $this->url = calendar_absolute_url("showevent.php?id=" . $this->id);
    $ret = "BEGIN:VEVENT\r\n".
      "DTSTART;TZID={$timezone}:" .  date ('Ymd\THi\0\0', $this->start) . "\r\n".
      "DTEND;TZID={$timezone}:"   .  date ('Ymd\THi\0\0', $this->end) . "\r\n".
      "UID:" . $this->id . "@$adl_uid_end\r\n".
      "SUMMARY:" .  str_replace(",", "\\,", $this->title) . "\r\n".
      "URL:" .  $this->url . "\r\n".
      "DESCRIPTION:" .  $this->description . "\r\n".
      "LOCATION:" . $this->city . "\r\n".
      "END:VEVENT\r\n";
    return $ret;
  }

   /*
   * Function that replaces all xml-incompatible characters with they
   * XML-compatible equivalent
   */
  private function xmlentities($string, $quote_style=ENT_COMPAT)
  {
     return htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
  }

  function toRss($map)
  {
    $start_day = onlyday_timestamp2humanreadable($this->start);
    $end_day   = onlyday_timestamp2humanreadable($this->end);

    $ret = "  <item rdf:about=\"" .
      calendar_absolute_url("showevent.php?id=" . $this->id) . "\">\n";

    $ret .= "  <title>";
    $ret .= $this->xmlentities($this->city) . " : " . $this->xmlentities($this->title);
    if ($start_day == $end_day)
      $ret .= ", le " . $start_day;
    else
      $ret .= ", du " . $start_day . " au " . $end_day;
    $ret .= "</title>\n";

    $ret .= "   <link>" .
      calendar_absolute_url("showevent.php?id=" . $this->id) . "</link>\n";
    $ret .= "   <dc:identifier>" . $this->id .
      "@agendadulibre.org</dc:identifier>\n";
    $evt_timezone = preg_replace("~^\+([0-9][0-9])([0-9][0-9])$~", "+\\1:\\2",
               date('O', $this->start));
    $evt_date     =str_replace(" ", "T", $this->decision_time);
    $ret .= "   <dc:date>" . $evt_date . $evt_timezone . "</dc:date>\n";
    if ($this->longitude && $this->latitude)
      $ret .= "   <georss:point>" . $this->latitude . " " . $this->longitude . "</georss:point>\n";

    $ret .= "   <description>\n";
    $ret .= $this->xmlentities($map ? substr(strip_tags($this->formatHTML()),0,128) : strip_tags($this->formatHTML()));
    $ret .= "   </description>\n";
    if (! $map)
      {
        $ret .= "   <content:encoded>\n";
        $ret .= $this->xmlentities($this->formatHTML());
        $ret .= "   </content:encoded>\n";
      }
    $ret .= "  </item>\n\n";

    return $ret;
  }

  function toKml()
  {
    // title
    $start_day = onlyday_timestamp2humanreadable($this->start);
    $end_day   = onlyday_timestamp2humanreadable($this->end);
    $name = $this->xmlentities($this->city) . " : " . $this->xmlentities($this->title);
    if ($start_day == $end_day)
      $name .= ", le " . $start_day;
    else
      $name .= ", du " . $start_day . " au " . $end_day;

    // description
    $link = calendar_absolute_url("showevent.php?id=". $this->id);
    $description = "<a href=\"{$link}\">$link</a><br/>\n".
        $this->xmlentities(substr(strip_tags($this->formatHTML()),0,128));

    return <<<PARTOFKML
    <Placemark>
        <name>{$name}</name>
        <description><![CDATA[{$description}]]></description>
        <styleUrl>#eventIcon</styleUrl>
        <Point>
            <coordinates>{$this->latitude},{$this->longitude},0</coordinates>
        </Point>
    </Placemark>\n
PARTOFKML;
  }

  function toXml($xml)
  {
      $xml->startElement("event");
      $xml->writeElement("title",       $this->title);
      $xml->writeElement("start-time",  $this->start_time);
      $xml->writeElement("end-time",    $this->end_time);
      $xml->writeElement("city",        $this->city);
      $region = new region($this->db, $this->region);
      $xml->writeElement("region",      $region->name);
      unset($region);
      $xml->writeElement("locality",    $this->locality);
      $xml->writeElement("url",         $this->url);
      $xml->writeElement("tags",        $this->tags);
      $xml->writeElement("contact",     $this->contact);
      $xml->writeElement("adlurl",      calendar_absolute_url("showevent.php?id=" . $this->id));
      $xml->startElement("description");
        $xml->writeCData($this->description);
      $xml->endElement();
      $xml->endElement();
      return $xml;
  }

  // -------------------------------------------------------------------

  function toGeojson()
  {
    // title
    $start_day = onlyday_timestamp2humanreadable($this->start);
    $end_day   = onlyday_timestamp2humanreadable($this->end);
    $name = $this->xmlentities($this->city) . " : " . $this->xmlentities($this->title);
    if ($start_day == $end_day)
      $name .= ", le " . $start_day;
    else
      $name .= ", du " . $start_day . " au " . $end_day;

    $properties = new \StdClass();
    $properties->id = $this->id;
    $properties->name = $name;
    $properties->href = calendar_absolute_url("showevent.php?id=". $this->id);
    $properties->start_time = $this->start_time;
    $properties->end_time = $this->end_time;
    $properties->place_name = $this->place;
    $properties->address = $this->address;
    $properties->city = $this->city;
    $properties->postalcode = $this->postalcode;
    $properties->tags = explode(" ", $this->tags);
    $properties->description = $this->description;

    $JSON_properties = json_encode($properties);

    return <<<PARTOFGEOJSON
    { "type": "Feature",
      "properties": {$JSON_properties},
      "geometry": {
        "type": "Point",
        "coordinates": [ {$this->longitude}, {$this->latitude} ]
      }
    }
PARTOFGEOJSON;
  }

  // -------------------------------------------------------------------

  function toJs()
  {
    return "document.write(\"".
      "  <li>". strftime ("%d/%m/%Y", $this->start).
      " <a href='".
      calendar_absolute_url("showevent.php?id={$this->id}"). "'>".
      "<b>" . htmlentities(substr(utf8_decode(stripslashes($this->city)),0,15), ENT_COMPAT | ENT_HTML401, 'ISO-8859-15') . "</b>: ".
      htmlentities(substr(utf8_decode(stripslashes($this->title)),0,35), ENT_COMPAT | ENT_HTML401, 'ISO-8859-15').
      "</a>". "</li>\\n\");\n";
  }

    function generate_date_forms ($ident, $current_time)
    {
      echo "<input type='date' id='__event_{$ident}_date' ".
        "name='__event_{$ident}_date' value='".
        date_timestamp2mysqldate($current_time). "' />\n";
      echo "<input type='time' id='__event_{$ident}_time' ".
        "name='__event_{$ident}_time' data-time-format='H:i' value='".
        date_timestamp2humanreadabletime($current_time). "'/>\n";
    }

  function toHtmlList()
  {
    $this->get();
    $result  = "<li>";
    $result .= "<a href=\"showevent.php?id=" . $this->id . "\">";
    $result .= stripslashes($this->title);
    $result .= "</a>";
    $result .= " en tant que ". $GLOBALS['adl_lg_role'][$this->role];
    $startday = onlyday_timestamp2humanreadable(date_mysql2timestamp($this->start_time));
    $endday   = onlyday_timestamp2humanreadable(date_mysql2timestamp($this->end_time));
    $result .= "<br/>";
    if ($startday == $endday)
      $result .= "le " . $startday . " ";
    else
      $result .= "du " . $startday . " au " . $endday . " ";
    $result .= " à " . $this->city;
    $result .= "</li>\n";
    return $result;
  }
}

?>
