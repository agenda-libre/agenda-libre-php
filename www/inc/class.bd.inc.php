<?php

/*
 * Copyright 2005-2015
 * - Mélanie Bats <melanie POINT bats CHEZ utbm POINT fr>
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * - Loic Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Perform a SQL query
 *
 * @param[in] The SQL query to be performed
 */

class db
{
    var $db_link;

  function __construct()
  {
    global $db_host;
    global $db_user;
    global $db_pass;
    global $db_name;

    $this->db_link = @new mysqli($db_host, $db_user, $db_pass, $db_name);
    if ($this->db_link->connect_errno)
      {
          echo "Erreur de connexion (".
            $this->db_link->connect_errno . ") ". $this->db_link->connect_error;
          die();
      }

    $this->query("set names 'utf8'");
  }

  function query ($query)
  {
    if( ($result = $this->db_link->query($query)) == FALSE)
      {
        echo "Probleme dans la syntaxe de $query : " . $this->db_link->error . "\n";
        return 0;
      }

    return $result;
  }

  function insertid ()
  {
    return $this->db_link->insert_id;
  }

  function fetchObject ($result)
  {
    return $result->fetch_object();
  }

  function fetchArray ($result)
  {
    return $result->fetch_array();
  }

  function fetchRow ($result)
  {
    return $result->fetch_row();
  }

  function freeResult ($result)
  {
    return $result->free_result();
  }

  function numRows ($result)
  {
    return $result->num_rows;
  }

  function getOne($result) {
    $row = $this->fetchArray($result);
    return $row[0];
  }

  /**
   * Converts the argument of an SQL request in a format accepted by MySQL.
   *
   * @param[in] value String or integer to use as argument
   *
   * @return The string to use in the request
   */
  function quote_smart($value)
  {
    if (get_magic_quotes_gpc())
      $value = stripslashes($value);

    if (!is_numeric($value))
      $value = "'" . $this->db_link->real_escape_string($value) . "'";

    return $value;
  }
} // end class
