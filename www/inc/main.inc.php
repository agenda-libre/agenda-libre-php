<?php

/*
 * Copyright 2005-2015
 * - Mélanie Bats <melanie POINT bats CHEZ utbm POINT fr>
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * - Loic Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

// Report all errors - easier to debug
#error_reporting(0); # Prod
error_reporting(E_ALL); # Debug
#error_reporting(E_ALL & ~E_DEPRECATED ^ E_STRICT); # Devel

ob_start();

// load main config file
include('inc/config.inc.php');

/*
 * Return a string with path and name to file in path theme if
 * exists else to default path
 *
 * $filename param could be 'text/text.inc.php'
 * or 'template/header.php' ...
 */
function _themeFile($filename)
  {
    global $default_theme, $theme;

    if (is_file(/*'./'.*/ $theme. '/'. $filename))
      {
        return /*'./'.*/ $theme. '/'. $filename;
      }
    elseif (is_file(/*'./'.*/ $default_theme. '/'. $filename))
      {
        return /*'./'.*/ $default_theme. '/'. $filename;
      }
    else
      {
        die(/*'./'.*/ $default_theme. '/'. $filename. " introuvable");
      }
  }

// load common functions
include('inc/funcs.inc.php');

// load html and settings
include(_themeFile("text/text.inc.php"));

// load database functions
include("inc/class.bd.inc.php");
// initiate database connexion
$db = new db();


if(date_default_timezone_set($timezone) == false)
  {
    echo "timezone setting error";
  }
calendar_setlocale();
$now = date("Y-m-d H:i:s");

/* Make all webpage private of set in config.inc.php */
include("inc/class.session.inc.php");
$session = new session();

if (isset($_GET['disconnect']))
  {
    $session->destroy();
    header("Location:" . calendar_absolute_url());
  }

include("inc/class.user.inc.php");
$user = new user($db, $session);

// First use. Create user.
if ($noUser = $user->is_table_empty())
{
  if (basename($_SERVER['SCRIPT_NAME']) != 'moderateusers.php') {
    header("Location:" . calendar_absolute_url('moderateusers.php?op=first'));
    exit;
  }
}

if (!$noUser && (($privateAgenda && (!isset($privatePage) || $privatePage!=false))
  || (isset($privatePage) && $privatePage==true)))
  {
    $user->identify();
  }
