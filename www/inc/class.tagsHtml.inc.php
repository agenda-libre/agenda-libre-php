<?php

/*
 * Copyright 2010-2017
 * - Loic Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

class tagsHtml {
    var $HTML,
        $tagsSelect,
        $tagsRest;
    var $db;

    function __construct($db)
    {
        $this->db = $db;
    }

    // Function to choose tags in categories (only one per category)
    // $aTags is an array (tag_name)
    // output is array (categoryName=>, categoryDescription=>, HTMLSelect=>)
    public function selectWithCategoryTags($aTags, $multiple=true)
    {
      // TODO Select only tags used twice or more.
      // Select all categories
      $ret = $this->db->query ("SELECT id, name, description ".
       "FROM {$GLOBALS['db_tablename_prefix']}tags_categories ".
       "ORDER BY name");
      if ($ret == FALSE)
      {
        error ("Erreur lors de la recherche des mots-clés");
        put_footer();
        exit;
      }

      $category_id=-1;
      $return = array();
      $tagsSelect = "";

      while ($row = $this->db->fetchObject($ret))
      {
        $oneline = new tagsHtml($this->db);
        $oneline->selectCategoryTags($aTags, $row->id,
          $fieldName="tags[". $row->name. "]", $multiple, $row->name);
        $tagsSelect .= " ". $oneline->tagsSelect;

        $return[$row->id]["categoryName"] = $row->name;
        $return[$row->id]["categoryDescription"] = ($row->description!="" ? $row->description : "");
        $return[$row->id]["HTMLSelect"] = $oneline->HTML;

      } // end while
      $this->db->freeResult($ret);

      $this->HTML = $return;
      $this->tagsSelect = $tagsSelect; //implode(' ', $tagsSelect);
      $this->tagsRest = implode(' ', array_diff($aTags, explode(' ', $tagsSelect)));
      //$oTags->tagsRest = $oneline->tagsRest;

      return $this;
    }

    // Function to choose tags
    // $aTags is an array (tag_name) of tags to be selected/checked
    // output if HTML string select/checkboxes part of form
    public function selectCategoryTags($aTags=array(), $categoryId=0, $fieldName="tags[0]", $multiple=false, $categoryName="")
    {
      global $now;
      // TODO Select only tags used twice or more.
      // Or sort and class tags in alphabetical optgroup
      // Count how many tags
      $query = "SELECT id FROM {$GLOBALS['db_tablename_prefix']}tags";
      $ret = $this->db->query($query);
      $countTags = $this->db->numRows($ret);
      $this->db->freeResult($ret);

      // More than 20, select most recent and used twice or more
      $limitTags = ($countTags>20);

      $query = "SELECT name, COUNT(event_id) AS n, ta.description ".
        "FROM {$GLOBALS['db_tablename_prefix']}tags AS ta ".
        "LEFT JOIN {$GLOBALS['db_tablename_prefix']}tags_events ON tag_id=ta.id ";
      if ($limitTags && $categoryId==0) {
          // only tags more recent than one year old
        $query .= "LEFT JOIN {$GLOBALS['db_tablename_prefix']}events AS ev ON ev.id=event_id ".
          "WHERE ev.moderated=1 AND category_id='{$categoryId}' AND TO_DAYS(".
          $this->db->quote_smart($now). ") - TO_DAYS(ev.start_time) <= 365 ";
      } else {
        $query .= "WHERE category_id='{$categoryId}' ";
      }
      $query .= "GROUP BY ta.id ORDER BY name";
      $ret = $this->db->query ($query);
      if ($ret == FALSE)
      {
        error ("Erreur lors de la recherche des mots-clés");
        put_footer();
        exit;
      }

      $tagsSelect = array();
      $n=0;

      $skeleton = "<label for='%1\$s' title=\"%4\$s\">%1\$s</label>". ($multiple ?
        "<input type=\"checkbox\" name=\"{$fieldName}[%3\$s]\" %2\$s value=\"%1\$s\" id=\"%1\$s\" title=\"%4\$s\" /> &emsp; " :
        "<option value=\"%1\$s\" %2\$s>%1\$s</option>");

      $return = ($multiple ? "" : "<select name='{$fieldName}'>\n<option></option>\n");

      while ($row = $this->db->fetchObject($ret))
      {
        // is this tag in $aTags = checked/selected
        $inArray = in_array($row->name,
            $categoryName>"" && in_array($categoryName, $aTags) && is_array($aTags[$categoryName]) ?
            $aTags[$categoryName] : $aTags);

        // only tags used twice or more
        if (!$limitTags || $categoryId>0 || $row->n>1)
        {
          $return .= sprintf($skeleton, $row->name, ($inArray ? ($multiple ? "checked='checked'" : "selected='selected'") : ""), $n, $row->description);
          if ($inArray) $tagsSelect[] = $row->name;
          $n++;
        }
      } // end while

      if (!$multiple) $return .= "</select>\n";
      $this->db->freeResult($ret);

      $this->HTML = $return;
      $this->tagsSelect = implode(' ', $tagsSelect);
      $this->tagsRest = implode(' ', array_diff($aTags, $tagsSelect));

      return $this;
    }

    // Display tags grouped by categories
    // $tags is a string of tag
    function formatTagsHtml($tags)
    {
        $res = $this->db->query("SELECT tags.name AS tag, tags.description as description, tags.category_id as cat_id, tags_categories.name as category ".
        "FROM {$GLOBALS['db_tablename_prefix']}tags AS tags ".
        "LEFT JOIN {$GLOBALS['db_tablename_prefix']}tags_categories AS tags_categories ON category_id=tags_categories.id ".
        "WHERE tags.name IN ('". str_replace(" ", "','", $tags). "') AND tags.name>' ' ".
        "ORDER BY tags_categories.name, tags.name");

        $result = $lastline = "";
        $cat = "-1";
        $aTags = array();
        while ($rec = $this->db->fetchObject($res))
        {
          if (!isset ($aTags[$rec->category]))
            $aTags[$rec->category] = array();
          $aTags[$rec->category][] = array($rec->tag, $rec->description);
        }
        foreach ($aTags as $key=>$aaTags)
        {
          if ($key>"a")
            {
              $result .= "<p>". ucfirst($key). "&nbsp;: ";
              foreach ($aaTags as $tag)
                {
                  $result .= "<a href=\"" . calendar_absolute_url("listevents.php?tag=$tag[0]"). "\" title=\"{$tag[1]}\">{$tag[0]}</a> ";
                  $tags = str_replace($tag[0], "", $tags);
                }
              $result .= "</p>";
            }
          else
            {
              $lastline .= "<p>". _("Mots-clés"). " : ";
              foreach ($aaTags as $tag)
                {
                  $lastline .= "<a href=\"" . calendar_absolute_url("listevents.php?tag=$tag[0]"). "\" title=\"{$tag[1]}\">{$tag[0]}</a> ";
                  $tags = trim(str_replace($tag[0]." ", "", $tags." "));
                }
            }
        }
        $lastline .= trim($tags);
        return $result. $lastline;
    } // end function

    // Function to choose tags (only one per category)
    // $aTags is an array (tag_name)
    // output if HTML string select part of form
    function selectTagsHTML($aTags)
    {
        $this->selectCategoryTags($aTags, 0, $fieldName="tags[tag]", $multiple=false);
        $return = "<strong>". _("Mots-clés"). "</strong> : {$this->HTML}<br />\n";

        $this->selectWithCategoryTags($aTags, $multiple=true);
        foreach ($this->HTML as $category_id=>$aSelect)
        {
            $return .= "<strong>". ucfirst($aSelect['categoryName']). "</strong> : ".
                $aSelect['HTMLSelect'].
                "{$aSelect['categoryDescription']}<br />\n";
        }
        return $return;
    } // end


    // Get tags name for one event
    // Filter for one category, all if null
    // return a string
    function eventTagsList($event_id, $category_id=null)
    {
        $query = "SELECT name FROM {$GLOBALS['db_tablename_prefix']}tags_events ".
            "LEFT JOIN {$GLOBALS['db_tablename_prefix']}tags ".
            "ON {$GLOBALS['db_tablename_prefix']}tags.id=tag_id ".
            "WHERE event_id='{$event_id}' ".
            ($category_id!==null ? "AND category_id='{$category_id}" : "").
            " ORDER BY name";
        $result = $this->db->query ($query);
        if ($result == FALSE)
        {
            error (_("Erreur lors de la recherche des mots-clés"));
            put_footer();
            exit;
        }

        $return = "";
        while ($record = $this->db->fetchObject($result))
        {
            $return .= " ".$record->name;
        }
        $this->db->freeResult($result);
        $return = substr($return, 1);
        return $return;
    }

} // end class

