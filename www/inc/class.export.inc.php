<?php

/*
 * Copyright 2015-2018
 * - Loic Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

include_once('inc/class.event.inc.php');
include_once('inc/class.region.inc.php');

class exportEvent {
    var $region,        // numero de la region
        $daylimit,      // nombre de jours dans le futur
        $count,         // nombre d'événement
        $tags,          // tableau des tags
        $past,          // boolean true si dans le passé
        $event_id,      // 0 ou un numero ID d'evenement
        $localgroup_id; // 0 ou un numero ID de groupe local
    var $event;         // objet event
    var $error, $message;
    var $db;

    // -------------------------------------------------------------------

    function __construct($db)
    {
        $this->region = "all";
        $this->delay  = 0;
        $this->count  = 0;
        $this->tags   = array();
        $this->past   = false;
        $this->event_id = 0;

        $this->event = new event($db);

        $this->db = $db;
        $this->error = 0;
        $this->message = "";
        return true;
    }

    // -------------------------------------------------------------------

    function getEventsList()
    {
        $sqlSelect = "SELECT events.id AS id";
        $sqlFrom = " FROM {$GLOBALS['db_tablename_prefix']}events AS events";
        $sqlWhere = " WHERE (moderated=1)";

        // event_id
        if ($this->event_id != 0)
        {
            $sqlWhere .= " AND (events.id=". $this->db->quote_smart($this->event_id). ")";
            $sql = $sqlSelect. $sqlFrom. $sqlWhere;
            //$sql .= $sqlWhere;
        }
        else
        {
            // past/future and daylimit
            if ($this->past)
            {
                if ($this->daylimit !=0)
                    $start = time() - ($this->daylimit * 24 * 60 * 60);
                $end   = time();
            }
            else
            {
                $start = time();
                if ($this->daylimit !=0)
                    $end   = time() + ($this->daylimit * 24 * 60 * 60);
            }
            if (isset($start))
                $sqlWhere .= " AND (end_time >= '" . date_timestamp2mysql($start) . "') ";
            if (isset($end))
                $sqlWhere .= " AND (start_time <= '" . date_timestamp2mysql($end)   . "')";
            // tags
            if (count($this->tags)>0)
            foreach ($this->tags as $categoryId=>$tag)
            {
              $sqlWhere .= " AND '{$tag}' IN (SELECT name FROM {$GLOBALS['db_tablename_prefix']}tags AS tags".
                " LEFT JOIN {$GLOBALS['db_tablename_prefix']}tags_events AS te ON te.tag_id=tags.id WHERE te.event_id=events.id)";
            }
            // region
            if ($this->region != "all")
                $sqlWhere .= " AND ((region=" . $this->db->quote_smart($this->region) . ") OR (locality>=1))";
            // localgroup_id
            if ($this->localgroup_id != 0)
            {
                $sqlSelect .= ",role ";
                $sqlFrom .= " LEFT JOIN {$GLOBALS['db_tablename_prefix']}localgroups_events AS le ON events.id=le.event_id";
                $sqlWhere .= " AND (localgroup_id=". $this->db->quote_smart($this->localgroup_id). ")";
            }

            $sql = $sqlSelect. $sqlFrom. $sqlWhere;
            if (count($this->tags)>0 || $this->localgroup_id != 0)
                $sql .= " GROUP BY events.id";
            // past/future
            $sql .= " ORDER BY start_time ". ($this->past ? "DESC" : "ASC");
            // count
            if ($this->count > 0)
                $sql .= " LIMIT ". $this->count;
        }

        return $this->db->query($sql);
    }

    function htmlListStartCalendar()
    {
        return " <ul>\n";
    }

    function htmlListEndCalendar()
    {
        return " </ul>\n";
    }

    function get2fields()
    {
        $this->map = get_safe_integer('map', 0);
        $this->region = get_safe_integer('region', 'all');
        $this->count = get_safe_integer('count', 10);
        $this->daylimit = get_safe_integer('daylimit', 30);
        $this->event_id = get_safe_integer('event_id', 0);
        $this->localgroup_id = get_safe_integer('localgroup_id', 0);
        $this->getTagList('tag');
    }

    function getTagList($fieldName)
    {
        // Get tags ===
        $aTags = (isset($_GET[$fieldName]) ? $_GET[$fieldName] : array());
        if (!is_array($aTags))
          {
            $aTags = explode(' ', $aTags);
          }
        foreach ($aTags as $tags)
          {
            if (is_array($tags))
              {
                foreach ($tags as $tag)
                  {
                    $this->tags[] = $tag;
                  }
              }
            else
              {
                $this->tags[] = $tags;
              }
          }
        return;
    }
    // -------------------------------------------------------------------

    private function partOfUrl()
    {
        // tags
        $urltags = "";
        $aTags = $this->tags;
        foreach ($aTags as $tags)
        {
            if (is_array($tags))
            {
                foreach ($tags as $tag)
                {
                    $urltags .= ($urltags>"" ? "+" : ""). $tag;
                    $tagsE[] = $tag;
                }
            }
            else
            {
                $urltags .= ($urltags>"" ? "+" : ""). $tags;
                $tagsE[] = $tags;
            }
        }
        if ($urltags>"")
            $urltags = "&tag=". $urltags;
        // region
        $url = "region=". $this->region;
        // delay
        if (($this->daylimit!=30))
            $url .=  "&daylimit=". $this->daylimit;
        // localgroup
        if ($this->localgroup_id>0)
            $url .= "&localgroup_id=". $this->localgroup_id;
        // event
        if ($this->event_id>0)
            $url .= "&event_id=". $this->event_id;

        $url .= $urltags;
        return $url;
    }

    // -------------------------------------------------------------------

    function rssUrl()
    {
        return calendar_absolute_url("exportevents.php?format=rss&amp;". $this->partOfUrl());
    }

    // -------------------------------------------------------------------

    function georssUrl()
    {
        return calendar_absolute_url("exportevents.php?format=georss&amp;". $this->partOfUrl());
        //return $this->rssUrl(). "&map=1";
    }

    // -------------------------------------------------------------------

    function iCalUrl()
    {
        return calendar_absolute_url("exportevents.php?format=ical&amp;". $this->partOfUrl(), 'webcal:');
    }

    // -------------------------------------------------------------------

    function calGoogleUrl()
    {
        return "http://www.google.com/calendar/render?cid=".
            calendar_absolute_url("exportevents.php?format=ical&amp;". $this->partOfUrl());
    }

    // -------------------------------------------------------------------

    function jsUrl()
    {
        return calendar_absolute_url("exportevents.php?format=js&amp;". $this->partOfUrl());
    }

    // -------------------------------------------------------------------

    function webUrl()
    {
        return calendar_absolute_url("listevents.php?". $this->partOfUrl());
    }

    // -------------------------------------------------------------------

    function KmlUrl()
    {
        return calendar_absolute_url("exportevents.php?format=kml&amp;". $this->partOfUrl());
    }

    // -------------------------------------------------------------------

    function XmlUrl()
    {
        return calendar_absolute_url("exportevents.php?format=xml&amp;". $this->partOfUrl());
    }

    // -------------------------------------------------------------------

    function geoJsonUrl()
    {
        return calendar_absolute_url("exportevents.php?format=geojson&amp;". $this->partOfUrl());
    }

    // iCal ------------------------------------------------------------

    function icalStartCalendar()
    {
      global $adl_short_title, $adl_subtitle, $timezone, $root;

      header("Content-Type: application/octet-stream; charset=utf-8");
      //header("Content-Type: text/calendar; charset=UTF-8");

      $ret = "BEGIN:VCALENDAR\r\n".
        "VERSION:2.0\r\n".
        "PRODID:-//{$root}\r\n".
        "X-WR-CALNAME:{$adl_short_title}";
      if ($this->region != "all") {
        $regionName = region_find($db, $this->region);
        $ret .= " - région ". $regionName;
      }
      if (count($this->tags)>0)
        $ret .= " - tag " . implode('+', $this->tags);
      $ret .= "\r\n".
        "X-WR-TIMEZONE:". $timezone. "\r\n".
        "CALSCALE:GREGORIAN\r\n".
        "X-WR-CALDESC:" . $adl_subtitle;
      if ($this->region != "all")
        $ret .= " en région " . $regionName;
      if (count($this->tags)>0)
        $ret .= ", tag " . implode('+', $this->tags);
      $ret .= "\r\n";
      return $ret;
    }

    /*function icalEventCalendar()
    {
      return $this->event->toIcal();
    }*/

    function icalEndCalendar()
    {
      return "END:VCALENDAR\r\n";
    }

    // RSS -------------------------------------------------------------

    /* Prepare RSS feeds */
    function RssFeed()
    {
    global $adl_short_title, $adl_all_region;
        $region = new region($this->db);
        $regionList = $region->getList();
        if ($region->error)
        {
          echo returnError($region->message);
          put_footer();
          exit;
        }
        $return = $this->rssFeedHeader (calendar_absolute_url("exportevents.php?format=rss&region=all"),
            $adl_short_title. ", ". $adl_all_region);
        foreach ($regionList as $region)
        {
            $return .= $this->rssFeedHeader(calendar_absolute_url("exportevents.php?format=rss&region=".
                $region['id']), $adl_short_title. ", " . $region['name']);
        }
        return $return;
    }

    // -------------------------------------------------------------------

    function rssFeedHeader($href, $title)
    {
        return "    <link rel=\"alternate\" href=\"" . $href .
            "\" title=\"" . $title . "\" type=\"application/rss+xml\" />\n";
    }

    function rssStartCalendar()
    {
    global $adl_short_title, $adl_title, $adl_subtitle, $adl_url;

        header("Content-type: text/xml; charset=utf-8");

        $ret = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n";

        $ret .= "<rdf:RDF\n".
            "  xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n".
            "  xmlns=\"http://purl.org/rss/1.0/\"\n".
            "  xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n".
            "  xmlns:sy=\"http://purl.org/rss/1.0/modules/syndication/\"\n".
            "  xmlns:admin=\"http://webns.net/mvcb/\"\n".
            "  xmlns:cc=\"http://web.resource.org/cc/\"\n".
            "  xmlns:content=\"http://purl.org/rss/1.0/modules/content/\"\n".
            "  xmlns:georss=\"http://www.georss.org/georss\">\n";

        $ret .= "<channel rdf:about=\"". calendar_absolute_url(). "\">\n";
        $ret .= " <title>". $adl_short_title;
        if ($this->region != "all")
        {
            $region = new region($this->db, $this->region);
            $ret .= " [ " . $region->name . " ]";
        }
        $ret .= "</title>\n".
            " <description>". $adl_title." ".$adl_subtitle. "</description>\n".
            " <link>". calendar_absolute_url(). "</link>\n".
            " <dc:language>fr</dc:language>\n".
            " <dc:creator>AgendaDuLibre.org</dc:creator>\n";

        $ret .=    " <items>\n".
            "  <rdf:Seq>\n";

        return $ret;
    }

    function rssEventHeaderCalendar()
    {
        return "   <rdf:li rdf:resource=\"" . calendar_absolute_url("showevent.php?id=" . $this->event->id) . "\"/>\n";
    }

    function rssEndHeaderCalendar()
    {
        return "  </rdf:Seq>\n".
            " </items>\n".
            "</channel>\n\n\n";
    }

    function rssEndCalendar()
    {
        return "</rdf:RDF>\n";
    }

    // Javascript-------------------------------------------------------

    function jsStartCalendar()
    {
        return "document.write(\"<ul>\");\n";
    }

    function jsEndCalendar()
    {
        return "document.write(\"</ul>\");\n";
    }

    function jsCode()
    {
        return htmlentities("<script type=\"text/javascript\" language=\"JavaScript\" src=\"".
          $this->jsUrl(). "\"></script>\n");
    }

    // KML -------------------------------------------------------------

    function KmlStartCalendar()
    {
        $imageUrl = calendar_absolute_url(_themeFile('image/tux-adl.png'));

        header("Content-type: text/xml; charset=utf-8");

        return <<<KMLSTART
<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2">
  <Document>
    <name>KML {$GLOBALS['adl_short_title']} events</name>
    <open>1</open>
    <description>{$GLOBALS['adl_title']} events</description>
    <Style id="eventIcon">
      <IconStyle>
        <Icon>
          <href>{$imageUrl}</href>
        </Icon>
      </IconStyle>
    </Style>\n
KMLSTART;
    }

    function KmlEndCalendar()
    {
        return <<<KMLEND

  </Document>
</kml>
KMLEND;
    }

    // XML -------------------------------------------------------------
    function XmlStartCalendar()
    {
        $xml = new xmlWriter();
        $xml->openMemory();
        $xml->startDocument("1.0", "UTF-8");

        $xml->startElement("events");
        return $xml;
    }

    function XmlEndCalendar($xml)
    {
        $xml->EndElement();
        $xml->endDocument();

        header("Content-type: text/xml");
        return $xml->outputMemory(true);
    }

    // geoJSON ---------------------------------------------------------

    function geoJsonStartCalendar()
    {
        header("Content-Type: application/json; charset=utf-8");

        return <<<GEOJSONSTART
{ "type": "FeatureCollection",
  "features": [

GEOJSONSTART;
    }

    function geoJsonEndCalendar()
    {
        return "\n  ]\n}\n";
    }

    // -----------------------------------------------------------------

    // Function to choose daylimit (int)
    // $daylimit is (int) or 0 for no limit
    // output if HTML string select part of form
    function selectDurationHTML($daylimit)
    {
      $return = "<strong>". _("Période :") ."</strong> ";
      $return .= "<select name='daylimit'>\n";
      $aDurations = array("30"=>_("1 mois"), "91"=>_("3 mois"), "365"=>_("un an"), "0"=>_("sans limite"),);
      foreach ($aDurations as $value=>$label)
      {
        $return .= " <option value='{$value}' ". ($value==$daylimit ? "selected='selected' " : ""). ">{$label}</option>";
      }
      $return .= " </select>\n";
      return $return;
    }

}
