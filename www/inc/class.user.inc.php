<?php

/*
 * Copyright 2008-2015
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * - Loic Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

class user
{
  var $session; // class session
  var $db;      // class db
  var $message; // error message;
  var $id;
  var $login, $password, $password2;
  var $email, $lastname, $firstname;
  var $admin;   // true if admin : can edit users

  /* Construct */
  function __construct($db, $session)
  {
    $this->message = "";
    $this->session = $session;
    $this->db = $db;
  }

  private function query($sql)
  {
    $ret = $this->db->query($sql);
    if ($ret == FALSE)
    {
      error("Erreur lors de la requête <i>" . $sql . "</i>");
      return -1;
    }
    return $ret;
  }

  private function get_from_sql($sql)
  {
    $ret = $this->query($sql);
    if (! $ret) return -1;

    if ($this->db->numRows($ret) != 1)
      return -1;

    return $this->db->fetchObject($ret);
  }

  /*
   * Returns a positive ID if login exists, -1 otherwise
   */
  private function does_login_exist($login, $exceptId=null)
  {
    $sql =
      "SELECT id FROM {$GLOBALS['db_tablename_prefix']}users WHERE login=".
      $this->db->quote_smart($login).
      ($exceptId!=null ? " AND id<>". $this->db->quote_smart($exceptId) : "");
    $row = $this->get_from_sql($sql);
    return is_object($row) ? $row->id : false;
  }

  /*
   * Returns a positive ID if user identified, -1 otherwise
   */
  function check_identify($login, $password)
  {
    $sql =
      "SELECT id FROM {$GLOBALS['db_tablename_prefix']}users WHERE login=" . $this->db->quote_smart($login) .
      " AND password=" . $this->db->quote_smart(md5($password));
    $row = $this->get_from_sql($sql);
    return (is_object($row) ? $row->id : false);
  }

  function get()
  {
    $sql = "select * from {$GLOBALS['db_tablename_prefix']}users where id=" .
      $this->db->quote_smart(!isset($this->id) ? $this->session->value("agenda_libre_id") : $this->id);
    $row = $this->get_from_sql($sql);
    if (! $row || ! is_object($row)) return false;
    $this->id         = $row->id;
    $this->login      = $row->login;
    $this->password   = $row->password;
    $this->email      = $row->email;
    $this->lastname   = $row->lastname;
    $this->firstname  = $row->firstname;
    $this->admin      = ($row->admin==1);
    return true;
  }

  function save()
  {
    $sql = (isset($this->id) ? "UPDATE " : "INSERT INTO ");
    $sql .= "{$GLOBALS['db_tablename_prefix']}users SET ".
      "login=" .        $this->db->quote_smart($this->login). ", " .
      (isset($this->password) ? "password=md5(". $this->db->quote_smart($this->password). "), " : "").
      "email=" .        $this->db->quote_smart($this->email). ", ".
      (isset($this->lastname) ? "lastname=". $this->db->quote_smart($this->lastname). ", " : "").
      (isset($this->firstname) ? "firstname=". $this->db->quote_smart($this->firstname). ", " : "").
      "admin=" .        $this->db->quote_smart($this->admin ? 1 : 0);
    if (isset($this->id))
      $sql .= " WHERE id=". $this->db->quote_smart($this->id);
    if ($this->db->query($sql) == FALSE)
    {
      $sql = str_replace($this->password, "***", $sql);
      $this->message = returnError("La requête <i>". $sql. "</i> a échoué");
      return false;
    }

    if (!isset($this->id))
      $this->id = $this->db->insertid();

    return true;
  } // end function save();

  function delete()
  {
    $sql = "DELETE FROM {$GLOBALS['db_tablename_prefix']}users ".
      " WHERE id=". $this->db->quote_smart($this->id);
    if ($this->db->query($sql) == FALSE)
    {
      $this->message = returnError("La requête <i>". $sql. "</i> a échoué");
      return false;
    }

    return true;
  } // end function delete();

  function get_id_logon()
  {
    return $this->session->value("agenda_libre_id");
  }

  function get_login()
  {
    if (! isset($this->id)) $this->id = $this->session->value("agenda_libre_id");
    $this->get();
    return $this->login;
  }

  function get_name($id = null)
  {
    if ($id !== null) $this->id = $id;
    elseif (! isset($this->id)) $this->id = $this->session->value("agenda_libre_id");
    if ($this->get()) return $this->firstname . " " . $this->lastname;
    else return false;
  }

  function is_table_empty()
  {
    $sql = "SELECT id FROM {$GLOBALS['db_tablename_prefix']}users";
    $res = $this->query($sql);
    return $this->db->numRows($res)==0;
  }

  // Return true if the current atentificated user is admin
  function is_admin()
  {
    $this->id = $this->session->value("agenda_libre_id");
    if (! $this->get()) return false;
    return $this->admin;
  }

  function identify()
  {
    $this->message = "";

    // S'il n'y a aucun enregistrement dans la table user
    // on n'exige pas d'identification
    if ($this->is_table_empty())
      return false;

    if (! $this->session->exists("agenda_libre_id"))
    {
      if (! isset($_POST['__user_identify']))
      {
        ob_end_clean();
        put_header("Accès réservé");

        echo "<h2>Identification</h2>";
        echo "<table align=\"center\">";
        echo "<form method=\"post\">\n";
        echo "<tr><td>Identifiant&nbsp;:</td><td><input type=\"text\" name=\"__user_login\" size=\"20\"/></td></tr>";
        echo "<tr><td>Mot de passe&nbsp;:</td><td><input type=\"password\" name=\"__user_password\" size=\"20\"/></td></tr>";
        echo "<tr><td></td><td><input type=\"submit\" name=\"__user_identify\" value=\"Identifier\"></td></tr>";
        echo "</form>";
        echo "</table>";

        put_footer();
        exit;
      }
      else
      {
        if (($ret = $this->check_identify($_POST['__user_login'], $_POST['__user_password'])) > 0)
        {
          $this->id = $ret;
          $this->get();
          $this->session->set("agenda_libre_id", $ret);
        }
        else
        {
          ob_end_clean();
          put_header("Accès réservé");
          echo returnError("Mauvais identifiant/mot de passe");
          put_footer();
          exit;
        }
      }
    }
  }

  /* Presente un formulaire d'edition d'utilisateur */
  function edit($postUrl='moderateusers.php')
  {
    echo "<form method='post' action='{$postUrl}'>\n";
    echo "<input type='hidden' name='op' value='update'/>\n";
    if (isset($this->id))
      echo "<input type='hidden' name='__user_id' value='{$this->id}'/>\n";
    echo "<table class='stats'>\n";
    echo "<tr class='even'><td>Identifiant*:</td>".
      "<td><input type='text' size='30' name='__user_login' value='". $this->login. "'/>".
      "</td></tr>\n";
    echo "<tr><td>Mot de passe*:<br/>à saisir deux fois</td>".
      "<td>". (isset($this->id) ? "<label>Laisser le mot de passe vide s'il ne faut pas le changer.</label><br/>" : "").
        "<input type=\"password\" name=\"__user_password\" size=\"20\"/>".
        "<br /><input type=\"password\" name=\"__user_password2\" size=\"20\"/></td></tr>";
    echo "<tr class='even'><td>Courriel*:</td>".
      "<td><input type='mail' size='40' name='__user_email' value='". $this->email. "'/>".
      "</td></tr>\n";
    echo "<tr><td>Nom:</td>".
      "<td><input type='mail' size='30' name='__user_lastname' value='". $this->lastname. "'/>".
      "</td></tr>\n";
    echo "<tr class='even'><td>Prénom:</td>".
      "<td><input type='mail' size='30' name='__user_firstname' value='". $this->firstname. "'/>".
      "</td></tr>\n";
    if ($this->is_table_empty()) $this->admin = true;
    echo "<tr><td>Rôle*:</td>".
      "<td><label>Le rôle d'administrateur donne le droit de gérer les comptes de tous les utilisateurs</label>".
      "<br/><select name='__user_admin'/>".
      "<option value='0'". ($this->admin ? '' : ' selected').
        ($this->id==$this->session->value("agenda_libre_id") ? ' disabled' : ''). ">modérateur</option>".
      "<option value='1'". ($this->admin ? ' selected' : ''). ">administrateur</option>";
      "</td></tr>\n";

    echo "<tr class='even'><td></td><td><input type=\"submit\" name=\"__user_form\" value=\"Envoyer\"></td></tr>\n";
    echo "</table>\n";
    echo "</form>\n";
  }

  /* Presente un formulaire de suppression d'utilisateur */
  function delForm($postUrl='moderateusers.php')
  {
    echo "<form method='post' action='{$postUrl}'>\n";
    echo "<input type='hidden' name='op' value='delete'/>\n";
    echo "<input type='hidden' name='__user_id' value='{$this->id}'/>\n";
    echo "<table class='stats'><tr class='even'>". $this->formatTrHtml(). "</tr></table>\n";
    if ($this->admin || $this->id==$this->session->value("agenda_libre_id"))
    {
      echo "<p>Impossible de supprimer un administrateur.</p>\n";
    }
    else
    {
      echo "<input type=\"submit\" name=\"__user_delform\" value=\"Confirmer la suppression\">\n";
    }
    echo "</form>\n";
  }

  function fromForm()
  {
    $afieldName = array('id', 'login', 'password',
      'password2', 'email', 'lastname', 'firstname');
    foreach ($afieldName as $fieldName)
    {
      if (isset($_POST['__user_'. $fieldName]) && $_POST['__user_'. $fieldName]>"")
        $this->$fieldName = stripslashes($_POST['__user_'. $fieldName]);
      else
        unset($this->$fieldName);
    }

    if (isset($_POST['__user_admin']))
      $this->admin = ($_POST['__user_admin'] == 1);

    return true;
  }

  function check()
  {
    if (!isset($_POST['__user_form']))
      $this->message .= returnError("On ne vient pas du formulaire.");
    if (!isset($this->login, $this->email))
      $this->message .= returnError("Champ(s) identifiant et/ou courriel manquant(s).");
    if (!isset($this->id) && !isset($this->password, $this->password2))
      $this->message .= returnError("Mots de passe manquants.");
    if (strlen($this->login)<3)
      $this->message .= returnError("Identifiant trop court.");
    if (isset($this->password)) {
      if (strlen($this->password)<3)
        $this->message .= returnError("Mot de passe trop court.");
      if ($this->password!=$this->password2)
        $this->message .= returnError("Saisies de mot de passe différentes.");
    }
    if (! preg_match("~^([-A-Za-z0-9_+.]*)@([-A-Za-z0-9_]*)\.([-A-Za-z0-9_.]*)$~", $this->email))
      $this->message .= returnError("Adresse de courriel invalide");
    if (isset($this->id))
    {
      // check if id exists
      $user0 = new user($this->db, $this->session);
      if (! $user0->get_name($this->id))
      {
        unset($user0->password, $user0->admin, $user0->email);
        var_dump($user0);
        $this->message .= returnError("Problème de sécurité, numéro identifiant inexistant.");
      }
      unset($user0);
    }
    if ($this->does_login_exist($this->login, isset($this->id) ? $this->id : null))
      $this->message .= returnError("Identifiant déjà utilisé.");
    return (""==$this->message);
  }

  function checkDel()
  {
    if (!isset($_POST['__user_delform']))
      $this->message .= returnError("On ne vient pas du formulaire.");
    if (!isset($this->id))
      $this->message .= returnError("Numéro identifiant inexistant.");
    if (! $this->get())
      $this->message .= returnError("Problème de sécurité, numéro identifiant inexistant.");
    if ($this->admin)
      $this->message .= returnError("Impossible de supprimer un administrateur.");
    return (""==$this->message);
  }

  private function formatTrHtml()
  {
    $afieldName = array('id', 'login', 'email', 'lastname', 'firstname');
    $output = "";
    foreach ($afieldName as $fieldName)
    {
        $output .= "<td>{$this->$fieldName}</td>";
    }
    $output .= "<td>". ($this->admin ? 'admin' : ''). "</td>";
    return $output;
  }

  function listHTML()
  {
    $return = "<table class='stats'>\n";
    $return .= "<tr><th>Id</th><th>Identifiant</th><th>Courriel</th>".
      "<th>Nom</th><th>Prénom</th><th>Admin</th><th>Actions</th></tr>\n";
    $res = $this->db->query("SELECT id FROM {$GLOBALS['db_tablename_prefix']}users ORDER BY lastname, firstname");
    $impair=true;
    while ($oUser = $this->db->fetchObject($res))
    {
      $this->id = $oUser->id;
      $this->get();
      $return .= "<tr". ($impair ? " class='even'" : ""). ">". $this->formatTrHtml();
      $return .= "<td>".
        "<a href='?op=edit&id={$this->id}'>Editer</a>".
        ($this->session->value("agenda_libre_id")!=$this->id && !$this->admin ?
          " - <a href='?op=del&id={$this->id}'>Supprimer</a>" : "").
        "</td>";
      $return .= "</tr>\n";
      $impair = ! $impair;
    } // end while
    $this->db->freeResult($res);
    $return .= "</table>\n";
    return $return;
  }

}
