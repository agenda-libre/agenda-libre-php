<?php

/*
 * Copyright 2004-2015
 * - Mélanie Bats <melanie POINT bats CHEZ utbm POINT fr>
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

class session
{
  function __construct()
  {
    session_start();
  }

  function exists($variable)
  {
    if(!isset($_SESSION[$variable]) OR
       !isset($_COOKIE[$variable])    OR
       empty($_SESSION[$variable])  OR
       $_COOKIE[$variable]!=md5($_SESSION[$variable]))
      return FALSE;
    else
      return TRUE;
  }

  function value($variable)
  {
    return isset($_SESSION[$variable]) ? $_SESSION[$variable] : null;
  }

  function set($variable,$valeur)
  {
    setcookie($variable,md5($valeur),time()+(24*3600));
    $_SESSION[$variable]=$valeur;
  }

  function destroy()
  {
    setcookie('login','');
    session_unset();
    session_destroy();
  }
}

return;
?>
