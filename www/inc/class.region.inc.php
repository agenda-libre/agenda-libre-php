<?php

/*
 * Copyright 2015-2018
 * - Loic Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

class region {
    var $id;
    var $name;
    var $error, $message;
    var $db;

    function __construct($db, $id=0)
    {
        $this->db = $db;
        $this->id = $id;
        $this->error=false;
        $this->message="";
        return ($id==0 ? $this : $this->get());
    }

    function get()
    {
        $query = "SELECT name FROM {$GLOBALS['db_tablename_prefix']}regions".
            " WHERE id=". $this->db->quote_smart($this->id);
        $result = $this->db->query ($query);
        if (! ($record = $this->db->fetchObject($result)))
        {
          $this->message = "Aucune région au numéro ". $this->id;
          $this->error = true;
          return false;
        }
        $this->db->freeResult($result);
        $this->name = $record->name;
        return $this->name;
    }

    function getFromName()
    {
        $query = "SELECT id, name FROM {$GLOBALS['db_tablename_prefix']}regions".
            " WHERE name LIKE ". $this->db->quote_smart($this->name);
        $result = $this->db->query ($query);
        if (! ($record = $this->db->fetchObject($result)))
        {
            $this->message = "Aucune région nommée ". $this->name;
            $this->error = true;
            return false;
        }
        $this->db->freeResult($result);
        $this->id = $record->id;
        $this->name = $record->name;
        return $this->name;
    }

    // array of region order by name, but other country at the end
    function getList()
    {
        $ret = $this->db->query ("SELECT * FROM {$GLOBALS['db_tablename_prefix']}regions".
            " ORDER BY name");
        if ($ret == FALSE)
        {
            $this->message = "Erreur lors de la recherche des régions";
            $this->error = true;
            return false;
        }
        $regionList = array();
        while ($row = $this->db->fetchObject($ret))
        {
            $region = array('id'=>$row->id, 'name'=>$row->name);
            if ($row->id==50) {
                $otherCountry = $region;
            }
            else
            {
                $regionList[] = $region;
            }
        }
        if (isset($otherCountry))
        {
            $regionList[] = $otherCountry;
        }
        $this->db->freeResult($ret);
        return $regionList;
    }

    // Function to choose place (region)
    // $region is (int) region or (string) "all"
    // output if HTML string select part of form
    function selectHTML($onChange="")
    {
        $ret = $this->db->query ("SELECT * FROM {$GLOBALS['db_tablename_prefix']}regions".
            " WHERE id<>50 ORDER BY name");
        if ($ret == FALSE)
        {
            error ("Erreur lors de la recherche des régions");
            put_footer();
            exit;
        }
        $return = ""; //"<strong>". _("Région :") ."</strong> ";
        $return .= "<select name='region' $onChange>";
        $return .= " <option value='all' ".
            ($this->id=="all" ? "selected='selected' " : ""). ">".
            _("Toutes les régions"). "</option>";
        $return .= " <optgroup label=\"France\">\n";
        while ($row = $this->db->fetchObject($ret))
        {
            $return .= " <option value='{$row->id}' ".
                ($row->id==$this->id ? "selected='selected' " : "").
                ">{$row->name}</option>";
        }
        $this->db->freeResult($ret);
        $return .= " </optgroup>\n";
        $return .= " <optgroup label=\"Autres pays\">\n";
        $return .= "  <option value=\"50\" ".
            (50 == $this->id ? "selected=\"selected\" " : "").
            ">Autre pays</option>\n";
        $return .= " </optgroup>\n";
        $return .= " </select>\n";
        return $return;
    } // end function selectPlaceHTML()

    // Return an array of department_id or false
    function getDepartmentInRegion()
    {
        $query = "SELECT department_id FROM {$GLOBALS['db_tablename_prefix']}region_department".
            " WHERE region_id=". $this->db->quote_smart($this->id);
        if (! $result = $this->db->query ($query))
        {
            $this->message = "Erreur dans la requête ". $query;
            $this->error = true;
            return false;
        }
        $departmentsId = array();
        while ($record = $this->db->fetchObject($result))
        {
            $departmentsId[] = $record->department_id;
        }
        $this->db->freeResult($result);
        return $departmentsId;
    }

    function getRegionFromDepartment($departmentId)
    {
        $query = "SELECT region_id FROM {$GLOBALS['db_tablename_prefix']}region_department".
            " WHERE department_id=". $this->db->quote_smart($departmentId);
        if (! $result = $this->db->query ($query))
        {
            $this->message = "Erreur dans la requête ". $query;
            $this->error = true;
            return false;
        }
        if (!$record = $this->db->fetchObject($result))
        {
            $this->message = "Région non trouvée pour le département ". $departmentId;
            $this->error = true;
            $this->id = 50;
        } else {
            $this->db->freeResult($result);
            $this->id = $record->region_id;
        }
        $this->get();
        return $this->id;
    }

} // end class
