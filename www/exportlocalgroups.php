<?php

/*
 * Copyright 2008-2017
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * - Loic Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

$privatePage = false;

include("inc/main.inc.php");
include("inc/class.localgroup.inc.php");
include("inc/class.export.inc.php");

$localgroup_id = get_safe_integer('id', 0);
$region_id = get_safe_integer('region', 0);
$depart_id = get_safe_integer('depart', 0);
$format = get_safe_string('format', 'geojson');
$partOfLocalgroupName = get_safe_string('queryString', "");

$localgroups = new localgroups($db);

if ($localgroup_id>0) {
    $localgroups->getOne($localgroup_id);
} elseif ($depart_id>0) {
    $localgroups->getFromDepartment($depart_id);
} elseif ($region_id>0) {
    $localgroups->getFromRegion($region_id);
} elseif ($partOfLocalgroupName>"") {
    $localgroups->getFromName($partOfLocalgroupName);
}

if (count($localgroups->localgroups)==0)
{
    put_header(ucfirst($adl_localgroups));
    echo returnWarning("Pas de $adl_localgroups_acronym avec ce filtre");
    put_footer();
    exit;
}

switch ($format) {
    case 'csv' : {
        Header("Content-type: text/plain; charset: utf-8");
        echo $localgroups->formatCsvOsm();
        break;
    }

    case 'json' : {
        // Ajax query from Event form
        if ($partOfLocalgroupName>"") {
            http_response_code(200);
            header('Content-type: application/json');
            echo $localgroups->formatJson();
            break;
        } else {
            http_response_code(500);
        }
        break;
    }

    case 'geojson' : {
        $export = new exportEvent($db);
        echo $export->geoJsonStartCalendar();
        echo $localgroups->formatGeojson();
        echo $export->geoJsonEndCalendar();
        break;
    }
} // end switch
