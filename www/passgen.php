<?php

/*
 * Copyright 2005-2015
 * - Mélanie Bats <melanie POINT bats CHEZ utbm POINT fr>
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

$privatePage = false;

include("inc/main.inc.php");

put_header("Génération du mot de passe");

if ($_POST['__crypt_pass'])
{
  $result = $db->query ("select(md5('" . $_POST['__password'] . "'))");

  $row = $db->fetchRow($result);

  echo "Votre mot de passe hashé est : " . $row[0] . "<br/>";

  put_footer();
  exit;
}

echo "<p>Cette page sert aux futurs modérateurs pour générer un mot de passe hashé.</p>";

echo "<form method=\"post\">\n";

echo "Mot de passe: <input type=\"password\" name=\"__password\" size=\"30\"/>\n";
echo "<input type=\"submit\" name=\"__crypt_pass\" value=\"Hasher\"/>\n";

echo "</form>";

put_footer();

?>
