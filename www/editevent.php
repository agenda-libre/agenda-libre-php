<?php

/*
 * Copyright 2008-2017
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * - Loic Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

include("inc/main.inc.php");
include_once("inc/class.user.inc.php");
include("inc/diff.inc.php");
include("inc/class.event.inc.php");

function alertModerators ($event, $modifier, $diff)
{
  global $moderatorlist, $user;

  if ($event->moderated)
  {
    $mail_title = "Édition de l'évènement validé '" . $event->title . "'";
    $mail_body = "Bonjour,\n\n" .
        wordwrap("L'évènement validé '" . stripslashes($event->title).
          "' a été modifié par " . $modifier . ".\n") .
        "Modifications apportées:\n\n" .
        $diff . "\n\n".
        "Bonne journée\n\n".
        "-- L'équipe de modération";
  }
  else
  {
    $mail_title = "Édition de l'évènement '" . $event->title . "'";
    $mail_body = "Bonjour,\n\n" .
         "L'évènement '" . stripslashes($event->title).
            "' a été modifié par " . $modifier . ".\n".
         "Cet évènement est en attente de modération à l'adresse\n" .
         calendar_absolute_url("moderation.php#" . $event->id) . "\n\n".
         "Modifications apportées:\n\n" .
         $diff . "\n\n".
         "Bonne journée\n\n".
         "-- L'équipe de modération";
  } // end if else

  calendar_mail($moderatorlist, $mail_title, $mail_body, $event->id);

  return ($user ? "moderation.php" : "showevent.php?id=" . $event->id);
}

function updateEvent($user, $newEvent, $oldEvent)
{
  global $moderatorlist;

  if (! $newEvent->submitter)
    $newEvent->submitter = $newEvent->contact;

  $newEvent->save();

  $diff = @arr_diff (explode("\n", $oldEvent->formatAscii()),
    explode("\n", $newEvent->formatAscii()), 1);

  $modifier = $user ? $user->get_name() : "le soumetteur de l'évènement";

  return alertModerators($newEvent, $modifier, $diff);
} // end function


$id = get_safe_integer('id', 0);
$secret = get_safe_hash('secret', false);

$thisTitle = "Édition d'un évènement";

// Get fields and store in $event object
if (isset($_POST['__event_save']) || isset($_POST['__event_preview']))
{
  $updatedEvent = new event($db, $id);
  $updatedEvent->fromForm();
  // Check information, find city, postalcode and coordonnates
  $ret = $updatedEvent->check();
  $message = $updatedEvent->message;
}

$event = new event($db, $id);
if ($event->error)
{
  put_header($thisTitle);
  error($event->message);
  put_footer();
  exit;
}

// check secret, if not is user logon?
if ($secret == false)
{
  $user = new user($db, $session);
  $user->identify();
}
else
{
  if ($event->secret != $secret)
    {
      put_header($thisTitle);
      error("Hash d'identification non reconnu");
      put_footer();
      exit;
    }
  $user = false;
}

if (isset($_POST['__event_save']) && $ret)
{
  $returnaddr = updateEvent($user, $updatedEvent, $event);

  if ($returnaddr === false)
    {
      put_header($thisTitle);
      error ("Problème lors de la sauvegarde de l'évènement <i>".
        $_POST['__event_title'] . "</i>");
    }

  if ($returnaddr != "")
    header("Location: " . $returnaddr);

  put_header("édition d'un évènement");
  if (isset($message) && $message!="")
    echo $message;
  echo "<p>Les modifications sur l'évènement <i>" .
    $_POST['__event_title'] . "</i> ont bien été enregistrées.</p>";
  put_footer();

  exit;
} // end if

put_header($thisTitle);

if (isset($message) && $message!="") echo $message;

if (isset($updatedEvent))
  {
    echo '<p class="moderationheader">';
    echo ($user ? "<a href=\"moderation.php\">Modération</a>&nbsp;" : "").
      "&gt;&gt;&gt;&nbsp;Prévisualisation de l'évènement";
    echo "</p>";
  }

if (isset($event) && $event->moderated)
{
  echo '<div class="moderationbox">';
  echo '<p style="font-weight: bold;">Attention, cet évènement est déjà modéré. Toute modification sera immédiatement visible sur le site.</p>';
  echo '</div>';
}

if (isset($updatedEvent))
{
  echo '<div class="moderationbox">';
  echo $updatedEvent->formatHTML();
  echo '</div>';
}

echo '<p class="moderationheader">';
echo ($user ? "<a href=\"moderation.php\">Modération</a>&nbsp;" : "").
  "&gt;&gt;&gt;&nbsp;&Eacute;dition de l'évènement";
echo "</p>";

//echo '<div class="moderationbox">';

$posturl = "?id=" . $id. ($user ? "" : "&secret=" . $secret);

if (isset($updatedEvent))
  $updatedEvent->edit($_preview=true, $_valid=true, $posturl);
else
  $event->edit($_preview=true, $_valid=true, $posturl);
//echo '</div>';

put_footer();

?>
