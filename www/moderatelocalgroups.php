<?php

/*
 * Copyright 2016
 * - Loic Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

$privatePage = false;

include("inc/main.inc.php");

if (! $localgroupSubmit) {
    $user->identify();
}

put_header(ucfirst($adl_localgroups));

echo "<h2>". ucfirst($adl_localgroups). "</h2>";

include_once('inc/class.region.inc.php');
include_once('inc/class.localgroup.inc.php');

if (! $localgroupManagement) {
    error("La gestion des {$adl_localgroups} est d&eacute;sactiv&eacute;e.");
    put_footer();
    exit;
}


$op = (isset($_REQUEST['op']) ? $_REQUEST['op'] : null);
$localgroup_id = get_safe_integer('id', 0);
$region_id = get_safe_integer('region', 0);

/*
 *
 * Main page
 *
 */

// context links
if ($user->is_admin()) {
    echo "<div id=\"localgroup-list\">";
    echo "<h1>Actions</h1>\n";
    echo "<br/><a href=\"moderatelocalgroups.php\"><i class='fas fa-gavel'></i> ". $adl_localgroups_acronym. " ".
        _("en attente de mod&eacute;ration"). "</a>".
        "<br/><a href=\"moderatelocalgroups.php?op=list\"><i class='fas fa-list-ul'></i> ".
        _("Liste des"). " {$adl_localgroups_acronym}</a>".
        "<br/><a href=\"moderatelocalgroups.php?op=new\"><i class='fas fa-plus-square'></i> ".
        _("Nouveau"). " {$adl_localgroups_acronym}</a></p>\n";
    echo "</div>\n";
}

switch ($op) // compute
{

    case 'update' :
    {
        $localgroup = new localgroup($db, $localgroup_id);
        $localgroup->fromForm();

        if (! $localgroup->check())
        {
            // affichage des erreurs
            echo $localgroup->message;
            $op = 'edit2';
        } else {
            // affichage des avertissements s'il y en a
            echo $localgroup->message;
            if ($localgroup->is_preview) { // prévisualisation
                echo "<h3>Prévisualisation</h3>";
                echo $localgroup->formatHtml();
                $op='edit2';
            } else { // enregistrement
                if ($localgroup->save())
                {
                    header("Location: moderatelocalgroups.php?op=saved&id={$localgroup->id}");
                    exit;
                } else {
                    echo $localgroup->message;
                    $op='edit2';
                }
            }
        }
        break;
  }

    case 'saved' :
    {
        echo "<p>{$adl_localgroup} enregistré.</p>";
        $op='info';
        break;
    }

  case 'delete' :
  {
        $user->identify();
        $localgroup = new localgroup($db, $localgroup_id);
        $localgroup->fromConfirmDelForm();
        if ($localgroup->error)
        {
            echo $localgroup->message;
            $id = $localgroup->id;
            $op='info';
        } else {
            if ($localgroup->delete())
            {
                header("Location: moderatelocalgroups.php?op=deleted");;
                exit;
            } else {
                echo $localgroup->message;
                $id= $localgroup->id;
                $op='info';
            }
        }
    break;
  }

  case 'deleted' :
  {
        echo "<p>{$adl_localgroup} supprimé.</p>";
        $op="moderate";
        break;
    }

    case 'reject' :
    {
        $user->identify();
        $localgroup = new localgroup($db, $localgroup_id);
        $localgroup->fromConfirmRejectForm();
        if ($localgroup->error)
        {
            echo $localgroup->message;
        } else {
            if ($localgroup->reject())
            {
                header("Location: moderatelocalgroups.php?op=rejected");;
                exit;
            } else {
                echo $localgroup->message;
            }
        }
        $op='moderate';
        break;
    }

    case 'rejected' :
    {
        echo "<p>{$adl_localgroup} rejet&eacute;.</p>";
        $op="moderate";
        break;
    }

    case 'validate' :
    {
        $user->identify();
        $localgroup = new localgroup($db, $localgroup_id);
        $localgroup->fromConfirmValidateForm();
        if ($localgroup->error)
        {
            echo $localgroup->message;
        } else {
            if ($localgroup->validate())
            {
                header("Location: moderatelocalgroups.php?op=validated");;
                exit;
            } else {
                echo $localgroup->message;
            }
        }
        $op='moderate';
        break;
    }

    case 'validated' :
    {
        echo "<p>{$adl_localgroup} valid&eacute;.</p>";
        $op="moderate";
        break;
    }

} // end switch compute


switch ($op) // display
{

    case 'del' :
    {
        $user->identify();
        echo "<h3>Suppression d'un {$adl_localgroup}</h3>";
        $localgroup = new localgroup($db, $localgroup_id);
        echo $localgroup->confirmDelFormHtml($localgroup_id);
        break;
    }

    case 'new' :
    {
        echo "<h3>Édition d'un nouveau {$adl_localgroup}</h3>";
        $localgroup = new localgroup($db, 0);
        echo $localgroup->formHtml(false, true, false);
        break;
    }

    case 'dupl' :
    {
        $localgroup = new localgroup($db, $localgroup_id);
        echo "<h3>Édition d'après un {$adl_localgroup} existant</h3>";
        echo $localgroup->formHtml(true, true, false);
        break;
    }

    case 'edit' :
    {
        // TODO : accept edit with secret code
        $user->identify();
        $localgroup = new localgroup($db, $localgroup_id);
        echo "<h3>Édition</h3>";
        echo $localgroup->formHtml(false, true, false);
        break;
    }
    case 'edit2' :
    {
        echo "<h3>Édition</h3>";
        echo $localgroup->formHtml(false, true, true);
        break;
    }

    case 'info' :
    {
        $localgroups = new localgroups($db);
        echo $localgroups->formatHtml($localgroup_id);
        break;
    }

    case 'list' :
    {
        $user->identify();
        $localgroups = new localgroups($db);
        $localgroups->region_id = $region_id;
        $localgroups->get();
        $region = new region($db, $region_id);
        echo "<form method='get'>{$adl_localgroups} de ";
        echo $region->selectHtml("onchange=\"window.location='?op=list&region=' + this.options[this.selectedIndex].value;\"", false);
        echo "<input type='hidden' name='op' value='list' />";
        echo "<input value='Ok' type='submit'></form>\n";

        echo $localgroups->listHtml();
        break;
    }

    case 'validateform' :
    {
        $user->identify();
        $localgroups = new localgroups($db);
        echo $localgroups->confirmValidateFormHtml($localgroup_id);
        break;
    }

    case 'rejectform' :
    {
        $user->identify();
        $localgroups = new localgroups($db);
        echo $localgroups->confirmRejectFormHtml($localgroup_id);
        break;
    }

    case 'moderate' :
    default : // localgroups waiting for moderation
    {
        $user->identify();
        $localgroups = new localgroups($db);
        $localgroups->region_id = $region_id;
        $localgroups->moderated = false;
        $localgroups->get();

        $region = new region($db, $region_id);
        echo "<form method='get'>{$adl_localgroups} de ";
        echo $region->selectHtml("onchange=\"window.location='?op=moderate&region=' + this.options[this.selectedIndex].value;\"", false);
        echo "<input type='hidden' name='op' value='moderate' />";
        echo "<input value='Ok' type='submit'></form>\n";
        echo "<h3>". ucfirst($adl_localgroups). " &agrave; mod&eacute;rer</h3>\n";

        echo $localgroups->listHtml();
        if (count($localgroups->localgroups)>999) {
            echo returnWarning("Il y a plus de $adl_localgroups mais tous ne peuvent pas &ecirc;tre affich&eacute;s.");
        }

        break;
    }

} // end switch display


put_footer();

?>
