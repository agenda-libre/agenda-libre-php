<?php

/*
 * Copyright 2016-2018
 * - Loic Dayot <ldayot A epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

include("inc/main.inc.php");
include_once("inc/class.localgroup.inc.php");
include_once("inc/class.export.inc.php");

$id = get_safe_integer('id', 0);
$localgroup = new localgroup($db, $id);

if ($localgroup->error)
{
    put_header($adl_localgroup_info);
    error($localgroup->message);
    put_footer();
    exit;
}

function displayEventList($eventList, $list)
{
    echo $eventList->htmlListStartCalendar();
    while ($event = $eventList->db->fetchObject($list))
    {
        $eventList->event->id = $event->id;
        $eventList->event->role = $event->role;
        $eventList->event->get();
        echo $eventList->event->toHtmlList();
    }
    echo $eventList->htmlListEndCalendar();
}


if ($localgroup->latitude>0)
{
    $lgUrl = calendar_absolute_url('exportlocalgroups.php');
    $jcode = <<<JSCODE1

  <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="js/leaflet.js"></script>
  <script type="text/javascript" src="js/leaflet.markercluster-src.js"></script>
  <script type="text/javascript" src="js/displayMap.js"></script>


  <script type="text/javascript">
      var lat= {$localgroup->latitude};
      var lon= {$localgroup->longitude};
      var zoom= {$GLOBALS['mapLocalgroupDisplay']['zoom']};

      function init() {

        displayMap(lat, lon, zoom);

        /* localgroups ------------------------------------- */
        addLayer('{$lgUrl}?id={$localgroup->id}',
          "<span style='color:blue;'><i class='fas fa-users'></i>{$adl_localgroups_acronym}</a></span>",
          localgroupIcon, false);

      }

    </script>\n";
JSCODE1;

    put_header($adl_localgroups_info, $jcode, "init();");
}
else
{
    put_header($adl_localgroups_info);
}


// Display actions in context div
echo $localgroup->actionsHtml();
// Display localgroup details
echo $localgroup->formatHtml();

// Display map
if ($localgroup->latitude>0)
{
    echo "<ul class='mist' id='map' style=\"margin: auto; width:70%; height:400px; border: 1px solid black;\"></ul>\n";
}

// Display events in relation with localgroup
$eventList = new exportEvent($db);
$eventList->localgroup_id = $id;
$eventList->daylimit = 90;
if (($list = $eventList->getEventsList()) == FALSE)
{
  error ("Erreur lors de la récupération des évènements");
  exit;
}
if ($db->numRows($list)>0) {
    echo "<h3>Le $adl_localgroup participe aux prochains &eacute;v&eacute;nements suivants</h3>";
    displayEventList($eventList, $list);
}

$eventList->past = true;
$eventList->daylimit = 30;
if (($list = $eventList->getEventsList()) == FALSE)
{
  error ("Erreur lors de la récupération des évènements");
  exit;
}
if ($db->numRows($list)>0) {
    echo "<h3>Le $adl_localgroup participait aux &eacute;v&eacute;nements passés suivants</h3>";
    displayEventList($eventList, $list);
}

put_footer();

?>
