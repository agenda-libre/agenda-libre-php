<?php

/*
 * Copyright 2006-2015
 * - Mélanie Bats <melanie POINT bats CHEZ utbm POINT fr>
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

include("inc/main.inc.php");
include("inc/class.event.inc.php");

global $localGroupsName;

put_header("Informations sur un évènement");

echo "<div id=\"localgroup-list\"><h1>" . $adl_localgroups_around . "</h1>";

$event = new event($db, 0);

$localgroups = $db->query ("select distinct region, name, url ".
  "from {$GLOBALS['db_tablename_prefix']}localgroups where region = '" .
  $_POST['__event_region'] . "'");

if ($db->numRows ($localgroups) == 0)
{
  echo "Pas de {$adl_localgroups_around}.<br/>";
}
else
{
  echo " <ul>";

  while ($localgroup = mysql_fetch_object ($localgroups))
    {
      echo "<li><a href=\"" . $localgroup->url . "\">" . $localgroup->name . "</a> (";
      $dpts = $db->query ("select distinct department ".
        "from {$GLOBALS['db_tablename_prefix']}localgroups where name = '" . $localgroup->name . "'");
      $i = 1;
      while ($dpt = $db->fetchObject ($dpts))
	{
	  echo $dpt->department;
	  if ($i != $db->numRows($dpts))
	    echo ", ";
	  $i++;
	}
      echo ")</li>";
    }
  echo " </ul>";
}
echo "</div>";

/* Convert form date to timestamp */
if ($_POST['__event_start_day'])
{
  $start = mktime($_POST['__event_start_hour'],
		  $_POST['__event_start_minute'],
		  0,
		  $_POST['__event_start_month'],
		  $_POST['__event_start_day'],
		  $_POST['__event_start_year']);
}

if ($_POST['__event_end_day'])
{
  $end = mktime($_POST['__event_end_hour'],
		$_POST['__event_end_minute'],
		0,
		$_POST['__event_end_month'],
		$_POST['__event_end_day'],
		$_POST['__event_end_year']);
}

if (isset($_POST['__event_title'])) // if from form
{
  $event->fromForm();
  $event->check();
}

echo $event->formatHTML();

put_footer();

?>
