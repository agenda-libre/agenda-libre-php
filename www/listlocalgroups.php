<?php
/*
 * Copyright 2017-2018
 * - Loic Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

include("inc/main.inc.php");

if (! $localgroupList) {
    $user->identify();
}

put_header(ucfirst($adl_localgroups));

echo "<h2>". ucfirst($adl_localgroups). "</h2>";

include_once('inc/class.region.inc.php');
include_once('inc/class.localgroup.inc.php');

$region_id = get_safe_integer('region', 0);
$partOfName = get_safe_string('partOfName', '');

/*
 *
 * Main page
 *
 */

/* Fetch region name */
$region = new region($db, $region_id);
if ($region_id != 0)
  $regionName = $region->name;
else
  $regionName = $adl_all_region;

$localgroups = new localgroups($db);
$localgroups->region_id = $region_id;
$localgroups->partOfName = $partOfName;
$localgroups->get();
echo "\n\n<form method='get' name='filter1'>";
echo "<fieldset><legend>Filtres</legend>";
echo "Partie du nom <input type='text' name='partOfName' value=\"{$partOfName}\" />{$partOfName}";
echo " - R&eacute;gion ". $region->selectHtml("onchange=\"window.location='?op=list&partOfName='".
    "+ document.filter1.partOfName.value + ".
    "'&region=' + this.options[this.selectedIndex].value;\"", false);
echo " - <input value='Ok' type='submit'></form>\n";
echo "</fieldset>\n\n";
echo $localgroups->listHtml();

put_footer();

?>
