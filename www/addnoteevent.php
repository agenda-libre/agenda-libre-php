<?php

/*
 * Copyright 2009-2015
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

$privatePage = true;

include("inc/main.inc.php");
include("inc/class.event.inc.php");

if (isset($_POST['__event_note_cancel']))
{
    header("Location: moderation.php");
    exit;
}
else if (isset($_POST['__event_note_confirm']))
{
    $id = get_safe_integer('id', 0);

    $sql = "insert into {$GLOBALS['db_tablename_prefix']}notes (contents, date, event_id, author_id) values (" .
        $db->quote_smart($_POST['__event_note_text'])   . "," .
        $db->quote_smart($now)                          . "," .
        $id                                             . "," .
        $user->get_id_logon()                           . ")";
    $ret = $db->query($sql);
    if ($ret == FALSE)
    {
        error("La requête a échoué");
        return -1;
    }
    header("Location: moderation.php");
}


$id = get_safe_integer('id', 0);
put_header("Ajout d'une note de modération");

$event = new event($db, $id);
if (! $event)
{
    echo "<p>Pas d'évènement avec cet ID</p>";
    put_footer();
    exit;
}
if ($event->moderated)
{
    echo "<p>Évènement déjà modéré</p>";
    put_footer();
    exit;
}
echo '<p class="moderationheader">';
echo "<a href=\"moderation.php\">Modération</a>&nbsp;&gt;&gt;&gt&nbsp;Ajout d'une note de modération";
echo "</p>";

echo '<div class="moderationbox">';

if ($event->locality >= 0)
{
  echo '<p style="color: red; font-weight: bold; text-align: center;">';
  echo "Attention, cet évènement est à portée ".
    $adl_locality[$event->locality]. " !";
  echo '</p>';
}

echo '<form action="addnoteevent.php?id=' . $id . '" method="post">';
echo '<p style="text-align: center;">Votre commentaire&nbsp;:</p>';
echo '<textarea name="__event_note_text" cols="70" rows="10"></textarea><br/>';
echo '<input name="__event_note_confirm" type="submit" value="Enregistrer"/>&nbsp;';
echo '<input name="__event_note_cancel" type="submit" value="Annuler"/>';
echo '</form>';
echo '</div>';

echo '<div class="moderationbox">';
echo $event->formatHTML($event);
echo '</div>';

put_footer();
?>
