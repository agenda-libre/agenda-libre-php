<?php

/*
 * Copyright 2008-2009
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * - Loic Dayot <ldayot CHEZ ouvaton POINT org>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

include("inc/main.inc.php");
include_once("inc/class.user.inc.php");
include("inc/diff.inc.php");
include("inc/class.event.inc.php");

function cancel_event ($db, $user, $id)
{
  global $moderatorlist;

  $oldevent = new event($db, $id);
  if ($oldevent->error)
    exit;

  $oldevent->cancel();
  if ($oldevent->error)
    {
      error ($oldevent->message);
      return false;
    }

  if ($user)
    $modifier = $user->get_login();
  else
    $modifier = "le soumetteur de l'évènement";

  $subject = "Annulation de l'évènement '" . stripslashes($oldevent->title) . "'";

  $message =
    "Bonjour,\n\n" .
    wordwrap("L'évènement '" . stripslashes($oldevent->title) .
    "' a été annulé par " . $modifier . ".\n") .
    "Détail de l'évènement annulé:\n\n" .
    "=======================================================\n" .
    $oldevent->formatAscii() . "\n".
    "=======================================================\n\n" .
    "Bonne journée,\n\n".
    "-- L'équipe de modération";
  calendar_mail ($moderatorlist, $subject, $message, $oldevent->id);
}

$id = get_safe_integer('id', 0);
$secret = get_safe_hash('secret', false);

$event = new event($db, $id);
if ($event->error)
{
  put_header("Annulation d'un évènement");
  error($event->message);
  put_footer();
  exit;
}

if ($secret == FALSE)
{
  $user = new user($db, $session);
  $user->identify();
}
else
{
  if ($event->secret != $secret)
    {
      put_header("Annulation d'un évènement");
      error("Hash d'identification non reconnu");
      put_footer();
      exit;
    }

  $user = false;
}

if (isset($_POST['__event_cancelation_confirm']))
{
  put_header("Annulation d'un évènement");
  $ret = cancel_event ($db, $user, $id);
  if ($ret)
    {
      echo "<p>L'évènement a bien été annulé. Il n'apparaît plus dans l'Agenda.</p>";
    }
  put_footer();
  exit;
}
else if (isset($_POST['__event_cancelation_cancel']))
{
  put_header("Annulation d'un évènement");
  echo "<p>Vous n'avez pas souhaité annuler cet évènement.</p>";
  put_footer();
  exit;
}

put_header("Annulation d'un évènement");

$event = new event($db, $id);
if ($event->error)
{
  error($event->message);
  put_footer();
  exit;
}

if ($user)
{
  echo '<p class="moderationheader">';
  echo "<a href=\"moderation.php\">Modération</a>&nbsp;&gt;&gt;&gt;&nbsp;Annulation de l'évènement";
  echo "</p>";
}
else
{
  echo '<p class="moderationheader">';
  echo "&gt;&gt;&gt;&nbsp;Annulation de l'évènement";
  echo "</p>";
}

if ($user)
  $posturl = "?id=" . $id;
else
  $posturl = "?id=" . $id . "&secret=" . $secret;

echo '<div class="moderationbox">';
if ($event->moderated)
  echo '<p style="font-weight: bold;">Attention, cet évènement est déjà modéré. ' .
       'Cette annulation le fera disparaître de '. $adl_title. '.</p>';
echo '<form action="' . $posturl . '" method="post">';
echo '<p style="text-align: center;">Confirmez-vous l\'annulation de cet évènement&nbsp;?</p>';
echo '<input name="__event_cancelation_confirm" type="submit" value="Oui"/>&nbsp;';
echo '<input name="__event_cancelation_cancel" type="submit" value="Non"/>';
echo '</form>';
echo '</div>';

if ($user)
{
  echo '<p class="moderationheader">';
  echo "<a href=\"moderation.php\">Modération</a>&nbsp;&gt;&gt;&gt;&nbsp;Visualisation de l'évènement";
  echo "</p>";
}
else
{
  echo '<p class="moderationheader">';
  echo "&gt;&gt;&gt;&nbsp;Visualisation de l'évènement";
  echo "</p>";
}

echo '<div class="moderationbox">';
echo $event->formatHTML();
echo '</div>';

put_footer();

?>
