<?php

/*
 * Copyright 2004-2015
 * - Mélanie Bats <melanie POINT bats CHEZ utbm POINT fr>
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

include("inc/main.inc.php");

put_header ("Créer votre agenda");
?>

<h2>Créer votre agenda</h2>

<p>Cet agenda n'existe pas pour l'instant. Néanmoins, si vous le
souhaitez, nous pouvons vous aider à mettre en place un agenda des
évènements du Logiciel Libre dans votre pays en vous fournissant,
selon vos besoins&nbsp;:</p>

<ul>

  <li>Un nom de domaine de type <code>XXX.agendadulibre.org</code>,
  comme par exemple <code>be.agendadulibre.org</code> pour la
  Belgique</li>

  <li>De l'aide à l'installation du logiciel Agenda du Libre</li>

  <li>L'hébergement de votre Agenda du Libre sur notre serveur si vous
  ne disposez pas d'hébergement</li>

</ul>

<p>N'hésitez pas à nous contacter pour en discuter.</p>

<?php //'
 put_footer();
?>

