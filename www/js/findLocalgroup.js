/*
 * Copyright 2017
 * - Loic Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Inspired from Benjamin Marquant 2016
 * https://www.bxnxg.com/minituto-01-jquery-autocomplete-ajax-cache-astuce-listing-suggestion-javascript-autocompletion/
 */


/* initialisation paramètres globaux : */
var cache = {}; /* tableau cache de tous les termes */
var term = null; /* terme renseigné dans le champ input */

$(document).ready(function() {
    /* localgroup autocomplete */
    $('#lg_name').autocomplete({
        minLength:2, /* nombre de caractères minimaux pour lancer une recherche */
        delay:200, /* delais après la dernière touche appuyée avant de lancer une recherche */
        scrollHeight:320,
        appendTo:'#lg-container', /* div ou afficher la liste des résultats, si null, ce sera une div en position fixe avant la fin de </body> */

        /* dès qu'une recherche se lance, source est executé, il peut contenir soit un tableau JSON de termes, soit une fonctions qui retournera un résultat */
        source:function(e,t) {
            term = e.term; /* récupération du terme renseigné dans l'input */
            term = term.toLowerCase();
            if(term in cache) { /* on vérifie que la clé "term" existe dans le tableau "cache", si oui alors on affiche le résultat */
                t(cache[term]);
            } else { /* sinon on fait une requête ajax vers city.php pour rechercher "term" */
                $('#loading').attr('style','');
                $.ajax({
                    type:'GET',
                    url:'exportlocalgroups.php',
                    data:'format=json&queryString='+term,
                    dataType:"json",
                    async:true,
                    cache:true,
                    success:function(e){
                        cache[term] = e; /* vide ou non, on stocke la liste des résultats avec en clé "term" */
                        if(!e.length){ /* si aucun résultats, on renvoi un tableau vide pour informer qu'on a rien trouvé */
                            var result = [{
                                label: 'Aucun groupe utilisateur trouvé...',
                                value: null,
                                id: null,
                            }];
                            t(result); /* envoit du résultat à source */
                        }else{ /* sinon on renvoie toute la liste */
                            t($.map(e, function (item){
                                return{
                                    label: item.label,
                                    value: item.value,
                                    id: item.id,
                                }
                            }));  /* envoi du résultat à source avec map() de jQuery (permet d'appliquer une fonction pour tous les éléments d'un tableau */
                        }
                        $('#loading').attr('style','display:none;');
                    }
                });
            }
        },

        /* sélection depuis la liste des résultats (flèches ou clic) > ajout du résultat automatique et callback */
        select: function(event, ui) {
            $('form input[name="localgroup_id"]').val(ui.item ? ui.item.id : ''); /* on récupère juste l'id qu'on stocke dans l'autre input */
            $('#loading').attr('style','display:none;');
        },
        open: function() {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function() {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        },
    });
});

    function lg_add(nlg) {
        if ($('form input[name="localgroup_id"]').val()<=0) {
            alert("Attention, vous devez choisir un groupe utilisateur");
            return false;
        }
        if ($('form select[name="localgroup_role"]').val()=="") {
            alert("Attention, vous devez choisir rôle au groupe utilisateur");
            return false;
        }
        /* c'est bon, on ajoute une ligne au dessus */
        $('#lg_list').append(
            "<li id='lg_n"+ nlg+ "'>"+
            $('form input[name="lg_name"]').val()+
            " en tant que "+
            role_name($('form select[name="localgroup_role"]').val())+
            "<input type='hidden' name='__event_lg_id["+ nlg+ "]' value='"+
            $('form input[name="localgroup_id"]').val()+
            "' />"+
            "<input type='hidden' name='__event_lg_role["+ nlg+ "]' value='"+
            $('form select[name="localgroup_role"]').val()+
            "' />"+
            " <a href='#' onClick='javascript:lg_delete("+ nlg+ "); return false;'>retirer</a>"+
            "</li>"
        );
        /* vider les champs */
        $('form input[name="lg_name"]').val('');
        $('form select[name="localgroup_role"]').val('');
        /* changer la valeur du lien lg_add */
        $('form a[name="lg_add"]').replaceWith("<a href='#' onclick='javascript:lg_add("+ (nlg+1)+ "); return false;'>ajouter</a>");
        return false;
    }

    function lg_delete(nlg) {
        $('#lg_n'+nlg).remove();
        return false;
    }
