/*
 * Copyright 2017-2018
 * - Loic Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Inspired from Benjamin 2016
 * https://www.bxnxg.com/minituto-01-jquery-autocomplete-ajax-cache-astuce-listing-suggestion-javascript-autocompletion/
 */

/*
 * Display map in html page
 * Used in map.php, showevent.php and showlocalgroup.php
 */

var controls, map;
var eventIcon, localgroupIcon;

function displayMap(lat, lon, zoom) {

    map = new L.map('map', {
      center: [lat, lon],
      zoom: zoom,
    });

    controls = L.control.layers(null, null, {
      collapsed: true
    }).addTo(map);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://osm.org/copyright">OpenStreetMap</a>'
    }).addTo(map);

    /* events ------------------------------------- */
    eventIcon = L.icon({
      iconUrl: 'adl/image/marker.png',
      iconSize: [36, 46],
      iconAnchor: [18, 46]
    });

    /* localgroups ------------------------------------- */
    localgroupIcon = L.icon({
      iconUrl : 'adl/image/localgroup.png',
      iconSize : [32, 32]
    });
}

function addLayer (urlLayer, nameLayer, icon, hidden) {
  var eventMarkers = $.getJSON(urlLayer, function(json) {
    var layer;
    layer = L.markerClusterGroup({
      maxClusterRadius: 30
    }).addLayer(L.geoJson(json, {
      pointToLayer: function(feature, latlng) {
        return L.marker(latlng, {
          icon: icon
        });
      },
      onEachFeature: function(feature, layer) {
        with (feature.properties) {
          var popup = "<a href='"+ href+ "'>"+ name+ "</a>";
          if (feature.properties.tags) popup += "<br/>"+ tags.toString().replace(/\,/g, ", ");
          else "</a><br/>"+ city+ "("+ postalcode+ ")";
          return layer.bindPopup(popup);
        }
      }
    }));
    if (! hidden) map.addLayer(layer);
    controls.addOverlay(layer, nameLayer, {
      autoZindex: false
    });
  });
}

