/*
 * Copyright 2017-2018
 * - Loic Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Inspired from Benjamin 2016
 * https://www.bxnxg.com/minituto-01-jquery-autocomplete-ajax-cache-astuce-listing-suggestion-javascript-autocompletion/
 */

/* Display map */
var map;
var markers, icon;
var latlng;

function initFindPlace() {
    init();

    if (latitude!=0) {
        zoom = mapElementZoom;
        lat = latitude;
        lon = longitude;
        markers = L.marker( [ lat, lon ], {icon: eventIcon});
        markers.addTo(map);
        map.setView( [ lat, lon ], mapElementZoom);
    } else {
        map.setView([lat, lon]);
    }

    map.on('click', function(e) {
            latlng = e.latlng;
            if (markers) markers.remove();
            markers = L.marker(latlng, {icon: eventIcon});
            markers.addTo(map);
            document.forms[0].elements[fieldName+'[lon]'].value = latlng.lng;
            document.forms[0].elements[fieldName+'[lat]'].value = latlng.lat;
            searchPlace(latlng);
        }
    );

}

/* initialisation paramètres globaux : */
var plcache = {}; /* tableau cache de tous les termes */
var plterm = null; /* terme renseigné dans le champ input */
var place;
var locache = {}; /* tableau cache de tous les termes */
var loterm = null; /* terme renseigné dans le champ input */
var loc; /* location */


function searchPlace(lonlat) {
    plterm = lonlat.lng+'_'+lonlat.lat; /* récupération du terme renseigné dans l'input */
    if(plterm in plcache) { /* on vérifie que la clé "term" existe dans le tableau "cache", si oui alors on affiche le résultat */
        var result = plcache[plterm];
        displayPlResult(result);
    } else { /* sinon on fait une requête ajax vers city.php pour rechercher "term" */
        $('#plloading').attr('style','');
        var lat = lonlat.lat;
        var lon = lonlat.lng;
        /*console.log('https://nominatim.openstreetmap.org/reverse?'+'lat='+lat+'&lon='+lon+'&format=json'+'&addressdetails=1'+"&email="+moderatorlist);*/
        $.ajax({
            type:'GET',
            url:'https://nominatim.openstreetmap.org/reverse',
            data:'lat='+lat+'&lon='+lon+'&format=json'+'&addressdetails=1'+"&email="+moderatorlist,
            dataType:"json",
            async:true,
            cache:true,
            success:function(e){
                if(e.error){ /* si aucun résultats, on renvoi un tableau vide pour informer qu'on a rien trouvé */
                    var result = {
                        place:          'Aucune adresse trouvée... pointez directement sur la carte',
                        display_name:   null,
                        city:           null,
                        postcode:       null,
                        country:        null,
                        osm_id:         null,
                        osm_type:       null,
                        address:        null,
                    };
                }else{ /* sinon on renvoie toute le résultat */
                    var f = e.address;
                    var city = ((f.village) ? f.village :
                        ((f.town) ? f.town :
                            ((f.city) ? f.city :
                                ((f.county) ? f.county : f.address100)
                    )));
                    if (! city) city='';
                    city = city.replace('  ', ' ');
                    city = city.trim();
                    var field = [
                        'place_of_worship',
                        'address29',
                        'house_number',
                        'road',
                        'path',
                        'suburb',
                        'neighbourhood',
                    ];
                    var address = '';
                    for (n=0; n<field.length; n++) {
                        if (val = eval('f.'+field[n])) {
                            address += ((address>'') ? ', ' : '') + val;
                        }
                    }
                    address = address.trim();
                    var result = {
                        place:          e.display_name,
                        address:        address,
                        city:           city,
                        postcode:       f.postcode,
                        country:        f.country,
                        osm_id:         e.osm_id,
                        osm_type:       e.osm_type,
                    };  /* envoi du résultat à source avec map() de jQuery (permet d'appliquer une fonction pour tous les éléments d'un tableau */
                }
                $('#plloading').attr('style','display:none;');
                place = plcache[plterm] = result; /* vide ou non, on stocke la liste des résultats avec en clé "term" */
                //console.log(result);
                //console.log(f);
                displayPlResult();
            }
        });
    }
}

function displayPlResult() {
    var text = place.place;
    if ($('form input[name="'+fieldName+'[place]"]').val()=='' &
        $('form input[name="'+fieldName+'[address]"]').val()=='') {
        autoPlFill();
    } else {
        text += " <a href='#' onClick='javascript:autoPlFill(); return false;'>"+
            "<i class='fa fa-arrow-circle-o-down'></i>Remplir les champs en dessous avec cette adresse</a>";
    }
    $('#pl-container').html("<p>Trouvé: "+text+"</p>\n");
}

function autoPlFill() {
    $('form input[name="'+fieldName+'[place]"]').   val(place.place);
    $('form input[name="'+fieldName+'[address]"]'). val(place.address);
    $('form input[name="'+fieldName+'[city]"]').    val(place.city);
    if (place.postcode) {
        $('form input[name="'+fieldName+'[postalcode]"]').val(place.postcode);
        $('form input[name="'+fieldName+'[department]').val(place.postcode.substr(0,2));
    }
    $('form input[name="'+fieldName+'[country]"]'). val(place.country);
    $('form input[name="'+fieldName+'[osm_id]"]').  val(place.osm_id);
    $('form input[name="'+fieldName+'[osm_type]"]').val(place.osm_type);
}

function displayLoResult() {
    var text = "";
    var i = 0;
    if (loc.length == 0) {
        text += "<li>Aucune localisation trouvée à partir de l'adresse saisie</li>";
    } else {
        for (i=0; i < loc.length; i++) {
            f = loc[i];
            text += "<li>"+ f.place+ " ("+ f.osm_type+ ") "+
                "<a href='#' onClick='javascript:autoLoFill("+ i+ "); return false;'>"+
                "<i class='fa fa-arrow-circle-o-up'></i>Choisir le point avec ce lieu</a>"+
                "</li>";
        };
    }
    $('#pl-container').html("<ul>"+ text+ "</ul>\n");
    return false;
}

function autoLoFill(i) {
    locationChoosen = loc[i];
    $('form input[name="'+fieldName+'[lat]"]').     val(locationChoosen.lat);
    $('form input[name="'+fieldName+'[lon]"]').     val(locationChoosen.lon);
    $('form input[name="'+fieldName+'[place]"]').   val(locationChoosen.place);
    $('form input[name="'+fieldName+'[osm_id]"]').  val(locationChoosen.osm_id);
    $('form input[name="'+fieldName+'[osm_type]"]').val(locationChoosen.osm_type);
    var lat = locationChoosen.lat;
    var lon = locationChoosen.lon;

    if (markers) markers.remove();
    markers = L.marker( [ lat, lon ] , {icon: eventIcon});
    markers.addTo(map);
    map.setView( [ lat, lon ], mapElementZoom);
    return false;
}


function searchLocation() {
    loterm = $('form input[name="'+fieldName+'[address]"]').val()+ /* récupération du terme renseigné dans l'input */
        ","+ $('form input[name="'+fieldName+'[postalcode]"]').val()+
        ","+ $('form input[name="'+fieldName+'[city]"]').val()+
        ","+ $('form input[name="'+fieldName+'[country]"]').val();
    if(loterm in locache) { /* on vérifie que la clé "term" existe dans le tableau "cache", si oui alors on affiche le résultat */
        var result = locache[loterm];
        displayLoResult(result);
    } else { /* sinon on fait une requête ajax vers city.php pour rechercher "term" */
        $('#plloading').attr('style','');
        var query = loterm.replace(/ /g, " ");
        query = query.replace(/, /g, ",");
        query = query.replace(/ /g, "+");
        /*console.log('https://nominatim.openstreetmap.org/search?'+'q='+query+'&format=json'+'&addressdetails=1'+"&email="+moderatorlist);*/
        $.ajax({
            type:'GET',
            url:'https://nominatim.openstreetmap.org/search',
            data:'q='+query+'&format=json&email='+moderatorlist,
            dataType:"json",
            async:true,
            cache:true,
            success:function(e){
                result = [];
                if(e.error){ /* si aucun résultat, on renvoi un tableau vide pour informer qu'on a rien trouvé */
                    result.push = {
                        place:      'Aucune adresse trouvée...',
                        lat:        null,
                        lon:        null,
                        osm_id:     null,
                        osm_type:   null,
                    };
                }else{ /* sinon on renvoie toute le résultat */
                    e.forEach(function(f){
                        result.push({
                            place:      f.display_name,
                            lat:        f.lat,
                            lon:        f.lon,
                            osm_id:     f.osm_id,
                            osm_type:   f.osm_type,
                        });  /* envoi du résultat à source avec map() de jQuery (permet d'appliquer une fonction pour tous les éléments d'un tableau */
                    }); // end foreach
                }
                $('#plloading').attr('style','display:none;');
                loc = locache[loterm] = result; /* vide ou non, on stocke la liste des résultats avec en clé "term" */
                /*console.log(loc);*/
                displayLoResult();
            }
        });
    }
    return false;
}
