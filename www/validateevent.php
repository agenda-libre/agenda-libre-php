<?php

/*
 * Copyright 2008-2015
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * - Loic Dayot <ldayot A ouvaton POINT org>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

$privatePage = true;

include("inc/main.inc.php");
include("inc/class.event.inc.php");


if (isset($_POST['__event_validation_cancel']))
{
  header("Location: moderation.php");
  exit;
}
else if (isset($_POST['__event_validation_confirm']))
{
  $id = get_safe_integer('id', 0);

  /* Fetch contact email, in order to be able to send a confirmation
     mail */
  $event = new event($db, $id);
  if ($event->error)
  {
    put_header("Validation d'un évènement");
    error($event->message);
    put_footer();
    exit;
  }

  //$email = $event->contact;
  //$title = $event->title;
  //$secret = $event->secret;
  $link = calendar_absolute_url("showevent.php?id=".$event->id);

  /* Accept event */
  $sql = "UPDATE {$GLOBALS['db_tablename_prefix']}events SET moderated=1, decision_time=".
    $db->quote_smart($now). " WHERE id=". $db->quote_smart($event->id);
  $ret = $db->query ($sql);
  if ($ret == FALSE)
    {
      put_header("Validation d'un évènement");
      error ("La requête <i>" . $sql . "</i> a échoué");
      put_footer();
      exit;
    }

  /* Send email */
  if (substr($event->submitter,0,4)!="http")
     {
     $mailBody =
                "Bonjour,\n\n" .
            wordwrap("L'évènement que vous avez soumis a été modéré par " . $user->get_name() .
        ".\nIl est maintenant visible dans l'Agenda à l'adresse :\n") . $link . "\n\n" .
        wordwrap("Vous pouvez le modifier ultéreurement pour y ajouter des précisions en vous rendant à l'adresse :\n") .
        "  " . calendar_absolute_url("editevent.php?id=" . $event->id . "&secret=" . $event->secret) . "\n\n" .
        wordwrap("Vous pouvez également l'annuler en vous rendant à l'adresse :\n") .
        "  " . calendar_absolute_url("cancelevent.php?id=" . $event->id . "&secret=" . $event->secret) . "\n\n" .
        "=====================================================\n" .
        $event->formatAscii() . "\n" .
        "=====================================================\n\n" .
        "Merci de votre contribution à $adl_title et à bientôt !\n\n".
        "-- \nL'équipe de modération";

        calendar_mail ($event->submitter,
        "Évènement '" . $event->title . "' modéré",
        $mailBody,  $event->id);
        calendar_mail ($moderatorlist,
        "Évènement '" . $event->title . "' modéré",
        $mailBody,  $event->id);
        }

  if ($event->submitter != $event->contact && substr($event->contact,0,4)!="http")
      {
        $mailBody =
                "Bonjour,\n\n" .
                "Un évènement dont vous êtes le contact a été publié dans {$adl_title}" .
                ".\nIl est maintenant visible à l'adresse\n" . $link . "\n\n" .
                "=====================================================\n" .
                $event->formatAscii() . "\n" .
                "=====================================================\n\n" .
                "*Nous vous invitons à déposer dorénavant vous-mêmes les événements à publier sur l'agenda.*\n\n".
                "À bientôt sur {$adl_title}.\n\n".
                "-- \nL'équipe de modération.";

            calendar_mail ($event->contact,
                "Évènement '" . $event->title . "' publié",
                $mailBody,
                $event->id);
        }

  header("Location: moderation.php");
  exit;
}

$id = get_safe_integer('id', 0);

put_header("Validation d'un évènement");

$event = new event($db, $id);
if (! $event)
{
  echo "<p>Pas d'évènement avec cet ID.</p>";
  put_footer();
  exit;
}

if ($event->moderated)
{
  echo "<p>Évènement déjà modéré.</p>";
  put_footer();
  exit;
}

echo '<p class="moderationheader">';
echo "<a href=\"moderation.php\">Modération</a>&nbsp;&gt;&gt;&gt&nbsp;Validation de l'évènement";
echo "</p>";

echo '<div class="moderationbox">';

if ($event->locality >= 0)
{
  echo '<p style="color: red; font-weight: bold; text-align: center;">';
  echo "Attention, cet évènement est à portée ".
    $adl_locality[$event->locality]. " !";
  echo '</p>';
}

echo '<form action="validateevent.php?id=' . $id . '" method="post">';
echo '<p class="moderationheader">Confirmez-vous la validation de cet évènement&nbsp;?</p>';
echo '<input name="__event_validation_confirm" type="submit" value="Oui"/>&nbsp;';
echo '<input name="__event_validation_cancel" type="submit" value="Non"/>';
echo '</form>';
echo '</div>';

echo '<div class="moderationbox">';
echo $event->formatHTML($event);
echo '</div>';

put_footer();

?>
