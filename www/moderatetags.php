<?php

/*
 * Copyright 2004-2016
 * - Loic Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

$privatePage = true;

include("inc/main.inc.php");

put_header(_("Gestion des mots-clés"));

function tagCategoryListHTML()
{
  global $db;

  $return = "<ul>\n";

  $catQuery = "SELECT COUNT(*) as n FROM {$GLOBALS['db_tablename_prefix']}tags WHERE category_id=0 GROUP BY category_id";
  $catResult = $db->query($catQuery);
  while ($catRecord = $db->fetchObject($catResult))
    $return .= "  <li><a href='moderatetags.php?op=catListTag&id=0' title=\"". sprintf(_("Liste des mots-clés de la catégorie %s"), _("sans catégorie")). "\">". sprintf(_("%03s mots-clés"), $catRecord->n). "</a> <strong>". _("sans catégorie"). "</strong></li>\n";
  $db->freeResult($catResult);

  $catQuery = "SELECT COUNT(tags.id) as n, tags_categories.* FROM {$GLOBALS['db_tablename_prefix']}tags_categories AS tags_categories ".
    "LEFT JOIN {$GLOBALS['db_tablename_prefix']}tags AS tags ON tags_categories.id=category_id ".
    "GROUP BY tags_categories.id ".
    "ORDER BY tags_categories.name";
  $catResult = $db->query($catQuery);

  while ($catRecord = $db->fetchObject($catResult))
  {
    $return .= "  <li><a href='moderatetags.php?op=catListTag&id={$catRecord->id}' title=\"". sprintf(_("Liste des tags de la catégorie %s"), $catRecord->name). "\">". sprintf(_("%03s mots-clés"), $catRecord->n). "</a> <strong>{$catRecord->name}</strong> <sapn class='description'>{$catRecord->description}</span>";
    if ($catRecord->id>0) $return .= " - <a href='moderatetags.php?op=catEdit&id={$catRecord->id}' title=\"". sprintf(_("Modifier la catégorie %s"), $catRecord->name). "\">". _("Modifier"). "</a>";
    if ($catRecord->n==0) $return .= " - <a href='moderatetags.php?op=catDel&id={$catRecord->id}' title=\"". sprintf(_("Supprimer la catégorie %s"), $catRecord->name). "\">". _("Supprimer"). "</a>";
    $return .= "</li>\n";
  }

  $db->freeResult($catResult);

  $return .= "<li><a href='moderatetags.php?op=catNew'>".
   _("Nouvelle catégorie de mots-clés"). "</a></li>\n";

  $return .= "</ul>\n";

  return $return;
} // end function tagCategoryListHTML

function tagCategoryFormHTML($id)
{
  $return = "<form action=\"moderatetags.php\" method=\"post\" enctype=\"multipart/form-data\" name=\"tagCategoryForm\">\n";
  $return .= "<input type=\"hidden\" name=\"op\" value=\"catUpdate\" />\n";

  if (isset($_POST["tagCat"]))
  { // We came from form
    $tagCat = $_POST["tagCat"];
    $tagCat["name"] = stripslashes(strip_tags($tagCat["name"]));
    $tagCat["description"] = stripslashes(strip_tags($tagCat["description"], "<p><b><i><br/><a><ul><li><ol>"));
  } // end if
  else
  {
    if ($id>0)
    { // edit
     global $db;
     $res = $db->query("SELECT * FROM {$GLOBALS['db_tablename_prefix']}tags_categories WHERE id='{$id}'");
      if (! $tagCat = $db->fetchArray($res)) return error(_("Enregistrement introuvable"));
      $return .= "<input type=\"hidden\" name=\"id\" value=\"{$tagCat['id']}\" />\n";
      $db->freeResult($res);
      $tagCat['name'] = stripslashes(strip_tags($tagCat['name'], ""));
      $tagCat['description'] = stripslashes(strip_tags($tagCat['description'], "<p><b><i><br/><a><ul><li><ol>"));
    } // end if
    else
    { // new
      $tagCat = array("name"=>null, "description"=>null);
    } // end else
  } // end else

  $return .= "<table>";
  $return .= "<tr><th colspan='2'>". _("édition d'une catégorie de mots-clés"). "</th></tr>\n";
  $return .= "<tr><th>". _("Intitulé*"). "</th> <td><input type=\"text\" name=\"tagCat[name]\" value=\"{$tagCat['name']}\" size='40' /></td></tr>\n";
  $return .= "<tr><th>". _("Description"). "</th> <td><textarea name=\"tagCat[description]\" rows=\"4\" cols=\"70\">{$tagCat['description']}</textarea></td></tr>\n";

  $return .= "<tr><th colspan='2'><input type=\"submit\" name=\"submit\" value=\"". _("Valider"). "\" /></th></tr>\n";

  $return .= "</table>\n</form>\n";

  return $return;
} // end function tagCategoryFormHTML


function tagCategoryUpdate($id)
{
  global $db;
  if (!isset($_POST["tagCat"]))
     return error(_("Ne venant pas du formulaire"));
  $tagCat = $_POST["tagCat"];
  if (!is_array($tagCat) || !isset($tagCat["name"], $tagCat["description"]))
    return error(_("Ne venant pas du bon formulaire"));
  if (strlen($tagCat["name"])<4)
    return error(_("Intitulé indispensable d'au moins 4 caractères"));

  if (! preg_match("~^[a-z0-9\-]*$~", $tagCat["name"]))
    return error("L'intitulé ne doit contenir que des lettres minuscules, ".
    "des chiffres ou des tirets.");

  $tagCat["name"] = stripslashes(strip_tags($tagCat["name"]));
  $tagCat["description"] = stripslashes(strip_tags($tagCat["description"], "<p><b><i><br/><a><ul><li><ol>"));

  // Detect double
  $res = $db->query("SELECT * FROM {$GLOBALS['db_tablename_prefix']}tags_categories ".
    "WHERE name LIKE ". $db->quote_smart($tagCat['name']). ($id>0 ? " AND id<>$id" : ""));
  if ($db->fetchObject($res))
  { // tag already exists
    return error(sprintf(_("La catégorie de mots-clés %s existe déjà"), $tagCat["name"]));
  }

  $query = "{$GLOBALS['db_tablename_prefix']}tags_categories SET name=". $db->quote_smart($tagCat['name']). ", description=". $db->quote_smart($tagCat['description']);
  $query = ($id==0 ? "INSERT INTO ". $query : "UPDATE ". $query. " WHERE id='$id'");
  $ok = $db->query($query);
  if ($ok && $id==0) $_REQUEST['id'] = $db->insertid();

  return $ok;
}

function catDelConfirmFormHTML($id)
{
  if ($id==0) return error(_("Catégorie de mots-clés non sélectionné"));

  global $db;
  $res = $db->query("SELECT * FROM {$GLOBALS['db_tablename_prefix']}tags_categories WHERE id='{$id}'");
  if (! $cat = $db->fetchArray($res)) return error(_("Catégorie de mots-clés introuvable"));
  $db->freeResult($res);

  $return = "<form action=\"moderatetags.php\" method=\"post\" enctype=\"multipart/form-data\" name=\"tagCategoryForm\">\n";
  $return .= "<input type=\"hidden\" name=\"op\" value=\"catDelete\" />\n";

  $return .= "<input type=\"hidden\" name=\"id\" value=\"{$cat['id']}\" />\n";
  $return .= "<p>". sprintf(_("Vous êtes sur le point de supprimer la catégorie de mots-clés %s"), "<strong>{$cat['name']}</strong>"). "</p>\n";
  $return .= "<p><label for=\"confirm\">". _("Confirmer la suppression"). "<input type=\"checkbox\" id=\"confirm\" name=\"confirm\" value=\"yes\" /></label></p>";

  $return .= "<p><input type=\"submit\" name=\"submit\" value=\"". _("Valider"). "\" /></p>\n";

  $return .= "</form>\n";

  return $return;
} // end function catDelConfirmForm

function catDelete($id)
{
  global $db;
  if (!isset($_POST["id"]))
    return error(_("Ne venant pas du bon formulaire"));;
  if (!isset($_POST["confirm"]) || $_POST["confirm"]!="yes")
    return error(_("Supression non confirmée"));

  // Find cat
  $res = $db->query("SELECT * FROM {$GLOBALS['db_tablename_prefix']}tags_categories ".
    "WHERE id=". $db->quote_smart($_POST['id']));
  if (! $cat=$db->fetchArray($res))
  { // tag already exists
    return error(_("Enregistrement introuvable"));
  }
  $db->freeresult($res);

  // Is tag used ?
  $res = $db->query("SELECT * FROM {$GLOBALS['db_tablename_prefix']}tags ".
    "WHERE category_id=". $db->quote_smart($_POST['id']));
  if ($db->fetchObject($res))
  { // tag already exists
    $db->freeresult($res);
    return error(sprintf(_("La catégories %s comporte des mots-clés"), $cat["name"]));
  }

  $query = "DELETE FROM {$GLOBALS['db_tablename_prefix']}tags_categories ".
    "WHERE id=". $db->quote_smart($cat['id']);
  if ($ok = $db->query($query))
    echo "<p>". _("Catégorie de mots-clés supprimée")."</p>\n";
  else
    error(_("Impossible de supprimer la catégorie de mots-clés"));

  return $ok;
} // end function catDelete()

function tagListHTML($id)
{
  global $db;
  if ($id>0)
  {
    $res = $db->query("SELECT * FROM {$GLOBALS['db_tablename_prefix']}tags_categories ".
        "WHERE id='{$id}'");
    if (! $tagCat = $db->fetchObject($res)) return error(_("Enregistrement introuvable"));
    $db->freeResult($res);
    $return ="<h3>{$tagCat->name}</h3>\n";
    $return .= "<p class=\"description\">{$tagCat->description}</p>";
  }
  else
  {
    $return ="<h3>". _("Sans catégorie"). "</h3>\n";
  }

  $return .= "<ul>\n";

  $res = $db->query("SELECT {$GLOBALS['db_tablename_prefix']}tags.*, COUNT(event_id) AS n FROM {$GLOBALS['db_tablename_prefix']}tags ".
    "LEFT JOIN {$GLOBALS['db_tablename_prefix']}tags_events ON {$GLOBALS['db_tablename_prefix']}tags.id=tag_id ".
    "WHERE category_id='{$id}' GROUP BY {$GLOBALS['db_tablename_prefix']}tags.id ORDER BY {$GLOBALS['db_tablename_prefix']}tags.name");
  while ($tag = $db->fetchObject($res))
  {
    $return .= "  <li>". sprintf("%03s", $tag->n).
     " <a href=\"listevents.php?tag={$tag->name}\" title=\"".
     sprintf(_("Liste des événements portant le mot-clé %s"), $tag->name).
     "\">{$tag->name}</a> <span class='description'>{$tag->description}</span>";
    $return .= " - <a href='moderatetags.php?op=tagEdit&id={$tag->id}' title=\"".
     sprintf(_("Modifier le mot-clé %s"), $tag->name). "\">". _("Modifier"). "</a>";
    if ($tag->n==0)
      $return .= " - <a href='moderatetags.php?op=tagDel&id={$tag->id}' title=\"".
       sprintf(_("Supprimer le mot-clé %s"), $tag->name). "\">". _("Supprimer"). "</a>";
    else
        {
                $return .= " - <a href='moderatetags.php?op=tagTr&id={$tag->id}' title=\"".
                 sprintf(_("Transférer le mot-clé %s vers un autre mot-clé"), $tag->name).
                     "\">". _("Transférer"). "</a>";
          $return .= " - <a href='moderatetags.php?op=tagDel&id={$tag->id}' title=\"".
         sprintf(_("Supprimer le mot-clé %s et ses utilisations !"), $tag->name). "\">". _("Supprimer malgré son utilisation"). "</a>";
            }
    $return .= "</li>\n";
  } // end while

  if ($id>0) $return .= "<li><a href='moderatetags.php?op=tagNew&id=".
   "{$tagCat->id}' title=\"". sprintf(_("Nouveau mot-clé dans la catégorie %s"),
   $tagCat->name). "\">". _("Nouveau mot-clé"). "</a></li>";
  $return .= "</ul>\n";

  if ($id>0) $return .= "<a href='moderatetags.php?op=catEdit&id={$tagCat->id}' title=\"". sprintf(_("Modifier la catégorie %s"), $tagCat->name). "\">". _("Modifier"). "</a>";
  if ($db->numRows($res)==0) $return .= " - <a href='moderatetags.php?op=catDel&id={$tagCat->id}' title=\"". sprintf(_("Supprimer la catégorie %s"), $tagCat->name). "\">". _("Supprimer"). "</a>";

  $db->freeResult($res);

  return $return;
} // end function tagListHTML

function selectTagCategoryHTML($id, $field="category_id")
// Function to choose tag category ($id)
// $id is (int) tag category or (int) 0
// $field (string) name of select form
// output if HTML string select part of form
{
  global $db;
  $ret = $db->query ("SELECT * FROM {$GLOBALS['db_tablename_prefix']}tags_categories");
  if ($ret == FALSE)
  {
    error ("Erreur lors de la recherche des catégories de mots-clés");
    put_footer();
    exit;
  }
  $return = "<select name='$field'>";
  $return .= " <option value='0' ". ($id==0 ? "selected='selected' " : ""). ">". _("(aucune)"). "</option>";
  while ($row = $db->fetchObject($ret))
  {
    $return .= " <option value='{$row->id}' ". ($row->id==$id ? "selected='selected' " : ""). ">{$row->name}</option>";
  }
  $db->freeResult($ret);
  $return .= " </select>\n";
  return $return;
} // end function selectPlaceHTML()

function tagFormHTML($id, $category_id)
{
  $return = "<form action=\"moderatetags.php\" method=\"post\" enctype=\"multipart/form-data\" name=\"tagCategoryForm\">\n";
  $return .= "<input type=\"hidden\" name=\"op\" value=\"tagUpdate\" />\n";

  if (isset($_POST["tag"]))
  { // We came from form
    $tag = $_POST["tag"];
  } // end if
  else
  {
    if ($id>0)
    { // edit
     global $db;
     $res = $db->query("SELECT * FROM {$GLOBALS['db_tablename_prefix']}tags WHERE id='{$id}'");
      if (! $tag = $db->fetchArray($res)) return error(_("Enregistrement introuvable"));
      $return .= "<input type=\"hidden\" name=\"id\" value=\"{$tag['id']}\" />\n";
      $db->freeResult($res);

      $tag['name'] = stripslashes(strip_tags($tag["name"]));
      $tag['description'] = stripslashes(strip_tags($tag["description"], "<p><b><i><br/><a><ul><li><ol>"));
    } // end if
    else
    { // new
      $tag = array("name"=>null, "description"=>null, "category_id"=>$category_id);
    } // end else
  } // end else

  $return .= "<table>";
  $return .= "<tr><th colspan='2'>". _("Édition d'un mot-clé"). "</th></tr>\n";
  $return .= "<tr><th>". _("Intitulé*"). "</th> <td><input type=\"text\" name=\"tag[name]\" value=\"{$tag['name']}\" size='40' /></td></tr>\n";
  $return .= "<tr><th>". _("Catégorie*"). "</th> <td>". selectTagCategoryHTML($tag['category_id'], "tag[category_id]"). "</td></tr>\n";
  $return .= "<tr><th>". _("Description"). "</th> <td><textarea name=\"tag[description]\" rows=\"4\" cols=\"70\">{$tag['description']}</textarea></td></tr>\n";

  $return .= "<tr><th colspan='2'><input type=\"submit\" name=\"submit\" value=\"". _("Valider"). "\" /></th></tr>\n";

  $return .= "</table>\n</form>\n";

  return $return;
} // end function tagFormHTML

function tagUpdate($id)
{
  global $db, $tagMinSize, $tagMaxSize;
  if (!isset($_POST["tag"]))
     return error(_("Ne venant pas du formulaire"));
  $tag = $_POST["tag"];
  if (!is_array($tag) || !isset($tag["name"], $tag["category_id"], $tag["description"]))
    return error(_("Ne venant pas du bon formulaire"));;
  if (! preg_match("~^[a-z0-9\-]*$~", $tag["name"]))
    return error("L'intitulé ne doit contenir que des lettres minuscules, ".
    "des chiffres ou des tirets.");
  if (strlen($tag["name"]) < $tagMinSize || strlen($tag["name"]) > $tagMaxSize)
    return error(sprintf("Tag '" . $tag["name"] . "' trop court ou trop long. ".
                "La longueur des tags doit être de %s à %s caractères.",
                $tagMinSize, $tagMaxSize));

  $tagCat["name"] = stripslashes(strip_tags($tag["name"]));
  $tagCat["description"] = stripslashes(strip_tags($tag["description"], "<p><b><i><br/><a><ul><li><ol>"));

  // Detect double
  $res = $db->query("SELECT * FROM {$GLOBALS['db_tablename_prefix']}tags ".
    "WHERE name LIKE ". $db->quote_smart($tag['name']). ($id>0 ? " AND id<>$id" : ""));
  if ($db->fetchObject($res))
  { // tag already exists
    return error(sprintf(_("Le mot-clé %s existe déjà"), $tag["name"]));
  }

  $query = "{$GLOBALS['db_tablename_prefix']}tags SET name=". $db->quote_smart($tag['name']).
    ", category_id=". $db->quote_smart($tag['category_id']).
    ", description=". $db->quote_smart($tag['description']);
  $query = ($id==0 ? "INSERT INTO ". $query : "UPDATE ". $query. " WHERE id='$id'");
  $ok = $db->query($query);
  if ($ok && $id==0) $_REQUEST['id'] = $db->insertid();

  return $ok;
}

function tagDelConfirmFormHTML($id, $askForce=false)
{
  if ($id==0) return error(_("Mot-clé non sélectionné"));

  global $db;
  $res = $db->query("SELECT * FROM {$GLOBALS['db_tablename_prefix']}tags WHERE id='{$id}'");
  if (! $tag = $db->fetchArray($res)) return error(_("Mot-clé introuvable"));
  $db->freeResult($res);

  $return = "<form action=\"moderatetags.php\" method=\"post\" enctype=\"multipart/form-data\" name=\"tagDelConfirmForm\">\n";
  $return .= "<input type=\"hidden\" name=\"op\" value=\"tagDelete\" />\n";

  $return .= "<input type=\"hidden\" name=\"id\" value=\"{$tag['id']}\" />\n";
  $return .= "<input type=\"hidden\" name=\"category_id\" value=\"{$tag['category_id']}\" />\n";
  $return .= "<p>". sprintf(_("Vous êtes sur le point de supprimer le mot-clé %s"),
   "<strong>{$tag['name']}</strong>"). "</p>\n";
  $return .= "<p><label for=\"confirm\">". _("Confirmer la suppression"). "<input type=\"checkbox\" id=\"confirm\" name=\"confirm\" value=\"yes\" /></label></p>";
  if ($askForce)
    {
        $return .= "<p><label for=\"force\">". _("Confirmer la suppression malgré que le mot clé soit utilisé").
            "<input type=\"checkbox\" id=\"force\" name=\"force\" value=\"yes\" /></label></p>";
        }

  $return .= "<p><input type=\"submit\" name=\"submit\" value=\"". _("Valider"). "\" /></p>\n";

  $return .= "</form>\n";

  return $return;
} // end function tagDelConfirmForm

function tagDelete($id)
{
  global $db;
  if (!isset($_POST["id"], $_POST["category_id"]))
    return error(_("Ne venant pas du bon formulaire"));;
  if (!isset($_POST["confirm"]) || $_POST["confirm"]!="yes")
    return error(_("Supression non confirmée"));

  // Find tag
  $res = $db->query("SELECT * FROM {$GLOBALS['db_tablename_prefix']}tags ".
    "WHERE id=". $db->quote_smart($_POST['id']));
  if (! $tag=$db->fetchArray($res))
  { // tag already exists
    return error(_("Enregistrement introuvable"));
  }
  $db->freeresult($res);

  // Is tag used ?
  $res = $db->query("SELECT * FROM {$GLOBALS['db_tablename_prefix']}tags_events ".
    "WHERE tag_id=". $db->quote_smart($_POST['id']));
  if ($db->fetchObject($res))
  { // tag exists
    $db->freeresult($res);
    if (! isset($_POST['force']))
        return error(sprintf(_("Le mot-clé %s est utilisé. Confirmer quand même sa <a href='%s'>suppression</a>."),
            $tag["name"], "moderatetags.php?op=tagDel&id={$id}&exist=1"));
    // delete tags
      $res = $db->query("DELETE FROM {$GLOBALS['db_tablename_prefix']}tags_events ".
        "WHERE tag_id=". $db->quote_smart($_POST['id']));
      echo "<p>". _("Utilisation(s) du mot-clé supprimée(s)")."</p>\n";
  }

  $query = "DELETE FROM {$GLOBALS['db_tablename_prefix']}tags ".
    "WHERE id=". $db->quote_smart($tag['id']);
  if ($ok = $db->query($query))
    echo "<p>". _("Mot-clé supprimé")."</p>\n";
  else
    error(_("Impossible de supprimer le mot-clé"));

  return $ok;
}

// Function to choose tags
// $aTags is an array (category_id=>tag_name)
// output if HTML string select part of form
function selectTagHTML($id, $field="tag_id", $category_id=null)
{
  global $db;
  // TODO Select only tags used twice or more.
  // Or sort and class tags in alphabetical optgroup
  $ret = $db->query ("SELECT tags.id AS id, tags.name AS tag, tags.description AS tag_description, tags_categories.id AS category_id, tags_categories.name AS category, tags_categories.description as description ".
    "FROM {$GLOBALS['db_tablename_prefix']}tags AS tags LEFT JOIN {$GLOBALS['db_tablename_prefix']}tags_categories AS tags_categories ON tags_categories.id=category_id GROUP BY category_id, tags.id ORDER BY tags_categories.name, tags.name");
  if ($ret == FALSE)
  {
    error (_("Erreur lors de la recherche des mots-clés"));
    put_footer();
    exit;
  }
  $category_id=-1;
  $return = "<select name=\"$field\">";
  $return .= "  <option></option>";
  while ($row = $db->fetchObject($ret))
  {
    if ($category_id!=$row->category_id)
    {
      if ($category_id!=-1) $return .= "<optgroup>\n";
      $return .= " <optgroup label=\"{$row->category}". ($row->description>"" ? " - {$row->description}" : ""). "\">";
      $category_id = $row->category_id;
    } // end if
    $return .= "  <option value='{$row->id}'". ($id==$row->id ? " selected='selected'" : ""). ">{$row->tag}". ($row->tag_description>"" ? " - {$row->tag_description}" : ""). "</option>";
  } // end while
  $db->freeResult($ret);
  $return .= "</select>\n";

  return $return;
} // end function selectTagHTML()

function tagTransferFormHTML($id)
{
  if ($id==0) return error(_("Mot-clé non sélectionné"));

  global $db;

  // find tag
  $res = $db->query("SELECT * FROM {$GLOBALS['db_tablename_prefix']}tags WHERE id='{$id}'");
  if (! $tag = $db->fetchArray($res)) return error(_("Mot-clé introuvable"));
  $db->freeResult($res);

  // How many events with this tag
  $res = $db->query("SELECT event_id AS n FROM {$GLOBALS['db_tablename_prefix']}tags_events WHERE tag_id='{$id}'");
  $tagevent = $db->numRows($res);
  $db->freeResult($res);

  $return = "<form action=\"moderatetags.php\" method=\"post\" enctype=\"multipart/form-data\" name=\"tagTransferForm\">\n";
  $return .= "<input type=\"hidden\" name=\"op\" value=\"tagTransfer\" />\n";
  $return .= "<input type=\"hidden\" name=\"id\" value=\"{$tag['id']}\" />\n";
  $return .= "<input type=\"hidden\" name=\"category_id\" value=\"{$tag['category_id']}\" />\n";

  $return .= "<p>". sprintf(_("Vous souhaitez retirer le mot-clé %s des %s événements"), $tag['name'], $tagevent);

  $return .= "<br />". _("et les marquer avec le nouveau mot-clé*"). " ". selectTagHTML($id, "tag_id"). "</p>\n";

  $return .= "<p><label for=\"confirm\">". _("Confirmer le transfert"). "<input type=\"checkbox\" id=\"confirm\" name=\"confirm\" value=\"yes\" /></label></p>";

  $return .= "<p><input type=\"submit\" name=\"submit\" value=\"". _("Valider"). "\" /></p>\n";

  $return .= "</form>\n";

  return $return;
}

// Unmark events with $id_from tag
// and mark with $id_to tag
function tagTransfer($id_from, $id_to)
{
  if ($id_from==0 || $id_to==0) return error(_("Mot-clé non sélectionné"));

  global $db;

  // find from tag
  $res = $db->query("SELECT * FROM {$GLOBALS['db_tablename_prefix']}tags WHERE id='{$id_from}'");
  if (! $tag_from = $db->fetchObject($res)) return error(_("Mot-clé introuvable"));
  $db->freeResult($res);

  // find to tag
  $res = $db->query("SELECT * FROM {$GLOBALS['db_tablename_prefix']}tags ".
    "WHERE id='{$id_to}'");
  if (! $tag_to = $db->fetchObject($res)) return error(_("Mot-clé introuvable"));
  $db->freeResult($res);

  // select events
  $query = "SELECT event_id FROM {$GLOBALS['db_tablename_prefix']}tags_events ".
    "WHERE tag_id=". $db->quote_smart($id_from);
  $res = $db->query($query);
  $ok = $res;
  $aEvents = array();
  while ($rec = $db->fetchObject($res)) $aEvents[] = $rec->event_id;
  $db->freeResult($res);

  if (count($aEvents)>0) {
    $lEvents = implode(",", $aEvents);
    // remove mark events from
    $query = "DELETE FROM {$GLOBALS['db_tablename_prefix']}tags_events WHERE tag_id=". $db->quote_smart($id_from). " AND event_id IN (". $lEvents. ")";
    $res = $db->query($query);
    $ok &= $res;

    // mark events to
    $sep = "";
    $query = "REPLACE INTO {$GLOBALS['db_tablename_prefix']}tags_events ".
      "(event_id, tag_id) VALUES ";
    foreach ($aEvents as $event) {
        $query.= $sep. " (". $event. ", ". $id_to. ")";
        $res = ",";
    }
    $res = $db->query($query);
    $ok &= $res;
  } // end if

  return $ok;
} // end function tagTransfer($id_from, $id_to)

/*
 *
 * Main page
 *
 */

$op = (isset($_REQUEST["op"]) ? $_REQUEST["op"] : null);

switch ($op) // compute
{

  case "tagUpdate" :
  {
    if ($ret = tagUpdate(isset($_POST["id"]) ? $_POST["id"] : 0))
    { // good, return to tag list
      $op = "catListTag";
      $_REQUEST['id'] = $_POST["tag"]['category_id'];
    }
    else
    { // error return to tag form
      $op = (isset($_POST["id"]) ? "tagEdit" : "tagNew");
    }
    break;
  }

  case "tagDelete" :
  {
    $ret = tagDelete($_POST["id"]);
    $op = "catListTag";
    $_REQUEST['id'] = $_POST["category_id"];
    break;
  }

  case "tagTransfer" :
  {
    if (isset($_POST["confirm"]) && $_POST["confirm"]=="yes")
       $ret = tagTransfer($_POST["id"], $_POST["tag_id"]);
    $op = "catListTag";
    $_REQUEST['id'] = $_POST["category_id"];
    break;
  }

  case "catUpdate" :
  {
    $ret = tagCategoryUpdate(isset($_POST["id"]) ? $_POST["id"] : 0);
    $op = ( $ret ? "catList" : (isset($_POST["id"]) ? "catEdit" : "catNew") );
    break;
  }

  case "catDelete" :
  {
    $ret = catDelete($_POST["id"]);
    $op = "catList";
    break;
  }

} // end switch compute


switch ($op) // display
{

  case "tagDel" :
  {
    echo tagDelConfirmFormHTML($_REQUEST["id"], isset($_REQUEST["exist"]));
    break;
  }

  case "tagNew" :
  {
    echo tagFormHTML(0, isset($_REQUEST["id"]) ? $_REQUEST["id"] : 0);
    break;
  }

  case "tagEdit" :
  {
    echo tagFormHTML($_REQUEST["id"], 0);
    break;
  }

  case "tagTr" :
  {
    echo tagTransferFormHTML($_REQUEST["id"]);
    break;
  }

  case "catDel" :
  {
    echo catDelConfirmFormHTML($_REQUEST["id"]);
    break;
  }

  case "catNew" :
  {
    echo tagCategoryFormHTML(0);
    break;
  }

  case "catEdit" :
  {
    echo tagCategoryFormHTML($_REQUEST["id"]);
    break;
  }

  case "catDel" :
  {
    echo tagCategoryDelHTML($_REQUEST["id"]);
    break;
  }

  case "catListTag" :
  {
    echo tagListHTML($_REQUEST["id"]);
    break;
  }

  default : // list categories
  {
    // showTagCategoryList
    echo tagCategoryListHTML();
    break;
  }

} // end switch display

if ($op!="" && $op!="catList")
  echo "<p><a href=\"moderatetags.php\">".
   _("Liste des catégories de mots-clés"). "</a></p>\n";

put_footer();

?>
