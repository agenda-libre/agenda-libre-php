<?php

/*
 * Copyright 2004-2017
 * - Mélanie Bats <melanie POINT bats CHEZ utbm POINT fr>
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * - Loic Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

$privatePage = false;

include("inc/main.inc.php");
include_once("inc/class.event.inc.php");
include_once("inc/class.export.inc.php");

$eventList = new exportEvent($db);
$eventList->get2fields();

echo $eventList->rssStartCalendar();

$list = $eventList->getEventsList();

if ($list == FALSE)
{
  error ("Erreur lors de la récupération des évènements");
  exit;
}

while ($eventList->event->id = $db->getOne($list))
{
    echo $eventList->rssEventHeaderCalendar();
}
echo $eventList->rssEndHeaderCalendar();

$list = $eventList->getEventsList();
while ($eventList->event->id = $db->getOne($list))
{
  $eventList->event->get();
  echo $eventList->event->toRss($eventList->map);
}

echo $eventList->rssEndCalendar();

?>
