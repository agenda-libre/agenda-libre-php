<?php

/*
 * Copyright 2018
 * - Loic Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

$privatePage = false;

include("inc/main.inc.php");
include_once("inc/class.event.inc.php");
include_once("inc/class.export.inc.php");

$eventList = new exportEvent($db);
$eventList->get2fields();
$format = get_safe_string('format', 'rss');

if (($list = $eventList->getEventsList()) == FALSE)
{
  error ("Erreur lors de la récupération des évènements");
  exit;
}


switch ($format) {

    case 'js' : {
        echo $eventList->jsStartCalendar();
        while ($eventList->event->id = $db->getOne($list))
        {
            $eventList->event->get();
            echo $eventList->event->toJs();
        }
        echo $eventList->jsEndCalendar();
        break;
    }

    case 'ical' : {
        echo $eventList->icalStartCalendar();
        while ($eventList->event->id = $db->getOne($list))
        {
            $eventList->event->get();
            echo $eventList->event->toIcal();
        }
        echo $eventList->icalEndCalendar();
        break;
    }

    case 'kml' : {
        echo $eventList->kmlStartCalendar();

        while ($eventList->event->id = $db->getOne($list))
        {
          $eventList->event->get();
          echo $eventList->event->toKml();
        }
        echo $eventList->kmlEndCalendar();
        break;
    }

    case 'geojson' : {
        echo $eventList->geojsonStartCalendar();
        $isfirst=true;
        while ($eventList->event->id = $db->getOne($list))
        {
          $eventList->event->get();
          if ($isfirst) {
            $isfirst = false;
          } else {
            echo ",\n";
          }
          echo $eventList->event->toGeojson();
        }
        echo $eventList->geojsonEndCalendar();
        break;
    }

    // TODO : XML from old xmllistevents.php
    case 'xml' : {
        $xml = $eventList->XmlStartCalendar($xml = new xmlWriter());
        while ($eventList->event->id = $db->getOne($list))
        {
          $eventList->event->get();
          $eventList->event->toXml($xml);
        }
        echo $eventList->XmlEndCalendar($xml);
        break;
    }

    default :
    case 'georss' :
    case 'rss' : {
        echo $eventList->rssStartCalendar();
        while ($eventList->event->id = $db->getOne($list))
        {
            echo $eventList->rssEventHeaderCalendar();
        }
        echo $eventList->rssEndHeaderCalendar();
        $list = $eventList->getEventsList();
        while ($eventList->event->id = $db->getOne($list))
        {
          $eventList->event->get();
          echo $eventList->event->toRss($format == 'georss');
        }
        echo $eventList->rssEndCalendar();
        break;
    }
} // end switch

?>
