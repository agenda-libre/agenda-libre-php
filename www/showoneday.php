<?php

/*
 * Copyright 2004-2017
 * - Mélanie Bats <melanie POINT bats CHEZ utbm POINT fr>
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * - Loic Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

include("inc/main.inc.php");

$date = isset($_GET['date']) ? $_GET['date'] : date("Y-n-j");

$human_date = onlyday_timestamp2humanreadable(date_mysql2timestamp($date));

$year = substr($date,0,4);
$month = substr($date,5,2);
$day = substr($date,8,2);

put_header("Tous les événements du ". $human_date);

echo "<h2>Tous les événements du ". $human_date. "</h2>\n\n";

echo "<div id=\"localgroup-list\">\n";
// Display actions in context div
echo "<h1>Actions</h1>\n";
echo "<a href='submit.php?start=". strtotime($date). "'>".
  "<i class='fas fa-edit'></i>Proposer un &eacute;v&eacute;nement</a>\n";
echo "</h1>";
echo "</div>\n";

show_day_events ($db, $day, $month, $year, $region="all", $count=100);

put_footer();

?>
