-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Client: mysql2-vip.ouvaton.local
-- Généré le: Dim 04 Novembre 2012 à 17:20
-- Version du serveur: 5.0.51a-24+lenny5-log
-- Version de PHP: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
SET NAMES 'utf8';

-- --------------------------------------------------------
-- Prefix setting
--
SET  @tablePrefix = "adl_";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Contenu de la table `regions`
--

SET @s = CONCAT("TRUNCATE TABLE ", @tablePrefix, "regions;");
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

SET @s = CONCAT("INSERT INTO ", @tablePrefix, "regions",
" (`id`, `name`) VALUES
(1, 'Grand Est'),
(2, 'Nouvelle-Aquitaine'),
(3, 'Auvergne-Rhône-Alpes'),
(4, 'Normandie'),
(5, 'Bourgogne-Franche-Comté'),
(6, 'Bretagne'),
(7, 'Centre-Val de Loire'),
(9, 'Corse'),
(12, 'Île-de-France'),
(16, 'Occitanie'),
(17, 'Hauts-de-France'),
(18, 'Pays-de-la-Loire'),
(21, 'Provence-Alpes-Côte d''Azur'),
(23, 'Guadeloupe'),
(24, 'Guyane'),
(25, 'Martinique'),
(26, 'Réunion'),
(27, 'Territoires d''outre-mer'),
(50, 'Autre pays');");
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- --------------------------------------------------------


--
-- Contenu de la table `region_department`
--

SET @s = CONCAT("TRUNCATE TABLE ", @tablePrefix, "region_department;");
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

SET @s = CONCAT("INSERT INTO ", @tablePrefix, "region_department",
" (`region_id`, `department_id`) VALUES
(1, 67),
(1, 68),
(2, 24),
(2, 33),
(2, 40),
(2, 47),
(2, 64),
(3, 3),
(3, 15),
(3, 43),
(3, 63),
(4, 14),
(4, 50),
(4, 61),
(5, 21),
(5, 58),
(5, 71),
(5, 89),
(6, 22),
(6, 29),
(6, 35),
(6, 56),
(7, 18),
(7, 28),
(7, 36),
(7, 37),
(7, 41),
(7, 45),
(1, 8),
(1, 10),
(1, 51),
(1, 52),
(9, 20),
(5, 25),
(5, 39),
(5, 70),
(5, 90),
(4, 27),
(4, 76),
(12, 75),
(12, 77),
(12, 78),
(12, 91),
(12, 92),
(12, 93),
(12, 94),
(12, 95),
(16, 11),
(16, 30),
(16, 34),
(16, 48),
(16, 66),
(2, 19),
(2, 23),
(2, 87),
(1, 54),
(1, 55),
(1, 57),
(1, 88),
(16, 9),
(16, 12),
(16, 31),
(16, 32),
(16, 46),
(16, 65),
(16, 81),
(16, 82),
(17, 59),
(17, 62),
(18, 44),
(18, 49),
(18, 53),
(18, 72),
(18, 85),
(17, 2),
(17, 60),
(17, 80),
(2, 16),
(2, 17),
(2, 79),
(2, 86),
(21, 4),
(21, 5),
(21, 6),
(21, 13),
(21, 83),
(21, 84),
(3, 1),
(3, 7),
(3, 26),
(3, 38),
(3, 42),
(3, 69),
(3, 73),
(3, 74),
(23, 971),
(24, 973),
(25, 972),
(26, 974),
(26, 976),
(26, 977),
(26, 978),
(27, 98),
(50, 0);");
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;


-- --------------------------------------------------------

