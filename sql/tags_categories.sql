SET NAMES 'utf8';

-- --------------------------------------------------------
-- Prefix setting
--
SET  @tablePrefix = "adl_";

--
-- Contenu de la table `tags_categories`
--

SET @s = CONCAT("INSERT INTO ", @tablePrefix, "tags_categories",
" VALUES (1,'public','(le public à qui s''adresse prioritairement l''événement)'),
(2,'type-evenement','(permet de classer les évènements selon leur type)'),
(5,'reseaux-labels','(label du ou des réseaux à qui l''évènement s''adresse)');");
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
