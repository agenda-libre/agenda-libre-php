SET NAMES 'utf8';

-- --------------------------------------------------------
-- Prefix setting
--
SET  @tablePrefix = "adl_";

--
-- Base de données: `agenda`
--

-- --------------------------------------------------------

--
-- Structure de la table `campaign`
--
SET @s = CONCAT("CREATE TABLE IF NOT EXISTS ", @tablePrefix, "campaign",
" ( `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `start` date NOT NULL,
  `end` date NOT NULL,
  `is_message` int(11) NOT NULL DEFAULT '0',
  `messagehtml` text NOT NULL,
  `is_icon` int(11) NOT NULL DEFAULT '0',
  `iconhtml` text NOT NULL,
  `is_tag` int(11) NOT NULL DEFAULT '0',
  `tag` varchar(255) NOT NULL,
  `tag_messagehtml` text NOT NULL,
  `tag_explainhtml` text NOT NULL,
  `tag_explainstart` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `start` (`start`,`is_message`,`is_icon`,`is_tag`,`tag`,`tag_explainstart`)
) DEFAULT CHARSET=utf8 ;");
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

--
-- Contenu de la table `campaign`
--

SET @s = CONCAT("INSERT INTO ", @tablePrefix, "campaign",
" (`id`, `name`, `start`, `end`, `is_message`, `messagehtml`, `is_icon`, `iconhtml`, `is_tag`, `tag`, `tag_messagehtml`, `tag_explainhtml`, `tag_explainstart`) VALUES
(1, 'LEF2015', '2015-03-13', '2015-04-18', 1, '<p style=\"text-align: center; margin-left: 0.5em;\">Découvrez le Libre autour du 21 mars avec<br/>\r\n    <a href=\"http://www.libre-en-fete.net\">\r\n    <img style=\"border:0;max-width:140px\" src=\"http://www.libre-en-fete.net/images/banniere-2.png\"\r\n    alt=\"LEF\"/></a></p>', 1, '<a href=\"http://www.libre-en-fete.net\" title=\"Libre en Fête\"><img src=\"adl/image/lef-2007-small.png\" style=\"float: right; border: 0;\"  alt=\"Libre en Fête\" /></a>', 1, 'libre-en-fete-2015', '<h1>Libre en Fête 2014</h1><p><a href=\"http://www.libre-en-fete.net\"><img src=\"http://www.libre-en-fete.net/images/banniere-2.png\" width=\"100%\" style=\"float: right; margin-left: 5px; border: 0;\"/></a>Cet évènement s''inscrit dans le cadre de la douzième édition du <a href=\"http://www.libre-en-fete.net\">Libre en Fête</a>.</p>', '<p>Si votre événement s''inscrit dans le cadre de <a href=\"http://www.libre-en-fete.net\">Libre en Fête</a>, précisez le tag <code>libre-en-fete-2015</code></p>', '2014-12-01'),",
" (2, 'LEF2014', '2014-03-07', '2014-04-13', 1, '<p style=\"text-align: center; margin-left: 0.5em;\">Découvrez le Libre autour du 21 mars avec<br/>\r\n    <a href=\"http://www.libre-en-fete.net\">\r\n    <img style=\"border:0;max-width:140px\" src=\"http://www.libre-en-fete.net/images/banniere-2.png\"\r\n    alt=\"LEF\"/></a></p>', 1, '<a href=\"http://www.libre-en-fete.net\" title=\"Libre en Fête\"><img src=\"adl/image/lef-2007-small.png\" style=\"float: right; border: 0;\"  alt=\"Libre en Fête\" /></a>', 1, 'libre-en-fete-2014', '<h1>Libre en Fête 2014</h1>\";\r\n <p>\r\n <a href=\"http://www.libre-en-fete.net\">\r\n <img src=\"http://www.libre-en-fete.net/images/banniere-2.png\" width=\"100\" style=\"float: right; margin-left: 5px; border: 0;\"/>\r\n </a>\r\n Cet évènement s''inscrit dans le cadre de la douzième édition du <a href=\"http://www.libre-en-fete.net\">Libre en Fête</a>.\r\n </p>', '<p>Si votre événement s''inscrit dans le cadre de <a href=\"http://www.libre-en-fete.net\">Libre en Fête</a>, précisez le tag <code>libre-en-fete-2014</code><br/></p>', '2013-12-01'); ");
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
