# install guide

This guide will explain how to install **agenda-libre-php**. The install has been tested on a freshly installed Debian Jessie server.

This guide assumes that you are installing from a user account and you have configured sudo to have administrator rights (this is how Ubuntu is configured by default).

## prerequisite: install needed support software

Depending on your current configuration, you may need to install the necessary software components. These components are:

- **apache** as the webserver
- **php** as the scripting language that generates the dynamic web pages
- **MariaDB** as the database that stores all the data
- **unzip** to unzip the zip archive of **agenda-libre-php**

Note: **agenda-libre-php** is known to work well with MySQL (instead of MariaDB). This configuration is however not deeply tested.

If you're about to host **agenda-libre-php** to a remote hoster, you probably cannot install these components by yourself. They are however probably already installed. Ask your customer support if you need some more information.

You can install apache2, php5, MariaDB and unzip using the following commande line:

```bash
sudo apt-get install apache2 php5 php5-mysql mariadb-server unzip
```

This install can last minutes as apache and MariaDB are large pieces of software.

The MariaDB installer will ask you to create a MariaDB root password. Please note that these MariaDB root user and password have nothing to do with the system root user and password. The system root user applies to the system while the MariaDB root user applies to MariaDB only.

## get agenda-libre-php source code

You can optionally get the source code using git but this won't be covered in this guide.

The source code will be downloaded and unzipped in a temporary directory: `~/tmp/agenda-libre-php/`.

```bash
mkdir -p ~/tmp/agenda-libre-php/
cd ~/tmp/agenda-libre-php/
wget https://git.framasoft.org/agenda-libre/agenda-libre-php/repository/archive.zip
unzip archive.zip
```

## install in a directory accessible to apache

```bash
sudo rm /var/www/html/index.html
sudo cp -a ~/tmp/agenda-libre-php/agenda-libre-php*/www/* /var/www/html/
```

Note : these commands require root access, hence they are prefixed by `sudo`.

## configure agenda-libre-php

### configure MariaDB database

We will configure a new MariaDB database and a new MariaDB user that has access to the database. In this guide, the MariaDB database will be named `agendadb` and the MariaDB user will be named `agendauser`. Feel free to change these values to anything that fits your personal installation.

**You are strongly advised to replace the weak password by a strong personal password!**

```bash
mysql --user=root --password
Enter password:

[...]

MariaDB [(none)]> CREATE DATABASE agendadb;
Query OK, 1 row affected (0.00 sec)

MariaDB [(none)]> GRANT SELECT, INSERT, UPDATE, DELETE, CREATE ON agendadb.* TO 'agendauser'@'localhost'
                  IDENTIFIED BY 'agendapass' WITH GRANT OPTION;
Query OK, 0 rows affected (0.00 sec)

MariaDB [(none)]> quit
Bye
```

If you're about to host **agenda-libre-php** to a remote hoster, your hoster might already have created your MariaDB database and your MariaDB user. Ask your customer support if you need some more information.

As you can see, the user has some [data manipulation language (dml)](https://en.wikipedia.org/wiki/Data_manipulation_language) permissions (*select*, *insert*, *update*, *delete*) and one [data definition language (ddl)](https://en.wikipedia.org/wiki/Data_definition_language) permission (*create*).

The **dml** permissions are useful for querying, creating, modifying and deleting the data. They are mandatory for the proper functioning of the application.

The **ddl** permissions are useful for altering the database schema, creating, modifying and deleting tables and table columns. They are *only* mandatory when installing or upgrading the application.

### create schema and load default data

The default prefix for table name is `adl_`. If you want to use another table name prefix, replace it in the first lines of each file of `sql/*.sql`).

```bash
cd ~/tmp/agenda-libre-php/agenda-libre-php*/
mysql -h localhost -p agendadb -u agendauser < sql/schema.sql
mysql -h localhost -p agendadb -u agendauser < sql/regions.sql
mysql -h localhost -p agendadb -u agendauser < sql/tags_categories.sql
mysql -h localhost -p agendadb -u agendauser < sql/campaign.sql
```

If you changed the MariaDB database name, the MariaDB user name or the MariaDB user password, remember to change `agendadb` and `agendauser` in the command line above. Enter your password when MariaDB request it. You should also replace `localhost` by the MariaDB server of your hoster if you're installing to a remote MariaDB server.

### revoke unnecessary database permissions

Once the schema has been created, your application do not need the *create* permission any more. You can now revoke the unnecessary database permissions.

```bash
mysql --user=root --password
Enter password:

[...]

MariaDB [(none)]> REVOKE CREATE ON agendadb.* FROM 'agendauser'@'localhost';
Query OK, 0 rows affected (0.00 sec)

MariaDB [(none)]> quit
Bye
```

### edit configuration file

Create and edit the main configuration file and fill it with mandatory informations.

```bash
cp /var/www/html/inc/config.inc.php.template /var/www/html/inc/config.inc.php
```

Open `/var/www/html/inc/config.inc.php` and fill it according to your personal configuration.

Here is an example of a minimal standard configuration:

```php
$db_host = "localhost";               /** Where the database is hosted */
$db_user = "agendauser";              /** Username needed to connect to the database */
$db_pass = "agendapass";              /** Password needed to connect to the database */
$db_name = "agendadb";                /** Database name */
$root = "http://agenda.example.org/"; /** Root URL of the libre agenda, without leading slash */
```

Please note that `http://agenda.example.org/` is the example [FQDN](https://en.wikipedia.org/wiki/Fully_qualified_domain_name) of your server that we use for this guide.

### clean temporary directory

You don't need the original archive nor the unzipped files, we will now delete the temporary directory.

```bash
rm -rf ~/tmp/agenda-libre-php/
```

## use it!

Open your web browser and go to: http://agenda.example.org/

### configure the first moderator

When browsing for the first time, you are asked to create the first user record. Be careful to choose admin as role.

### connect as moderator

Go to http://agenda.example.org/moderation.php (also available from the menu `(réservé)`. Enter login and password of the previously created admin user.

### add the list of local groups (optional)

For now, localgroups can be loaded directly to the database if the data are available as sql statements.

### manage tags and tag categories

The list of tags and tag categories are available on http://agenda.example.org/moderatetags.php

Each tag category may contain few tags. Else, you might delete the tag category.

## advanced customization

### create your own theme

You can easily create your own theme from scratch, but you will preferably start from an existing theme and modify it until it fits your needs.

Copy the `/var/www/html/adl/` directory to a new directory:

```bash
sudo cp -a /var/www/html/adl/ /var/www/html/my-new-theme/
```

Edit the `/var/www/html/inc/config.inc.php` configuration file and set the `theme` variable:

```php
$theme = "my-new-theme";                         /** Custom theme */
```

Modify any files in `my-new-theme/` until it fits your needs.
