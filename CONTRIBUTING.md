/* CONTRIBUTE */

This is the contribute.md of our project. Great to have you here. Here are a few ways you can help make this project better!

# contribute.md

## team members

Contributors are listed on the [graphs page](https://git.framasoft.org/agenda-libre/agenda-libre-php/graphs/master).

## learn and listen

Here are the places where you can hear from us:

- [forge](https://git.framasoft.org/agenda-libre/agenda-libre-php): the main place of the project is our GitLab forge hosted by [Framasoft](http://framasoft.org/)
- [wiki](https://git.framasoft.org/agenda-libre/agenda-libre-php/wikis/home): the wiki host everything that do not need to be distributed (manuals, developer guidelines...)
- mailing list: devel@agendadulibre.org (you need to [subscribe](https://listes.agendadulibre.org/wws/subscribe/devel) before posting)
- irc channel: we're not on the irc at any time of day, but we sometime have a meeting there
- phone call: we sometime prefer to call each other by plain old telephon instead of sending another mail, we find it more friendly

## adding new features

- The best way to add new features is to register on the [forge](https://git.framasoft.org/agenda-libre/agenda-libre-php), then to fork the codebase and fill-in a merge request once your feature is fully implemented.
- We have no particular coding style rules to follow at the moment, but we would be glad to adopt one if you have a strong opinion on that.
- The merge request will be taken in account by one the core developers.
- Please test your feature before submitting the merge request.
- Last but not least, do not forget to use clean git commit message as we use them to automatically generate the changelog (see [wiki](https://git.framasoft.org/agenda-libre/agenda-libre-php/wikis/home#git-commit-message) for more informations).

Don’t get discouraged! We estimate that the response time from the maintainers is around: less than a week.

# bug triage

- You can help us by [looking through the existing bugs or reporting new bugs](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues).
- You can help us diagnose and fix existing bugs by asking and providing answers for the following:
    - Is the bug reproducible as explained?   
    - Is it reproducible in other environments (for instance, on different browsers or devices)?   
    - Are the steps to reproduce the bug clear? If not, can you describe how you might reproduce it?  
    - What tags should the bug have?  
    - Is this bug something you have run into? Would you appreciate it being looked into faster?  
- You can check **old and open** bugs to see if they are still happening.
- You can report us any duplicate bug reports by adding a comment linking the two duplicate bugs.

# beta testing

For our project you can find the [roadmap](https://git.framasoft.org/agenda-libre/agenda-libre-php/milestones) and [features and bugs that require testing](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues?label_name=test+required).

# translations

Historically, this project has been started by french developers (see [history](https://git.framasoft.org/agenda-libre/agenda-libre-php/wikis/home#history)) so the interface is mainly in french.

The codebase is however in english and we plan to add internationalisation (i18n), probably using [gettext](http://www.gnu.org/software/gettext/).

Once gettext will be used, we'll need translators from all over the wor(l)d!

# documentation

The documentation is hosted in our [wiki](https://git.framasoft.org/agenda-libre/agenda-libre-php/wikis/home). Feel free to read it and to report us improvements and lacks.

# community 

If you're not a developer but you're willing to help us, you can!

- You can help us answer [questions from our users](https://git.framasoft.org/agenda-libre/agenda-libre-php/issues?label_name=question).
- You can help us writing a manual:
    - user manual
    - administrator manual
    - tester manual
    - developer manual
- You can help us by testing the next release (see [beta testing](#beta-testing)) or brand new feature that has not even been merged into develop (see [adding new features](#adding-new-features)).
- You can also help us by taking in charge newsletters and internal communications..

# your first bugfix

If you came here to know how to submit your first bugfix, the good point is that you read this whooole document, and that makes us happy! Our advice is that you should give us all the useful information in the comments of the issue, then to wait for an answer.

If you don't get any answer in a reasonable delay, forgive us, we might be very occupied elsewhere. Do not hesitate to send an email to the mailing list (see [learn and listen](#learn-and-listen)).
